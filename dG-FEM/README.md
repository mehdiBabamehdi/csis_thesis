
## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [How to use](#how-to-use)

## General info
This code descritises Laplace equation with dG-FEM method (spectral and nodal FEM) and solve it itertively. itertive solver is multigrid algorithm (V-Cycle) to solve the Laplace equation on GPGPU. The host part developed by C++ programming language and device kernel by OPENCL library.

## Technologies
Project is created with:

g++: version 10.1.0

OPENCL: version 2.0

python: version 2.7

pybind11: version 3.5

boost: version 1.74.0

## How to use
To run the code, modify makefile and add path to header(`CL_INC`) and shared object (`CL_LIB`) of OPENCL library in your local machine. 
