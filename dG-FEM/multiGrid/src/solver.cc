
/*-----------------------------------------------------------------------*\
           ___________________________________________________
          /                                                  /|
         /                                                  / |
        |--------------------------------------------------|  |
        |    ______ __     __  ______    _____  __   __    |  |
        |   |   ___|  \   /  |/   ___|  |  _  \|  | |  |   |  |
        |   |  |_  |   \_/   |   /      | |_)  |  | |  |   |  |
        |   |    | |         |  |   ____|   __/|  | |  |   |  |
        |   |   _| |   ___   |  |  |_  _|  |   |  |_|  |   |  |
        |   |  |   |  |   |  |   \__/ / |  |   |       |   |  |
        |   |__|   |__|   |__|\______/  |__|    \_____/    |  |
        |                                                  |  |
        |      Full      Multigrid      On     GPGPU       | /
        |__________________________________________________|/

--------------------------------------------------------------------------

Author

    Mehdi Baba Mehdi
    mehdi.baba_mehdi@uni-wuppertal.de

Date  
    Wed 01.04.2020
\*-----------------------------------------------------------------------*/

#include "../includes/solver.hpp"

// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

template <class T>
solver<T>::solver( T* _RHS,
                int _origCellNo_x,  int _origCellNo_y,  int _origCellNo_z,
                int _polynomialOrder,  std::string _method,  double _penalty)
               :
               RHS{_RHS},
               origCellNo_x{_origCellNo_x},
               origCellNo_y{_origCellNo_y},
               origCellNo_z{_origCellNo_z},
               polynomialOrder{_polynomialOrder}
{
  printLabel();

  origSpatialStepSize = (double)spatialLength / (double)(origCellNo_x);

  addGhostCell();

  nodePerCellPerDir = polynomialOrder + 1;

  nodePerCell = (int) pow(nodePerCellPerDir, 3);

  totalNodeNo_x = origCellNo_x * (nodePerCellPerDir);
  totalNodeNo_y = origCellNo_y * (nodePerCellPerDir);
  totalNodeNo_z = origCellNo_z * (nodePerCellPerDir);
  
  x   = new T[totalNodeNo_y * totalNodeNo_x * totalNodeNo_z]();
  RHS = new T[totalNodeNo_y * totalNodeNo_x * totalNodeNo_z]();
  
  dgLocalMatrix = new dGLGLLocalMatrix<T>(_polynomialOrder, _method, _penalty);

  setInitialValues(initialValue);   
  setRightHandSide();
  bcObject = new boundaryCondition<T>(*this);  
}


template <class T>
solver<T>::solver(std::ifstream& _RHSFileName,
                int _origCellNo_x,  int _origCellNo_y,  int _origCellNo_z,
                int _polynomialOrder,  std::string _method,  double _penalty)
               :
               origCellNo_x{_origCellNo_x},
               origCellNo_y{_origCellNo_y},
               origCellNo_z{_origCellNo_z},
               polynomialOrder{_polynomialOrder}
{
  printLabel();

  origSpatialStepSize = (double)spatialLength / (double)(origCellNo_x);
  
  addGhostCell();

  nodePerCellPerDir = polynomialOrder + 1;
  nodePerCell = (int) pow(nodePerCellPerDir, 3);

  totalNodeNo_x = origCellNo_x * (nodePerCellPerDir);
  totalNodeNo_y = origCellNo_y * (nodePerCellPerDir);
  totalNodeNo_z = origCellNo_z * (nodePerCellPerDir);
  
  x   = new T[totalNodeNo_y * totalNodeNo_x * totalNodeNo_z]();
  RHS = new T[totalNodeNo_y * totalNodeNo_x * totalNodeNo_z]();
  
  dgLocalMatrix = new dGLGLLocalMatrix<T>(_polynomialOrder, _method, _penalty);

  readDataFromFile(_RHSFileName);
  setInitialValues(initialValue);   
  bcObject = new boundaryCondition<T>(*this);  
}


template <class T>
solver<T>::solver( int _origCellNo_x,  int _origCellNo_y,  int _origCellNo_z,
                int _polynomialOrder,  std::string _method,  double _penalty)
               :
               origCellNo_x{_origCellNo_x},
               origCellNo_y{_origCellNo_y},
               origCellNo_z{_origCellNo_z},
               polynomialOrder{_polynomialOrder}
{
  printLabel();

  origSpatialStepSize = (double)spatialLength / (double)(origCellNo_x);

  addGhostCell();

  nodePerCellPerDir = polynomialOrder + 1;
  nodePerCell = (int) pow(nodePerCellPerDir, 3);



  totalNodeNo_x = origCellNo_x * (nodePerCellPerDir);
  totalNodeNo_y = origCellNo_y * (nodePerCellPerDir);
  totalNodeNo_z = origCellNo_z * (nodePerCellPerDir);
  
  x   = new T[totalNodeNo_y * totalNodeNo_x * totalNodeNo_z]();
  RHS = new T[totalNodeNo_y * totalNodeNo_x * totalNodeNo_z]();
  
  setInitialValues(initialValue);   
  
  dgLocalMatrix = new dGLGLLocalMatrix<T>(_polynomialOrder, _method, _penalty);
  bcObject      = new boundaryCondition<T>(*this);  
  
  setRightHandSide();
}


template <class T>
solver<T>::solver(solver<T>& _solver,
                int _polynomialOrder,  std::string _method,  double _penalty)              
{           
  if (_solver.origCellNo_x == 0 || _solver.origCellNo_y == 0 ||
      _solver.origCellNo_z  == 0 || _solver.origSpatialStepSize == 0)
  {
     std::cout << "==> Solver class, uninitialized variable in copy constructor!" 
               << std::endl;
  }

  if ( _solver.origSpatialStepSize == 0.0)
  {
     origSpatialStepSize = (double)spatialLength / (double)(origCellNo_x - 2);
  } else
  {
     origSpatialStepSize = _solver.origSpatialStepSize;
  }

  polynomialOrder = _solver.polynomialOrder;
  nodePerCellPerDir = _solver.nodePerCellPerDir;
  nodePerCell = _solver.nodePerCell;

  origCellNo_x = _solver.getOrigCellNo_x();
  origCellNo_y = _solver.getOrigCellNo_y();
  origCellNo_z = _solver.getOrigCellNo_z();

  totalNodeNo_x = origCellNo_x * (_solver.nodePerCellPerDir);
  totalNodeNo_y = origCellNo_y * (_solver.nodePerCellPerDir);
  totalNodeNo_z = origCellNo_z * (_solver.nodePerCellPerDir);
  
  
  dgLocalMatrix = new dGLGLLocalMatrix<T>(_polynomialOrder, _method, _penalty);

  if(_solver.x == NULL)
  {
     x = new T[totalNodeNo_y * totalNodeNo_x * totalNodeNo_z]();
     setInitialValues(initialValue);   
     bcObject = new boundaryCondition<T>(*this);  
  } else 
  {
     x = new T[totalNodeNo_y * totalNodeNo_x * totalNodeNo_z]();
     x = _solver.x;
  }
  
  if (_solver.RHS == NULL)
  {
     RHS = new T[totalNodeNo_y * totalNodeNo_x * totalNodeNo_z]();
     setRightHandSide();
  } else
  {
     RHS = new T[totalNodeNo_y * totalNodeNo_x * totalNodeNo_z]();
     RHS = _solver.RHS;
  }
}


template <class T>
solver<T>::solver()
{
}


// * * * * * * * * * * * * * * * * Destructor  * * * * * * * * * * * * * * * //

template <class T>
 solver<T>::~solver()
{
}


// * * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * //

template <class T>
void solver<T>::solve()
{
}


template <class T>
inline int solver<T>::getOrigCellNo_y() const
{
   return origCellNo_y;
}


template <class T>
inline int solver<T>::getOrigCellNo_x() const
{
   return origCellNo_x;
}


template <class T>
inline int solver<T>::getOrigCellNo_z() const
{
   return origCellNo_z;
}

template <class T>
inline int solver<T>::getPolynomialOrder() const
{
   return polynomialOrder;
}


template <class T>
inline int solver<T>::getNodePerCell() const
{
   return nodePerCell;
}


template <class T>
inline T solver<T>::getOrigSpatialStepSize() const
{
   return origSpatialStepSize;
}




template <class T>
void solver<T>::addGhostCell()
{
  // add ghost cell 
  origCellNo_x += 2;  
  origCellNo_y += 2;  
  origCellNo_z += 2;  

}


template <class T>
void solver<T>::readDataFromFile(std::ifstream& RHSFileName)
{
  if(!RHSFileName.is_open())
  {
     std::cerr << "==> Solver class, RHS File is NULL!" << std::endl;
     exit(0);
  }

  for(int i=0; i < totalNodeNo_y * totalNodeNo_x * totalNodeNo_z; ++i)
  {
    RHSFileName >> *(RHS + i);
  }
}



template <class T>
void  solver<T>::setInitialValues(T initValue)
{
  for (int k = 0; k < origCellNo_z; ++k) 
  {
     for (int i = 0; i < origCellNo_x; ++i)
     {
        for (int j = 0; j < origCellNo_y; ++j)
        { 
           for (int in = 0; in < nodePerCell; ++in)
           {
               int arrayNumber = (k * origCellNo_x * origCellNo_y +
                                  i * origCellNo_y + j) * nodePerCell + in;

               x[arrayNumber] = initValue;
            }
         } 
      } 
   }
}


template <class T>
void solver<T>::setRightHandSide()
{
  dgLocalMatrix->createRightHandSide(origCellNo_x, origSpatialStepSize, RHS); 
  std::cout << " ||RHS||_L2 = " << 
                 norm_L2(RHS, totalNodeNo_y * totalNodeNo_x * totalNodeNo_z, 
                         origSpatialStepSize) 
            << std::endl;

}


template <class T>
inline T solver<T>::u(T x, T y, T z)
{
   return  (pow(x,2) - pow(x,4)) * (pow(y,4)- pow(y,2)) * (pow(z,2) - pow(z,4));
}


template <class T>
void solver<T>::calcX_exact(T* x_exact)
{
  T xPos, yPos, zPos;

  const double quadPoint[4] = {-1.0, -sqrt(5)/5.0, sqrt(5.0)/5.0, 1.0};

  for (int k = 1; k < origCellNo_z - 1; ++k)
  {
     for (int i = 1; i < origCellNo_x - 1; ++i)
     {
        for (int j = 1; j < origCellNo_y - 1; ++j)
        {
           for (int l = 0; l < nodePerCellPerDir; ++l)
           {
              for (int m = 0; m < nodePerCellPerDir; ++m)
              {
                 for (int n = 0; n < nodePerCellPerDir; ++n)
                 {  
                    if (!((k == 1 && l == 0) || (k == origCellNo_z - 1 && l == nodePerCellPerDir - 1)  ||
                          (i == 1 && m == 0) || (i == origCellNo_z - 1 && m == nodePerCellPerDir - 1)  ||
                          (j == 1 && n == 0) || (j == origCellNo_z - 1 && n == nodePerCellPerDir - 1)))
                    {
                       zPos = k * origSpatialStepSize +
                             (quadPoint[l] - quadPoint[0]) * origSpatialStepSize / 2.0 ;
                       xPos = i * origSpatialStepSize +
                             (quadPoint[m] - quadPoint[0]) * origSpatialStepSize / 2.0 ;
                       yPos = j * origSpatialStepSize +
                             (quadPoint[n] - quadPoint[0]) * origSpatialStepSize / 2.0 ;
           
                       int arrayNumber = (k * origCellNo_y * origCellNo_x + 
                                          i * origCellNo_y + j) * nodePerCell +
                                          l * nodePerCellPerDir * nodePerCellPerDir +
                                          m * nodePerCellPerDir + n;

                       x_exact[arrayNumber] = this->u(xPos, yPos, zPos); 
                    }
                 }
              }
           }
        }
     }
  }
}


template <class T>
void solver<T>::calcError(T* x_exact, T* x_app, T* error)
{
  for (int i = 0; i < totalNodeNo_y * totalNodeNo_x * totalNodeNo_z; ++i)
  {
     error[i] = std::abs(x_exact[i] - x_app[i]);
  }
} 


template <class T>
double solver<T>::norm_2(const T* vec)
{
  T vec_2 = 0.0;

  for (int i = 0; i < totalNodeNo_y * totalNodeNo_x * totalNodeNo_z; ++i)
  {
    vec_2 += pow(vec[i],2);
  }

  return sqrt(vec_2);
}


template <class T>
double solver<T>::norm_L2(const T* vec, const int& domainSize, const T& spatialStepSize)
{
  T vec_2 = 0.0;

  for (int i = 0; i < domainSize; ++i)
  {
    vec_2 += pow(vec[i],2);
  }

  return sqrt(pow(spatialStepSize, 3) * vec_2);
}



template<class T>
T solver<T>::norm_inf(const T* vec)
{
  T vec_inf = 0.0;

  for (int i = 0; i < totalNodeNo_y * totalNodeNo_x * totalNodeNo_z; ++i)
  {
    if (vec[i] > vec_inf) vec_inf = vec[i];
  }

  return vec_inf;
}


template <class T>
inline void solver<T>::printLabel()
{
  auto time = std::chrono::system_clock::now();
  std::time_t currentTime = std::chrono::system_clock::to_time_t(time);
  std::string label = "\n\n"
"\t      ___________________________________________________\n"
"\t     /                                                  /|\n"
"\t    /__________________________________________________/ |\n"
"\t   |                                                  |  |\n"
"\t   |      __  _______     _______    _____  __   __   |  |\n"
"\t   |     |  |/   ____|   /   ____|  |  _  \\|  | |  |  |  |\n"
"\t   |     |  |   /       |   /       | |_)  |  | |  |  |  |\n"
"\t   |   __|  |  |   _____|  |   _____|   __/|  | |  |  |  |\n"
"\t   |  /  _  |  |  |_   _|  |  |_   _|  |   |  |_|  |  |  |\n"
"\t   | |  (_| |   \\__/  / |   \\__/  / |  |   |       |  |  |\n"
"\t   |  \\_____|\\_______/   \\_______/  |__|    \\_____/   |  |\n"
"\t   |                                                  |  |\n"
"\t   |     discontinuous Galerkin    On      GPGPU      | /\n"
"\t   |__________________________________________________|/\n\n"
"\t\t    Author\n"
"\t\t        Mehdi Baba Mehdi\n"
"\t\t        mehdi.baba_mehdi@uni-wuppertal.de\n\n"
"\t\t    Date";


  std::cout << label << std::endl;
  std::cout << "\t\t       " << std::ctime(&currentTime) << std::endl;
  std::cout << "\n\n";

} 


// * * * * * * * * * * * * * * * Operator Overloading * * * * * * * * * * * * * //


template <class T>
std::ofstream& operator<<(std::ofstream& ofs, const solver<T>& result)
{
   std::cout << "==> Solver class, writing data into given file" << std::endl;
   int arrayNumber;

  for (int k = 0; k < result.origCellNo_z; ++k)
  {
     for (int i = 0; i < result.origCellNo_x; ++i)
     {
        for (int j = 0; j < result.origCellNo_y; ++j)
        {
           for (int l = 0; l < result.nodePerCellPerDir; ++l)
           {
              for (int m = 0; m < result.nodePerCellPerDir; ++m)
              {
                 for (int n = 0; n < result.nodePerCellPerDir; ++n)
                 {  
          
                    arrayNumber = (k * result.origCellNo_y * result.origCellNo_x + 
                                i * result.origCellNo_y + j) * result.nodePerCell +
                                l * result.nodePerCellPerDir * result.nodePerCellPerDir +
                                m * result.nodePerCellPerDir + n;
                    
                    ofs << result.x[arrayNumber] << "  ";
                 }
              }
           }
        }
        ofs << "\n";
     }
     ofs << "\n\n";
  }

   return ofs;
}


template <class T>
std::iostream& operator<<(std::iostream& os, const solver<T>& result)
{
   int arrayNumber;

  for (int k = 0; k < result.origCellNo_z; ++k)
  {
     for (int i = 0; i < result.origCellNo_x; ++i)
     {
        for (int j = 0; j < result.origCellNo_y; ++j)
        {
           for (int l = 0; l < result.nodePerCellPerDir; ++l)
           {
              for (int m = 0; m < result.nodePerCellPerDir; ++m)
              {
                 for (int n = 0; n < result.nodePerCellPerDir; ++n)
                 {  
          
                    arrayNumber = (k * result.origCellNo_y * result.origCellNo_x + 
                                 i * result.origCellNo_y + j) * result.nodePerCell +
                                 l * result.nodePerCellPerDir * result.nodePerCellPerDir +
                                 m * result.nodePerCellPerDir + n;
                    
                    os << result.x[arrayNumber] << "  ";
                 }
              }
           }
        }
        os << "\n";
     }
     os << "\n\n";
  }

   return os;
}


