


/*-----------------------------------------------------------------------*\
           ___________________________________________________
          /                                                  /|
         /                                                  / |
        |--------------------------------------------------|  |
        |    ______ __     __  ______    _____  __   __    |  |
        |   |   ___|  \   /  |/   ___|  |  _  \|  | |  |   |  |
        |   |  |_  |   \_/   |   /      | |_)  |  | |  |   |  |
        |   |    | |         |  |   ____|   __/|  | |  |   |  |
        |   |   _| |   ___   |  |  |_  _|  |   |  |_|  |   |  |
        |   |  |   |  |   |  |   \__/ / |  |   |       |   |  |
        |   |__|   |__|   |__|\______/  |__|    \_____/    |  |
        |                                                  |  |
        |      Full      Multigrid      On     GPGPU       | /
        |__________________________________________________|/

--------------------------------------------------------------------------

Author

    Mehdi Baba Mehdi
    mehdi.baba_mehdi@uni-wuppertal.de

Date  
    Mon 13.04.2020
\*-----------------------------------------------------------------------*/


#include "../includes/boundaryCondition.hpp"
#include "../includes/bcFunctionDict.hpp"
#include "../src/bcFunctionDict.cc"

template<class T>
boundaryCondition<T>::boundaryCondition(solver<T>& mySolver)
{
   readBCFromFile();
   setBoundaryCondition(mySolver);
   std::cout << "==> BoundaryCondition class, Boundary conditions were set!"  << 
                 std::endl;
}


template<class T>
void boundaryCondition<T>::readBCFromFile()
{
  std::string firstInput;
  std::string secondInput;
  std::string thirdInput;
  std::string forthInput;
  std::string line;
 
   std::ifstream bcSetupFile(bcSetupFileName);
 
//  bcSetupFile.open(bcSetupFileName, std::ios_base::in);
  if (!bcSetupFile.is_open())
  {
     std::cout << "  -> BoundaryCondition class, Problem in opening bcDict!" << std::endl;
     exit(1);
  }

  for (int i = 0; i < 6; ++i)
  {
     // pass empty lines
//     while ( std::getline(std::cin, line) && line != "" );
     
     bcSetupFile >> firstInput >> secondInput;
     bcSetupFile >> thirdInput >> forthInput;

     if (firstInput == "left" && forthInput == "function")
     {
        bCLeft.insert(std::pair<std::string, std::string>(firstInput, secondInput));
        bCLeft.insert(std::pair<std::string, std::string>(thirdInput, forthInput));
     } else if (firstInput == "right" && forthInput == "function")
     {
        bCRight.insert(std::pair<std::string, std::string>(firstInput, secondInput));
        bCRight.insert(std::pair<std::string, std::string>(thirdInput, forthInput));
     } else if (firstInput == "up" && forthInput == "function")
     {
        bCUp.insert(std::pair<std::string, std::string>(firstInput, secondInput));
        bCUp.insert(std::pair<std::string, std::string>(thirdInput, forthInput));
     } else if (firstInput == "down" && forthInput == "function")
     {
        bCDown.insert(std::pair<std::string, std::string>(firstInput, secondInput));
        bCDown.insert(std::pair<std::string, std::string>(thirdInput, forthInput));
     } else if (firstInput == "back" && forthInput == "function")
     {
        bCBack.insert(std::pair<std::string, std::string>(firstInput, secondInput));
        bCBack.insert(std::pair<std::string, std::string>(thirdInput, forthInput));
     } else if (firstInput == "front" && forthInput == "function")
     {
        bCFront.insert(std::pair<std::string, std::string>(firstInput, secondInput));
        bCFront.insert(std::pair<std::string, std::string>(thirdInput, forthInput));
     } else
     {
        std::cout << "   -> BoundaryCondition class,  Problem in loading bcDict!" <<
                     std::endl;
                                 
        exit(1);
     }
  }
  bcSetupFile.close();
}


/*
 * left  bc x = 0
 * right bc x = n
 * down  bc y = 0
 * up    bc y = n
 * back  bc z = 0
 * front bc z = n
 */
template<class T>
void boundaryCondition<T>::setBoundaryCondition(solver<T>& mySolver)
{
   boundaryHandler<T> bcHandler;
   // left bc
   if (bCLeft["left"] == "Dirichlet")
   {
      bcHandler.boundaryFunction = left;
      applyBC(bcHandler, mySolver, 0, 1, 0, mySolver.getOrigCellNo_x(), 0, 
              mySolver.getOrigCellNo_z());
   }
   
   // right bc
   if (bCRight["right"] == "Dirichlet")
   {
      bcHandler.boundaryFunction = right;
      applyBC(bcHandler, mySolver, mySolver.getOrigCellNo_y()-1, 
              mySolver.getOrigCellNo_y(), 0, mySolver.getOrigCellNo_x(), 0, 
              mySolver.getOrigCellNo_z());
   }
   
   // down bc
   if (bCDown["down"] == "Dirichlet")
   {
     bcHandler.boundaryFunction = down;
     applyBC(bcHandler, mySolver, 0, mySolver.getOrigCellNo_y(), 0, 1, 0, 
             mySolver.getOrigCellNo_z());
   }

   // up bc
   if (bCUp["up"] == "Dirichlet")
   {
      bcHandler.boundaryFunction = up;
      applyBC(bcHandler, mySolver, 0, mySolver.getOrigCellNo_y(), 
              mySolver.getOrigCellNo_x()-1, mySolver.getOrigCellNo_x(), 0, 
              mySolver.getOrigCellNo_z());
   }
   
   // back bc
   if (bCBack["back"] == "Dirichlet")
   {
      bcHandler.boundaryFunction = back;
      applyBC(bcHandler, mySolver, 0, mySolver.getOrigCellNo_y(), 0, 
              mySolver.getOrigCellNo_x(), 0, 1);
   }
   
   // front bc
   if (bCFront["front"] == "Dirichlet")
   {
      bcHandler.boundaryFunction = front;
      applyBC(bcHandler, mySolver, 0, mySolver.getOrigCellNo_y(), 0, 
              mySolver.getOrigCellNo_x(), mySolver.getOrigCellNo_z()-1, 
              mySolver.getOrigCellNo_z());
   }
}



/*
 * x in j direction, i.e. x = j * h
 * y in i direction, i.e. y = i * h
 * z in k direction, i.e. z = k * k
 */
template<class T>
void boundaryCondition<T>::applyBC(boundaryHandler<T> bcHandler, solver<T>& mySolver, int i0, int in, int j0, int jn, int k0, int kn)
{

  int nodePerCell = mySolver.getNodePerCell();

  T xPos, yPos, zPos;
  for (int k = k0; k < kn; ++k)
  {
     for (int i = i0; i < in; ++i)
     {
        for (int j = j0; j < jn; ++j)
        {
           xPos = j * mySolver.getOrigSpatialStepSize();
           yPos = i * mySolver.getOrigSpatialStepSize();
           zPos = k * mySolver.getOrigSpatialStepSize();
           for (int ni = 0; ni < nodePerCell; ++ni)
           {
              mySolver.x[(k * mySolver.getOrigCellNo_y() * mySolver.getOrigCellNo_x() +
                i * mySolver.getOrigCellNo_y() + j) * nodePerCell + ni] = 0.0;
                 // bcHandler.boundaryFunction(xPos,yPos,zPos);
           }
        }
     }
  }
}
