
/*-----------------------------------------------------------------------*\
	      __________________________________________________
	     /                                                 /|
	    /_________________________________________________/ |
	   |                                                 |  |
	   |     __  _______     _______    _____  __   __   |  |
	   |    |  |/   ____|   /   ____|  |  _  \|  | |  |  |  |
	   |    |  |   /       |   /       | |_)  |  | |  |  |  |
	   |  __|  |  |   _____|  |   _____|   __/|  | |  |  |  |
	   | /  _  |  |  |_   _|  |  |_   _|  |   |  |_|  |  |  |
	   ||  (_| |   \__/  / |   \__/  / |  |   |       |  |  |
	   | \_____|\_______/   \_______/  |__|    \_____/   |  |
	   |                                                 |  |
           |    discontinuous Galerkin    On      GPGPU      | /
	   |_________________________________________________|/


--------------------------------------------------------------------------

Author

    Mehdi Baba Mehdi
    mehdi.baba_mehdi@uni-wuppertal.de

Date  
    Fri 10.04.2020
\*-----------------------------------------------------------------------*/


#include "../includes/restriction.hpp"

template<class T>
restriction<T>::restriction(cl::Program& _program, cl::CommandQueue& _queue,
                            cl::Context& _context, const int& _nodePerCellPerDir)
                            :
                            nodePerCellPerDir{_nodePerCellPerDir}
{
  restrictionStencilSize = std::pow(nodePerCellPerDir,3) * std::pow(nodePerCellPerDir,3) * 
                           std::pow(stencilWidth,3);
 
  localSizeDir = stencilWidth * nodePerCellPerDir;

  setStencil(_context, _queue, nodePerCellPerDir);
  createKernel(_program);
}


template<class T>
restriction<T>::~restriction()
{
   delete[] stencil;
}

template<class T>
inline const long double restriction<T>::getTime() const
{
   return timeOfOperation;
}


template<class T>
void restriction<T>::setStencil(cl::Context& context, cl::CommandQueue& queue,
                                const int nodePerCellPerDir)
{
  dGStencil = new dGProlStencilCreator<T>(nodePerCellPerDir);
  dGStencil->kroneckerProduct("restriction");

  try
  {
     restrictionStencilBuffer = cl::Buffer(context, CL_MEM_READ_ONLY, 
                                           restrictionStencilSize * sizeof(T));
     queue.enqueueWriteBuffer(restrictionStencilBuffer,  CL_TRUE, 0, 
                              restrictionStencilSize * sizeof(T), dGStencil->stencil3D);
  }catch (const cl::Error& error)
  {
    std::cout << "  -> Restriction class, Problem in buffer creation/writing data to "
                 "device " << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }
}


template<class T> 
void restriction<T>::createKernel(cl::Program& program)
{
  std::cout << std::setfill(' ') << std::setw(40);
  std::cout << std::left << "==> Restriction class, Creating kernels";
  try
  {
    restrictionKernel  = cl::Kernel(program, "restriction");
    std::cout << std::right << "  -> Done! " << std::endl;
  }catch (const cl::Error& error)
  {
    std::cout << "  -> Restriction class, Problem in kernel  " << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }
}


template<class T>
void restriction<T>::doRestriction(cl::CommandQueue&  queue, 
                   cl::Buffer& inputBuffer, cl::Buffer& outputBuffer,
                   const cl::NDRange& distGlobalRange, const cl::NDRange& distLocalRange,
                   const cl::size_t<3> maxDistCellNo,
                   cl::Event& event)
{

  auto t1 = std::chrono::high_resolution_clock::now();

  int localMemSize = std::pow(localSizeDir, 3) * sizeof(T);

  int argCount = 0; 
  try
  {
    restrictionKernel.setArg(argCount++, inputBuffer);
    restrictionKernel.setArg(argCount++, (int)maxDistCellNo[0]);
    restrictionKernel.setArg(argCount++, (int)maxDistCellNo[1]);
    restrictionKernel.setArg(argCount++, (int)maxDistCellNo[2]);
    restrictionKernel.setArg(argCount++, restrictionStencilBuffer);
    restrictionKernel.setArg(argCount++, outputBuffer);
    restrictionKernel.setArg(argCount++, localMemSize, NULL);
    restrictionKernel.setArg(argCount++, localSizeDir);
    restrictionKernel.setArg(argCount++, localSizeDir);
    restrictionKernel.setArg(argCount++, localSizeDir);
    restrictionKernel.setArg(argCount++, stencilWidth);
  } catch (const cl::Error& error)
  {
    std::cout << "  -> Restriction class, Problem in setting the argument of kernel "
                      << "restriction" << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }

  try
  {
    queue.enqueueNDRangeKernel(restrictionKernel, cl::NullRange, 
                               distGlobalRange, distLocalRange, NULL, &event);
  } catch (const cl::Error& error)
  {
    std::cout << "  -> Restriction class, Problem in enqueue kernel restrcition" 
              << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }

  try
  {
    queue.finish();
  } catch (const cl::Error& error)
  {
    std::cout << "  -> Restriction class, Problem in finishing enqueue" << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }
  
  auto t2 = std::chrono::high_resolution_clock::now();
  timeOfOperation +=  std::chrono::duration_cast<std::chrono::microseconds>
                      (t2-t1).count() ;

}
