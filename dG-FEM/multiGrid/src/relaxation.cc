
/*-----------------------------------------------------------------------*\
           ___________________________________________________
          /                                                  /|
         /                                                  / |
        |--------------------------------------------------|  |
        |    ______ __     __  ______    _____  __   __    |  |
        |   |   ___|  \   /  |/   ___|  |  _  \|  | |  |   |  |
        |   |  |_  |   \_/   |   /      | |_)  |  | |  |   |  |
        |   |    | |         |  |   ____|   __/|  | |  |   |  |
        |   |   _| |   ___   |  |  |_  _|  |   |  |_|  |   |  |
        |   |  |   |  |   |  |   \__/ / |  |   |       |   |  |
        |   |__|   |__|   |__|\______/  |__|    \_____/    |  |
        |                                                  |  |
        |      Full      Multigrid      On     GPGPU       | /
        |__________________________________________________|/

--------------------------------------------------------------------------

Author

    Mehdi Baba Mehdi
    mehdi.baba_mehdi@uni-wuppertal.de

Date  
    Fri 10.04.2020
\*-----------------------------------------------------------------------*/



#include "../includes/relaxation.hpp"

template<class T>
relaxation<T>::relaxation(cl::Program& _program, cl::CommandQueue& _queue,
                          const cl::Context& _context, 
                          std::string relaxationMethod_, T omega_)
:
                     relaxationMethod{relaxationMethod_},
                     omega{omega_}
{
  createKernel(_program);
}


template<class T>
relaxation<T>::~relaxation()
{
}

template<class T>
inline const T relaxation<T>::getOmega() const
{
   return omega;
}


template<class T>
inline const long double relaxation<T>::getTime() const
{
   return timeOfOperation;
}


template<class T>
void relaxation<T>::createKernel(cl::Program& program)
{
  std::cout << std::setfill(' ') << std::setw(40);
  std::cout << std::left << "==> Relaxation class, Creating kernels";

  try
  {
    if (relaxationMethod == "jacobi")
    {
       kernelJacobi  = cl::Kernel(program, "jacobi");
    } else if (relaxationMethod == "GS")
    {  
       kernelGSRed    = cl::Kernel(program, "GS_red");
       kernelGSBlack  = cl::Kernel(program, "GS_black");
    }
    std::cout << std::right << "  -> Done! " << std::endl;
  }catch (const cl::Error& error)
  {
    std::cout << "  -> Relaxation class, Problem in kernel  " << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }
}


/*
 * x_n = b - Ax_{n-1}
 * inoutBuffer -> x
 * RHSBuffer   -> b
 * relaxationStencilBuffer -> A
 */
template<class T>
void relaxation<T>::relaxing(cl::CommandQueue& queue,
                         cl::Buffer& inoutBuffer, cl::Buffer& RHSBuffer,
                         cl::Buffer& intermediateBuffer,
                         const int polynomialOrder, 
                         const cl::Buffer& volumeIntegralMatrixBuffer,
                         const std::vector<cl::Buffer> M11Buffer,
                         const std::vector<cl::Buffer> M12Buffer,
                         const std::vector<cl::Buffer> M21Buffer,
                         const std::vector<cl::Buffer> M22Buffer,
                         const cl::NDRange& globalRange, const cl::NDRange& localRange,
                         const int numberOfRelaxationSweep,
                         const cl::size_t<3> domainCellNo, 
                         cl::size_t<3> bufferOrigin, cl::size_t<3> hostOrigin, 
                         cl::size_t<3> region, 
                         cl::Event& event)
{
  auto t1 = std::chrono::high_resolution_clock::now();

  if (domainCellNo[0] < 1)
  {
     directSolver(queue, inoutBuffer, RHSBuffer,
                         intermediateBuffer,
                         polynomialOrder, 
                         volumeIntegralMatrixBuffer,
                         M11Buffer,
                         M12Buffer,
                         M21Buffer,
                         M22Buffer,
                         globalRange, localRange,
                         domainCellNo,
                         bufferOrigin, hostOrigin, 
                         region, 
                         event); 
  } else if (domainCellNo[0] > 1)
  {
     if (relaxationMethod == "jacobi")
     {
        jacobi(queue, inoutBuffer, RHSBuffer,
                      intermediateBuffer,
                      polynomialOrder, 
                      volumeIntegralMatrixBuffer,
                      M11Buffer,
                      M12Buffer,
                      M21Buffer,
                      M22Buffer,
                      globalRange, localRange,
                      numberOfRelaxationSweep,
                      domainCellNo,
                      bufferOrigin, hostOrigin, 
                      region, 
                      event); 

     } else if (relaxationMethod == "GS")
     {
        Guass_Seidel(queue, inoutBuffer, RHSBuffer,
                            intermediateBuffer,
                            polynomialOrder, 
                            volumeIntegralMatrixBuffer,
                            M11Buffer,
                            M12Buffer,
                            M21Buffer,
                            M22Buffer,
                            globalRange, localRange,
                            numberOfRelaxationSweep,
                            domainCellNo,
                            bufferOrigin, hostOrigin, 
                            region, 
                            event); 
     }
  }
  auto t2 = std::chrono::high_resolution_clock::now();
  timeOfOperation += std::chrono::duration_cast<std::chrono::microseconds>
                     (t2-t1).count();
}



template<class T>
void relaxation<T>::jacobi(cl::CommandQueue& queue,
                         cl::Buffer& inoutBuffer, cl::Buffer& RHSBuffer,
                         cl::Buffer& intermediateBuffer,
                         const int polynomialOrder, 
                         const cl::Buffer& volumeIntegralMatrixBuffer,
                         const std::vector<cl::Buffer> M11Buffer,
                         const std::vector<cl::Buffer> M12Buffer,
                         const std::vector<cl::Buffer> M21Buffer,
                         const std::vector<cl::Buffer> M22Buffer,
                         const cl::NDRange& globalRange, 
                         const cl::NDRange& localRange,
                         const int numberOfRelaxationSweep,
                         const cl::size_t<3> domainCellNo, 
                         cl::size_t<3> bufferOrigin, cl::size_t<3> hostOrigin, 
                         cl::size_t<3> region, 
                         cl::Event& event)
{
  cl::Event copyEvent;
  cl::Event iterationEvent;

  int localSize = cellInLocalMem * (polynomialOrder + 1);

  int localMemSize = std::pow(cellInLocalMem, 3) * 
                     std::pow(polynomialOrder + 1, 3) * sizeof(T);


  try
  {
    queue.enqueueCopyBufferRect(inoutBuffer, intermediateBuffer, bufferOrigin, 
              hostOrigin, region, domainCellNo[0] * (polynomialOrder + 1) * sizeof(T), 0, 
              domainCellNo[0] * (polynomialOrder + 1) * sizeof(T), 
              0, NULL, &copyEvent);
  } catch (const cl::Error& error)
  {
    std::cout << "  -> Relaxation class, Problem in copying buffer x to y" << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }

  try
  {
    queue.finish();
  }catch (const cl::Error& error)
  {
    std::cout << "  -> Relaxation class, Problem in finishing copy" << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }
  copyEvent.wait();

  int argCount = 0;
  try
  {
    kernelJacobi.setArg(argCount++, inoutBuffer);
    kernelJacobi.setArg(argCount++, (int)domainCellNo[0]);
    kernelJacobi.setArg(argCount++, (int)domainCellNo[1]);
    kernelJacobi.setArg(argCount++, (int)domainCellNo[2]);
    kernelJacobi.setArg(argCount++, volumeIntegralMatrixBuffer);
    
    for (int i = 0; i < faceNo; ++i)
    {  
       kernelJacobi.setArg(argCount++, M11Buffer[i]);
       kernelJacobi.setArg(argCount++, M12Buffer[i]);
       kernelJacobi.setArg(argCount++, M21Buffer[i]);
       kernelJacobi.setArg(argCount++, M22Buffer[i]);
    }
    
    kernelJacobi.setArg(argCount++, RHSBuffer);
    kernelJacobi.setArg(argCount++, intermediateBuffer);
    kernelJacobi.setArg(argCount++, localMemSize, NULL);
    kernelJacobi.setArg(argCount++, localSize);
    kernelJacobi.setArg(argCount++, localSize);
    kernelJacobi.setArg(argCount++, localSize);
    kernelJacobi.setArg(argCount++, cellInLocalMem);
  } catch (const cl::Error& error)
  {
    std::cout << "  -> Relaxation class, Problem in setting the argument of kernel" << 
                std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }

  for (int i = 0; i < numberOfRelaxationSweep; ++i)
  {
    try
    {
      queue.enqueueNDRangeKernel(kernelJacobi, cl::NullRange, globalRange, localRange, 
                                 NULL, &iterationEvent);
    }catch (const cl::Error& error)
    {
      std::cout << "  -> Relaxation class, Problem in enqueue kernel" << 
                   std::endl;
      std::cout << "  -> " << getErrorString(error) << std::endl;
      exit(0);
    }

    try
    {
      queue.finish();
    }catch (const cl::Error& error)
    {
      std::cout << "  -> Relaxation class, Problem in finishing kernel" << std::endl;
      std::cout << "  -> " << getErrorString(error) << std::endl;
      exit(0);
    }
    
    iterationEvent.wait();
  
    if (i == numberOfRelaxationSweep - 1)
    {
       try
       {
          queue.enqueueCopyBufferRect(intermediateBuffer, inoutBuffer, bufferOrigin, 
              hostOrigin, region, domainCellNo[0] * (polynomialOrder + 1) * sizeof(T), 0, 
              domainCellNo[0] * (polynomialOrder + 1) * sizeof(T),
              0, NULL, &event);
       } catch (const cl::Error& error)
       {
           std::cout << "  -> Problem in copying buffer x to y" << std::endl;
           std::cout << "  -> " << getErrorString(error) << std::endl;
           exit(0);
       }
    } else
    {
       try
       {
          queue.enqueueCopyBufferRect(intermediateBuffer, inoutBuffer, bufferOrigin, 
              hostOrigin, region, domainCellNo[0] * (polynomialOrder + 1) * sizeof(T), 0, 
              domainCellNo[0] * (polynomialOrder + 1) * sizeof(T),
              0, NULL, &copyEvent);
       } catch (const cl::Error& error)
       {
           std::cout << "  -> Problem in copying buffer x to y" << std::endl;
           std::cout << "  -> " << getErrorString(error) << std::endl;
           exit(0);
       }
       copyEvent.wait();
    }

    try
    {
      queue.finish();
    }catch (const cl::Error& error)
    {
      std::cout << "  -> Relaxation class, Problem in finishing copy" << std::endl;
      std::cout << "  -> " << getErrorString(error) << std::endl;
      exit(0);
    }
  }
}


template<class T>
void relaxation<T>::Guass_Seidel(cl::CommandQueue& queue,
                         cl::Buffer& inoutBuffer, cl::Buffer& RHSBuffer,
                         cl::Buffer& intermediateBuffer,
                         const int polynomialOrder, 
                         const cl::Buffer& volumeIntegralMatrixBuffer,
                         const std::vector<cl::Buffer> M11Buffer,
                         const std::vector<cl::Buffer> M12Buffer,
                         const std::vector<cl::Buffer> M21Buffer,
                         const std::vector<cl::Buffer> M22Buffer,
                         const cl::NDRange& globalRange, const cl::NDRange& localRange,
                         const int numberOfRelaxationSweep,
                         const cl::size_t<3> domainCellNo, 
                         cl::size_t<3> bufferOrigin, cl::size_t<3> hostOrigin, 
                         cl::size_t<3> region, 
                         cl::Event& event)
{
  cl::Event copyEvent;
  cl::Event iterationEvent;

  int localSize = cellInLocalMem * (polynomialOrder + 1);

  int localMemSize = std::pow(cellInLocalMem, 3) * 
                     std::pow(polynomialOrder + 1, 3) * sizeof(T);
  try
  {
    queue.enqueueCopyBufferRect(inoutBuffer, intermediateBuffer, bufferOrigin, 
            hostOrigin, region, domainCellNo[0] * (polynomialOrder + 1) * sizeof(T), 0, 
            domainCellNo[0] * (polynomialOrder + 1) * sizeof(T),
            0, NULL, &copyEvent);
  } catch (const cl::Error& error)
  {
    std::cout << "  -> Relaxation class, Problem in copying buffer x to y" << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }

  try
  {
    queue.finish();
  }catch (const cl::Error& error)
  {
    std::cout << "  -> Relaxation class, Problem in finishing copy" << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }
  copyEvent.wait();

  int argCount = 0;
  try
  {
    kernelGSRed.setArg(argCount++, inoutBuffer);
    kernelGSRed.setArg(argCount++, (int)domainCellNo[0]);
    kernelGSRed.setArg(argCount++, (int)domainCellNo[1]);
    kernelGSRed.setArg(argCount++, (int)domainCellNo[2]);
    kernelGSRed.setArg(argCount++, volumeIntegralMatrixBuffer);
    
    for (int i = 0; i < faceNo; ++i)
    {  
       kernelGSRed.setArg(argCount++, M11Buffer[i]);
       kernelGSRed.setArg(argCount++, M12Buffer[i]);
       kernelGSRed.setArg(argCount++, M21Buffer[i]);
       kernelGSRed.setArg(argCount++, M22Buffer[i]);
    }
    
    kernelGSRed.setArg(argCount++, RHSBuffer);
    kernelGSRed.setArg(argCount++, intermediateBuffer);
    kernelGSRed.setArg(argCount++, localMemSize, NULL);
    kernelGSRed.setArg(argCount++, localSize);
    kernelGSRed.setArg(argCount++, localSize);
    kernelGSRed.setArg(argCount++, localSize);
    kernelGSRed.setArg(argCount++, cellInLocalMem);
  } catch (const cl::Error& error)
  {
    std::cout << "  -> Relaxation class, Problem in setting the argument of kernel" << 
                std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }

  argCount = 0;
  try
  {
    kernelGSBlack.setArg(argCount++, inoutBuffer);
    kernelGSBlack.setArg(argCount++, (int)domainCellNo[0]);
    kernelGSBlack.setArg(argCount++, (int)domainCellNo[1]);
    kernelGSBlack.setArg(argCount++, (int)domainCellNo[2]);
    kernelGSBlack.setArg(argCount++, volumeIntegralMatrixBuffer);

    for (int i = 0; i < faceNo; ++i)
    {  
       kernelGSBlack.setArg(argCount++, M11Buffer[i]);
       kernelGSBlack.setArg(argCount++, M12Buffer[i]);
       kernelGSBlack.setArg(argCount++, M21Buffer[i]);
       kernelGSBlack.setArg(argCount++, M22Buffer[i]);
    }

    kernelGSBlack.setArg(argCount++, RHSBuffer);
    kernelGSBlack.setArg(argCount++, intermediateBuffer);
    kernelGSBlack.setArg(argCount++, localMemSize, NULL);
    kernelGSBlack.setArg(argCount++, localSize);
    kernelGSBlack.setArg(argCount++, localSize);
    kernelGSBlack.setArg(argCount++, localSize);
    kernelGSBlack.setArg(argCount++, cellInLocalMem);
  } catch (const cl::Error& error)
  {
    std::cout << "  -> Relaxation class, Problem in setting the argument of kernel" << 
                std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }
  
  for (int i = 0; i < numberOfRelaxationSweep; ++i)
  {
    try
    {
       queue.enqueueNDRangeKernel(kernelGSRed, cl::NullRange, globalRange, localRange, 
                                  NULL, &iterationEvent);
    }catch (const cl::Error& error)
    {
       std::cout << "  -> Relaxation class, Problem in enqueue kernel" << 
                    std::endl;
       std::cout << "  -> " << getErrorString(error) << std::endl;
       exit(0);
    }

    try
    {
       queue.finish();
    }catch (const cl::Error& error)
    {
       std::cout << "  -> Relaxation class, Problem in finishing kernel" << std::endl;
       std::cout << "  -> " << getErrorString(error) << std::endl;
       exit(0);
    }
    
    iterationEvent.wait();
    try
    {
       queue.enqueueCopyBufferRect(intermediateBuffer, inoutBuffer, bufferOrigin, 
              hostOrigin, region, domainCellNo[0] * (polynomialOrder + 1) * sizeof(T), 0, 
              domainCellNo[0] * (polynomialOrder + 1) * sizeof(T),
              0, NULL, NULL);
    } catch (const cl::Error& error)
    {
       std::cout << "  -> Problem in copying buffer x to y" << std::endl;
       std::cout << "  -> " << getErrorString(error) << std::endl;
       exit(0);
    }
    
    try
    {
       queue.finish();
    }catch (const cl::Error& error)
    {
       std::cout << "  -> Relaxation class, Problem in finishing kernel" << std::endl;
       std::cout << "  -> " << getErrorString(error) << std::endl;
       exit(0);
    }
    
    try
    {
       queue.enqueueNDRangeKernel(kernelGSBlack, cl::NullRange, globalRange, localRange, 
                                  NULL, &iterationEvent);
    }catch (const cl::Error& error)
    {
       std::cout << "  -> Relaxation class, Problem in enqueue kernel" << 
                   std::endl;
       std::cout << "  -> " << getErrorString(error) << std::endl;
       exit(0);
    }

    try
    {
       queue.finish();
    }catch (const cl::Error& error)
    {
       std::cout << "  -> Relaxation class, Problem in finishing kernel" << std::endl;
       std::cout << "  -> " << getErrorString(error) << std::endl;
       exit(0);
    }
    
    iterationEvent.wait();


    if (i == numberOfRelaxationSweep - 1)
    {
       try
       {
          queue.enqueueCopyBufferRect(intermediateBuffer, inoutBuffer, bufferOrigin, 
              hostOrigin, region, domainCellNo[0] * (polynomialOrder + 1) * sizeof(T), 0, 
              domainCellNo[0] * (polynomialOrder + 1) * sizeof(T),
              0, NULL, &event);
       } catch (const cl::Error& error)
       {
          std::cout << "  -> Problem in copying buffer x to y" << std::endl;
          std::cout << "  -> " << getErrorString(error) << std::endl;
          exit(0);
       }
    } else
    {
       try
       {
          queue.enqueueCopyBufferRect(intermediateBuffer, inoutBuffer, bufferOrigin, 
              hostOrigin, region, domainCellNo[0] * (polynomialOrder + 1) * sizeof(T), 0, 
              domainCellNo[0] * (polynomialOrder + 1) * sizeof(T),
              0, NULL, &copyEvent);
       } catch (const cl::Error& error)
       {
           std::cout << "  -> Problem in copying buffer x to y" << std::endl;
           std::cout << "  -> " << getErrorString(error) << std::endl;
           exit(0);
       }
       copyEvent.wait();
    }
    
    try
    {
      queue.finish();
    }catch (const cl::Error& error)
    {
      std::cout << "  -> Relaxation class, Problem in finishing copy" << std::endl;
      std::cout << "  -> " << getErrorString(error) << std::endl;
      exit(0);
    }
  }
}



template<class T>
void relaxation<T>::directSolver(cl::CommandQueue& queue,
                         cl::Buffer& inoutBuffer, cl::Buffer& RHSBuffer,
                         cl::Buffer& intermediateBuffer,
                         const int polynomialOrder, 
                         const cl::Buffer& volumeIntegralMatrixBuffer,
                         const std::vector<cl::Buffer> M11Buffer,
                         const std::vector<cl::Buffer> M12Buffer,
                         const std::vector<cl::Buffer> M21Buffer,
                         const std::vector<cl::Buffer> M22Buffer,
                         const cl::NDRange& globalRange, const cl::NDRange& localRange,
                         const cl::size_t<3> domainCellNo, 
                         cl::size_t<3> bufferOrigin, cl::size_t<3> hostOrigin, 
                         cl::size_t<3> region, 
                         cl::Event& event)
{
  cl::Event copyEvent;
  cl::Event iterationEvent;

  int localSize = cellInLocalMem * (polynomialOrder + 1);

  int localMemSize = std::pow(cellInLocalMem, 3) * 
                     std::pow(polynomialOrder + 1, 3) * sizeof(T);
  try
  {
    queue.enqueueCopyBufferRect(inoutBuffer, intermediateBuffer, bufferOrigin, 
           hostOrigin, region, domainCellNo[0] * (polynomialOrder + 1) * sizeof(T), 0, 
           domainCellNo[0] * (polynomialOrder + 1) * sizeof(T),
           0, NULL, &copyEvent);
  } catch (const cl::Error& error)
  {
    std::cout << "  -> Relaxation class, Problem in copying buffer x to y" << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }

  try
  {
    queue.finish();
  }catch (const cl::Error& error)
  {
    std::cout << "  -> Relaxation class, Problem in finishing copy" << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }
  copyEvent.wait();

  int argCount = 0;
  try
  {
    kernelDirectSolver.setArg(argCount++, inoutBuffer);
    kernelDirectSolver.setArg(argCount++, (int)domainCellNo[0]);
    kernelDirectSolver.setArg(argCount++, (int)domainCellNo[1]);
    kernelDirectSolver.setArg(argCount++, (int)domainCellNo[2]);
    kernelDirectSolver.setArg(argCount++, volumeIntegralMatrixBuffer);

    for (int i = 0; i < faceNo; ++i)
    {  
       kernelDirectSolver.setArg(argCount++, M11Buffer[i]);
       kernelDirectSolver.setArg(argCount++, M12Buffer[i]);
       kernelDirectSolver.setArg(argCount++, M21Buffer[i]);
       kernelDirectSolver.setArg(argCount++, M22Buffer[i]);
    }
    
    kernelDirectSolver.setArg(argCount++, RHSBuffer);
    kernelDirectSolver.setArg(argCount++, intermediateBuffer);
    kernelDirectSolver.setArg(argCount++, localMemSize, NULL);
    kernelDirectSolver.setArg(argCount++, localSize);
    kernelDirectSolver.setArg(argCount++, localSize);
    kernelDirectSolver.setArg(argCount++, localSize);
    kernelDirectSolver.setArg(argCount++, cellInLocalMem);
  } catch (const cl::Error& error)
  {
    std::cout << "  -> Relaxation class, Problem in setting the argument of kernel" << 
                std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }

  try
  {
    queue.enqueueNDRangeKernel(kernelDirectSolver, cl::NullRange, globalRange, localRange,
                                 NULL, &iterationEvent);
  }catch (const cl::Error& error)
  {
    std::cout << "  -> Relaxation class, Problem in enqueue kernel" << 
                   std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }

  try
  {
    queue.finish();
  }catch (const cl::Error& error)
  {
    std::cout << "  -> Relaxation class, Problem in finishing kernel" << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }
    
  iterationEvent.wait();
  
  try
  {
    queue.enqueueCopyBufferRect(intermediateBuffer, inoutBuffer, bufferOrigin, 
              hostOrigin, region, domainCellNo[0] * (polynomialOrder + 1) * sizeof(T), 0, 
              domainCellNo[0] * (polynomialOrder + 1) * sizeof(T),
                                      0, NULL, &event);
  } catch (const cl::Error& error)
  {
    std::cout << "  -> Problem in copying buffer x to y" << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }
  
  try
  {
    queue.finish();
  }catch (const cl::Error& error)
  {
    std::cout << "  -> Relaxation class, Problem in finishing copy" << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }
}
