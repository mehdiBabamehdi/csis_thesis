
/*-----------------------------------------------------------------------*\
           ___________________________________________________
          /                                                  /|
         /                                                  / |
        |--------------------------------------------------|  |
        |    ______ __     __  ______    _____  __   __    |  |
        |   |   ___|  \   /  |/   ___|  |  _  \|  | |  |   |  |
        |   |  |_  |   \_/   |   /      | |_)  |  | |  |   |  |
        |   |    | |         |  |   ____|   __/|  | |  |   |  |
        |   |   _| |   ___   |  |  |_  _|  |   |  |_|  |   |  |
        |   |  |   |  |   |  |   \__/ / |  |   |       |   |  |
        |   |__|   |__|   |__|\______/  |__|    \_____/    |  |
        |                                                  |  |
        |      Full      Multigrid      On     GPGPU       | /
        |__________________________________________________|/

--------------------------------------------------------------------------

Author

    Mehdi Baba Mehdi
    mehdi.baba_mehdi@uni-wuppertal.de

Date  
    Sat 11.04.2020
\*-----------------------------------------------------------------------*/


#include "../includes/residual.hpp"

template<class T>
residual<T>::residual(cl::Program& _program)
{
  createKernel(_program);
}


template<class T>
residual<T>::~residual()
{
}


template<class T>
inline const long double residual<T>::getTime() const
{
   return timeOfOperation;
}


template<class T>
void residual<T>::createKernel(cl::Program& program)
{
  std::cout << std::setfill(' ') << std::setw(40);
  std::cout << std::left << "==> Residual class, Creating kernels";
  try
  {
    kernel  = cl::Kernel(program, "residual");
    std::cout << std::right << "  -> Done! " << std::endl;
  }catch (const cl::Error& error)
  {
    std::cout << "  -> Residual class, Problem in kernel  " << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }
}


template<class T>
void residual<T>::calculateResidual(cl::CommandQueue& queue, 
                          const cl::Buffer& xBuffer, const cl::Buffer& RHSBuffer,
                          cl::Buffer& residualBuffer, const int& polynomialOrder, 
                          const cl::Buffer& volumeIntegralMatrixBuffer,
                          const std::vector<cl::Buffer> M11Buffer,
                          const std::vector<cl::Buffer> M12Buffer,
                          const std::vector<cl::Buffer> M21Buffer,
                          const std::vector<cl::Buffer> M22Buffer,
                          const cl::NDRange& globalRange, 
                          const cl::NDRange& localRange,
                          const cl::size_t<3> maxCellNo,
                          cl::Event& event)

{

  auto t1 = std::chrono::high_resolution_clock::now();

  int localSize = cellInLocalMem * (polynomialOrder + 1);

  int localMemSize = std::pow(cellInLocalMem, 3) * 
                     std::pow(polynomialOrder + 1, 3) * sizeof(T);

  int argCount = 0;
  try
  {
    kernel.setArg(argCount++, xBuffer);
    kernel.setArg(argCount++, (int)maxCellNo[0]);
    kernel.setArg(argCount++, (int)maxCellNo[1]);
    kernel.setArg(argCount++, (int)maxCellNo[2]);
    kernel.setArg(argCount++, volumeIntegralMatrixBuffer);
    
    for (int i = 0; i < faceNo; ++i)
    {  
       kernel.setArg(argCount++, M11Buffer[i]);
       kernel.setArg(argCount++, M12Buffer[i]);
       kernel.setArg(argCount++, M21Buffer[i]);
       kernel.setArg(argCount++, M22Buffer[i]);
    }

    kernel.setArg(argCount++, RHSBuffer);
    kernel.setArg(argCount++, residualBuffer);
    kernel.setArg(argCount++, localMemSize, NULL);
    kernel.setArg(argCount++, localSize);
    kernel.setArg(argCount++, localSize);
    kernel.setArg(argCount++, localSize);
    kernel.setArg(argCount++, cellInLocalMem);
  } catch (const cl::Error& error)
  {
    std::cout << "  -> Residual class, Problem in setting the argument of kernel residual"
              << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }

  try
  {
    queue.enqueueNDRangeKernel(kernel, cl::NullRange, globalRange, localRange, 
                               NULL, &event);
  }catch (const cl::Error& error)
  {
    std::cout << "  -> Residual class, Problem in enqueue kernel residual" << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }

  try
  {
    queue.finish();
  }catch (const cl::Error& error)
  {
    std::cout << "  -> Residual class, Problem in finishing kernel residual" << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }

  auto t2 = std::chrono::high_resolution_clock::now();
  timeOfOperation += std::chrono::duration_cast<std::chrono::microseconds>
                     (t2-t1).count();
}
