/*-----------------------------------------------------------------------*\
 	      __________________________________________________
	     /                                                 /|
	    /_________________________________________________/ |
	   |                                                 |  |
	   |     __  _______     _______    _____  __   __   |  |
	   |    |  |/   ____|   /   ____|  |  _  \|  | |  |  |  |
	   |    |  |   /       |   /       | |_)  |  | |  |  |  |
	   |  __|  |  |   _____|  |   _____|   __/|  | |  |  |  |
	   | /  _  |  |  |_   _|  |  |_   _|  |   |  |_|  |  |  |
	   ||  (_| |   \__/  / |   \__/  / |  |   |       |  |  |
	   | \_____|\_______/   \_______/  |__|    \_____/   |  |
	   |                                                 |  |
           |    discontinuous Galerkin    On      GPGPU      | /
	   |_________________________________________________|/

--------------------------------------------------------------------------

Author

    Mehdi Baba Mehdi
    mehdi.baba_mehdi@uni-wuppertal.de

Date  
    Fri 10.04.2020
\*-----------------------------------------------------------------------*/

#include "../includes/prolongation.hpp"

/*--------------------Constructor-------------------------*/
template<class T>
prolongation<T>::prolongation(cl::Program& _program, cl::CommandQueue& _queue, 
                const cl::Context& _context, const int& _nodePerCellPerDir)
                :
                nodePerCellPerDir{_nodePerCellPerDir}
               
{
  prolStencilSize = std::pow(nodePerCellPerDir, 3) * std::pow(nodePerCellPerDir, 3) * 
                    std::pow(stencilCellWidth, 3);
 
  localSizeDir = localCellNo * nodePerCellPerDir;
  
  setStencil(_context, _queue);
  createKernel(_program);
}


/*--------------------Deconstructor-------------------------*/
template<class T>
prolongation<T>::~prolongation()
{
}


/* * * * * * * * * * * * * * * * Member functions * * * * * * * * * * * * * * * * * */

template<class T>
inline const long double prolongation<T>::getTime() const
{
   return timeOfOperation;
}


template<class T>
void prolongation<T>::setStencil(const cl::Context& context, cl::CommandQueue& queue)
{
  dGStencil = new dGProlStencilCreator<T>(nodePerCellPerDir);
  dGStencil->kroneckerProduct("prolongation");
 
  try
  {  
    prolStencilBuffer = cl::Buffer(context, CL_MEM_READ_ONLY, 
                                   prolStencilSize * sizeof(T));
    queue.enqueueWriteBuffer(prolStencilBuffer,  CL_TRUE, 0, 
                             prolStencilSize * sizeof(T), dGStencil->stencil3D);
  } catch (const cl::Error& error)
  {
    std::cout << "  -> Prolongation class, Problem in buffer creation/writing "
                   "data to device " << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }
}


template<class T>
void prolongation<T>::createKernel(cl::Program& program)
{
  std::cout << std::setfill(' ') << std::setw(40);
  std::cout << std::left << "==> Prolongation class, Creating kernels";

  try
  {
    prolKernel = cl::Kernel(program, "prolongation");
    std::cout << std::right << "  -> Done! " << std::endl;
  }catch (const cl::Error& error)
  {
    std::cout << "  -> Prolongation class, Problem in kernel  " << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }
}


template<class T>
void prolongation<T>::linearInterpolation(cl::CommandQueue& queue, 
          cl::Buffer& inputBuffer,  cl::Buffer& outputBuffer,
          const cl::NDRange& srcGlobalRange, const cl::NDRange& srcLocalRange,   
          const cl::size_t<3> maxSrcCellNo, 
          cl::Event& event)
{
  auto t1 = std::chrono::high_resolution_clock::now();

  //* * * * * * * * * * * * * * * * apply prolongation * * * * * * * * * * * * * * *//  

  int localMemSize = std::pow(localSizeDir, 3) * sizeof(T); 

  int argCount = 0;
  try
  {
    prolKernel.setArg(argCount++, inputBuffer);
    prolKernel.setArg(argCount++, (int)maxSrcCellNo[0]);
    prolKernel.setArg(argCount++, (int)maxSrcCellNo[1]);
    prolKernel.setArg(argCount++, (int)maxSrcCellNo[2]);
    prolKernel.setArg(argCount++, prolStencilBuffer);
    prolKernel.setArg(argCount++, outputBuffer);
    prolKernel.setArg(argCount++, localMemSize, NULL);
    prolKernel.setArg(argCount++, localSizeDir);
    prolKernel.setArg(argCount++, localSizeDir);
    prolKernel.setArg(argCount++, localSizeDir);
    prolKernel.setArg(argCount++, stencilCellWidth);
    prolKernel.setArg(argCount++, localCellNo);
  } catch (const cl::Error& error)
  {
    std::cout << "  -> Prolongation class, Problem in setting the argument of kernel "
                 "prolongation" << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }

  try
  {
    queue.enqueueNDRangeKernel(prolKernel, cl::NullRange, srcGlobalRange, 
                               srcLocalRange, NULL, &event);
  }catch (const cl::Error& error)
  {
    std::cout << "  -> Prolongation class, Problem in enqueue kernel prolongation"
              <<  std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }

  try
  {
    queue.finish();
  }catch (const cl::Error& error)
  {
    std::cout << "  -> Prolongation class, Problem in finishing kernel "
                 "prolongation" << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }
  
  auto t2 = std::chrono::high_resolution_clock::now();
  timeOfOperation += std::chrono::duration_cast<std::chrono::microseconds>
                     (t2-t1).count();
}
