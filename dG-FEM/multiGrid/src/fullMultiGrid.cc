/*-----------------------------------------------------------------------*\
           ___________________________________________________
          /                                                  /|
         /                                                  / |
        |--------------------------------------------------|  |
        |    ______ __     __  ______    _____  __   __    |  |
        |   |   ___|  \   /  |/   ___|  |  _  \|  | |  |   |  |
        |   |  |_  |   \_/   |   /      | |_)  |  | |  |   |  |
        |   |    | |         |  |   ____|   __/|  | |  |   |  |
        |   |   _| |   ___   |  |  |_  _|  |   |  |_|  |   |  |
        |   |  |   |  |   |  |   \__/ / |  |   |       |   |  |
        |   |__|   |__|   |__|\______/  |__|    \_____/    |  |
        |                                                  |  |
        |      Full      Multigrid      On     GPGPU       | /
        |__________________________________________________|/

--------------------------------------------------------------------------

Author

    Mehdi Baba Mehdi
    mehdi.baba_mehdi@uni-wuppertal.de

Date  
    Wed 01.04.2020
\*-----------------------------------------------------------------------*/


#include "../includes/fullMultiGrid.hpp"

template <class T>
fullMultiGrid<T>::fullMultiGrid( T* _RHS,
           int _origCellNo_x,  int _origCellNo_y,  int _origCellNo_z,
          std::string _relaxationMethod, T _omega,
          cl_device_type _deviceType, int _deviceID, 
          int _nu_1, int _nu_2, bool _plotting)
          :
          multiGrid<T>(_RHS, _origCellNo_x, _origCellNo_y, _origCellNo_z,
                       _relaxationMethod, _omega, _deviceType, _deviceID),
          nu_1{_nu_1},
          nu_2{_nu_2},
          plotting{_plotting}
{
}


template <class T>
fullMultiGrid<T>::fullMultiGrid(std::ifstream _RHSFILENAME,
           int _origCellNo_x,  int _origCellNo_y,  int _origCellNo_z,
          std::string _relaxationMethod, T _omega,
          cl_device_type _deviceType, int _deviceID, 
          int _nu_1, int _nu_2, bool _plotting)
          :
          multiGrid<T>(_RHSFILENAME, _origCellNo_x, _origCellNo_y, _origCellNo_z, 
                       _relaxationMethod, _omega, _deviceType, _deviceID),
          nu_1{_nu_1},
          nu_2{_nu_2},
          plotting{_plotting}
{
}


template <class T>
fullMultiGrid<T>::fullMultiGrid( int _origCellNo_x,  int _origCellNo_y, 
            int _origCellNo_z,
           std::string _relaxationMethod, T _omega,
           cl_device_type _deviceType, int _deviceID, 
           int _nu_1, int _nu_2, bool _plotting)
           :
           multiGrid<T>(_origCellNo_x, _origCellNo_y, _origCellNo_z, _relaxationMethod, 
                        _omega, _deviceType, _deviceID),
           nu_1{_nu_1},
           nu_2{_nu_2},
           plotting{_plotting}
{
}


template <class T>
fullMultiGrid<T>::fullMultiGrid( multiGrid<T>& _fmgMultiGrid,
           int _nu_1, int _nu_2, bool _plotting)
           :
           multiGrid<T>(_fmgMultiGrid),
           nu_1{_nu_1},
           nu_2{_nu_2},
           plotting{_plotting}
{
}


template <class T>
fullMultiGrid<T>::fullMultiGrid(int _nu_1, int _nu_2, bool _plotting)
           :
           multiGrid<T>(),
           nu_1{_nu_1},
           nu_2{_nu_2},
           plotting{_plotting}
{
}


template <class T>
fullMultiGrid<T>::~fullMultiGrid()
{
  this->volumeIntegralMatrixBuffer.clear();
  this->faceIntegralMatrixBuffer.clear();
  this->faceM11IntegralMatrixBuffer.clear();
  this->residualBuffer.clear();
  this->errorBuffer.clear();
  this->globalRange.clear();
  this->localRange.clear();
  this->restProlGlobalRange.clear();
  this->deviceWidth.clear();
  this->deviceHeight.clear();
  this->deviceDepth.clear();
  this->deviceDataSize.clear();
  this->domainWidth.clear();
  this->domainHeight.clear();
  this->domainDepth.clear();
  delete[] this->residual_h; 
  delete[] this->x; 
  delete[] this->RHS; 
}



template<class T>
void fullMultiGrid<T>::solve()
{
  this->setDomainSize();

  int j;
  fmgTime = new double[this->multiGridLevels+1]();
  int mgLevel = this->multiGridLevels + 1;

  /* Initialization of device and buffers */
  this->buildProgram(this->omega);
  this->createBuffers();
  this->setNDRange();
  this->writeDataToDevice();

  this->mgRelaxation   = new relaxation<T>(this->program, this->queue, this->context, 
                                           this->relaxationMethod, this->omega);
  this->mgResidual     = new residual<T>(this->program);
  this->mgRestriction  = new restriction<T>(this->program, this->queue, this->context,
                                            this->nodePerCellPerDir);
  this->mgProlongation = new prolongation<T>(this->program, this->queue, this->context,
                                            this->nodePerCellPerDir);
  this->mgGnuplot      = new gnuplot<T>();

  
  std::cout << "__________________________________________________\n" << std::endl;

  // calculate error and plot it before implementing FMG
  T* x_exact = new T[this->domainWidth[0] * this->domainHeight[0] * this->domainDepth[0]];
  T* error = new T[this->domainWidth[0] * this->domainHeight[0] * this->domainDepth[0]];

  this->calcX_exact(x_exact);
  this->calcError(x_exact, this->x, error);

  std::cout<< "==> Initial error ";
  double normErrorInit = this->norm_L2(error, this->domainHeight[0] * this->domainWidth[0]                 * this->domainDepth[0], this->spatialStepSize[0]);

  std::cout << "   ->||e||_L2 = " << normErrorInit << std::endl;

  if(plotting) this->mgGnuplot->plot(error, this->domainHeight[0], this->domainWidth[0],
                        this->domainDepth[0], "error", 0);

  // calculate residual before impelementing FMG
  std::cout<< "\n==> Initial Residual ";
  this->mgResidual->calculateResidual(this->queue, this->xBuffer, this->bBuffer, 
                        this->residualBuffer[0],
                        this->polynomialOrder,
                        this->volumeIntegralMatrixBuffer[0],
                        this->faceIntegralMatrixBuffer[0],
                        this->faceM11IntegralMatrixBuffer[0],
                        this->globalRange[0], this->localRange[0], 
                        this->deviceHeight[0], this->deviceWidth[0], this->deviceDepth[0],
                        this->deviceLocalSize[0], this->subt[0],
                        this->event);

  this->event.wait();
  
  double normResidualInit = this->printData(this->residualBuffer[0], 0, false, true);
  double normResidualEnd;

  auto tStart = std::chrono::high_resolution_clock::now();
  auto tEnd = std::chrono::high_resolution_clock::now();
  double tt = std::chrono::duration_cast<std::chrono::microseconds>(tEnd-tStart).count();

  /*
   * <- prolongation
   * -> restriction
   */

  std::cout<< "\n\t >>> Start Full Multigrid Process <<<\n" << std::endl;
  for (int i = this->multiGridLevels; i >= 0; --i)
  {
     mgLevel--;
     
     // first descend leg going from finset to coursest grid. Only residual from step "h"
     // restricted into courser grid, and at coursest grid, solve "Ae=r" directly if 
     // possible or iteratively  
     
     if (mgLevel == this->multiGridLevels)
     {  
        // Relaxing and calculate residual finest grid
        tStart = std::chrono::high_resolution_clock::now();
        std::cout<< "==> Relaxation in level " << 0 << std::endl;
        this->mgRelaxation->relaxing(this->queue, 
                        this->xBuffer, this->bBuffer, 
                        this->intermediateBuffer[0],
                        this->polynomialOrder,
                        this->volumeIntegralMatrixBuffer[0],
                        this->faceIntegralMatrixBuffer[0],
                        this->faceM11IntegralMatrixBuffer[0],
                        this->globalRange[0], this->localRange[0], nu_1, 
                        this->deviceHeight[0], this->deviceWidth[0], this->deviceDepth[0],
                        this->deviceLocalSize[0],
                        this->bufferOrigin, this->hostOrigin, this->region[0],
                        this->subt[0], this->event);

        this->event.wait();
        
        tEnd = std::chrono::high_resolution_clock::now();
        tt = std::chrono::duration_cast<std::chrono::microseconds>(tEnd-tStart).count();
        if (timing) std::cout << "Exc time = " <<  tt << " u Sec" << std::endl;
        fmgTime[0] += tt;
        relaxTime += tt;

        tStart = std::chrono::high_resolution_clock::now();
        std::cout<< "==> Calculation Residual in level " << 0 << std::endl;
        this->mgResidual->calculateResidual(this->queue, this->xBuffer, this->bBuffer, 
                        this->residualBuffer[0], 
                        this->polynomialOrder,
                        this->volumeIntegralMatrixBuffer[0],
                        this->faceIntegralMatrixBuffer[0],
                        this->faceM11IntegralMatrixBuffer[0],
                        this->globalRange[0], this->localRange[0], 
                        this->deviceHeight[0], this->deviceWidth[0], this->deviceDepth[0],
                        this->deviceLocalSize[0], this->subt[0],
                        this->event);

        this->event.wait();

        tEnd = std::chrono::high_resolution_clock::now();
        tt = std::chrono::duration_cast<std::chrono::microseconds>(tEnd-tStart).count();
        if (timing) std::cout << "Exc time = " <<  tt << " u Sec" << std::endl;
        fmgTime[0] += tt;
        residTime += tt;

        #ifdef DEBUG
        std::cout << "residual" << std::endl;
        this->printData(this->residualBuffer[0], 0, true, true);
        #endif
        #ifndef DEBUG
        this->printData(this->residualBuffer[0], 0, false, true);
        #endif

        // Go down to coursest grid in first step (first descend leg, only restrict 
        // residual and directly solve on coursest grid)
        for(j = 0; j < mgLevel; ++j)
        { 
           // do restriction
           tStart = std::chrono::high_resolution_clock::now();
           std::cout << "==> Restriction, " << j << " --> " << j + 1 << std::endl;
           this->mgRestriction->doRestriction(this->queue, this->residualBuffer[j],
                       this->residualBuffer[j+1], 
                       this->restProlGlobalRange[j], this->localRange[j],
                       this->deviceHeight[j], this->deviceWidth[j], this->deviceDepth[j], 
                       this->deviceWidth[j+1], this->deviceLocalSize[j], this->subt[j], 
                       this->event);

           this->event.wait();

           tEnd = std::chrono::high_resolution_clock::now();
           tt = std::chrono::duration_cast<std::chrono::microseconds>
                                          (tEnd-tStart).count();
           if (timing) std::cout << "Exc time = " <<  tt << " u Sec" << std::endl;
           fmgTime[j] += tt;
           restTime += tt;
           
           #ifdef DEBUG
           std::cout << "Rest+interResidual" << std::endl;
           this->printData(this->interResidualBuffer[j+1], j+1, true, true);
           #endif

           if (j + 1 == mgLevel)
           {
              // coursest grid, solve directly if possible
              std::cout<< "==> Relaxation in level " << j + 1 << std::endl;
              tStart = std::chrono::high_resolution_clock::now();
              this->mgRelaxation->relaxing(this->queue, 
                  this->errorBuffer[j+1], this->residualBuffer[j+1], 
                  this->intermediateBuffer[j+1],
                  this->polynomialOrder,
                  this->volumeIntegralMatrixBuffer[j+1],
                  this->faceIntegralMatrixBuffer[j+1],
                  this->faceM11IntegralMatrixBuffer[j+1],
                  this->globalRange[j+1], this->localRange[j+1], nu_1, 
                  this->deviceHeight[j+1], this->deviceWidth[j+1], this->deviceDepth[j+1],
                  this->deviceLocalSize[j+1],
                  this->bufferOrigin, this->hostOrigin, this->region[j+1],
                  this->subt[j+1], this->event);

              this->event.wait();
              tEnd = std::chrono::high_resolution_clock::now();
              tt = std::chrono::duration_cast<std::chrono::microseconds>
                                             (tEnd-tStart).count();
              if (timing) std::cout << "Exc time = " <<  tt << " u Sec" << std::endl;
              fmgTime[j] += tt;
              relaxTime += tt;

              #ifdef DEBUG
              std::cout << "Relax+error" << std::endl;
              this->printData(this->errorBuffer[j+1], j+1, true, false);
              #endif
           } 

/*         std::cout << "==> r_" << j + 1 << " = r_" << j - 1 << " - A_" << j + 1 << 
                      " * e_" << j + 1 << std::endl;
           tStart = std::chrono::high_resolution_clock::now();
           this->mgResidual->calculateResidual(this->queue, this->errorBuffer[j+1], 
                  this->interResidualBuffer[j+1], 
                  this->residualBuffer[j+1], this->matrixBuffer[j+1],
                       this->polynomialOrder,
                       this->volumeIntegralMatrixBuffer[0],
                       this->faceIntegralMatrixBuffer[0],
                       this->faceM11IntegralMatrixBuffer[0],
                  this->globalRange[j+1], this->localRange[j+1], 
                  this->deviceHeight[j+1], this->deviceWidth[j+1], this->deviceDepth[j+1],
                  this->deviceLocalSize[j+1], this->subt[j+1],
                  this->event);

           this->event.wait();
           tEnd = std::chrono::high_resolution_clock::now();
           tt = std::chrono::duration_cast<std::chrono::microseconds>
                                          (tEnd-tStart).count();
           if (timing) std::cout << "Exc time = " <<  tt << " u Sec" << std::endl;
           fmgTime[j] += tt;
           residTime += tt;

           #ifdef DEBUG
           std::cout << "residual+residual" << std::endl;
           this->printData(this->residualBuffer[j+1], j+1, true, true);
           #endif

           #ifndef DEBUG
           this->printData(this->residualBuffer[j+1], j+1, false, true);
           #endif
  */       }
      }
   

      // go up to fine grid. the middle steps where we go from coursest grid to 
      // one step filner than perivous V-cycle
      for(j = this->multiGridLevels; j > mgLevel; --j)
      {
         if (j - 1 == 0 )
         {
            // prolongation into finset grid for last deep V-cycle
            // do prolongation 
            std::cout << "==> Prolongation, E_" << j - 1 << " <-- e_" << j <<
                         ",\tx =  E_" << j - 1 << " + x"  << std::endl;
            tStart = std::chrono::high_resolution_clock::now();

            this->mgProlongation->cubicInterpolation(this->context, this->queue, 
                 this->errorBuffer[j], this->xBuffer,
                 this->intermediateBuffer[j-1],
                 this->restProlGlobalRange[j], this->localRange[j],
                 this->restProlGlobalRange[j-1], this->localRange[j-1],
                 this->deviceHeight[j], this->deviceWidth[j], this->deviceDepth[j],
                 this->deviceHeight[j-1], this->deviceWidth[j-1], this->deviceDepth[j-1],
                 this->deviceLocalSize[j-1], this->subt[j-1], this->event);
         
           this->event.wait();
           tEnd = std::chrono::high_resolution_clock::now();
           tt = std::chrono::duration_cast<std::chrono::microseconds>
                                          (tEnd-tStart).count();
           if (timing) std::cout << "Exc time = " <<  tt << " u Sec" << std::endl;
           fmgTime[j] += tt;
           prolTime += tt;


           std::cout<< "==> Relaxation in level " << j - 1 << std::endl;
           tStart = std::chrono::high_resolution_clock::now();

           this->mgRelaxation->relaxing(this->queue, 
                  this->xBuffer, this->bBuffer, 
                  this->intermediateBuffer[0],
                  this->polynomialOrder,
                  this->volumeIntegralMatrixBuffer[j-1],
                  this->faceIntegralMatrixBuffer[j-1],
                  this->faceM11IntegralMatrixBuffer[j-1],
                  this->globalRange[j-1], this->localRange[j-1], nu_2, 
                  this->deviceHeight[j-1], this->deviceWidth[j-1], this->deviceDepth[j-1],
                  this->deviceLocalSize[j-1],
                  this->bufferOrigin, this->hostOrigin, this->region[j-1],
                  this->subt[j-1], this->event);
         
           this->event.wait();
           tEnd = std::chrono::high_resolution_clock::now();
           tt = std::chrono::duration_cast<std::chrono::microseconds>
                                           (tEnd-tStart).count();
           if (timing) std::cout << "Exc time = " <<  tt << " u Sec" << std::endl;
           fmgTime[j] += tt;
           relaxTime += tt;

           std::cout << "==> r_" << j - 1 << " = b - A_" << j - 1 << 
                      " * x" << std::endl;
           tStart = std::chrono::high_resolution_clock::now();

           this->mgResidual->calculateResidual(this->queue, 
                  this->xBuffer, this->bBuffer, 
                  this->residualBuffer[j-1],
                  this->polynomialOrder,
                  this->volumeIntegralMatrixBuffer[j-1],
                  this->faceIntegralMatrixBuffer[j-1],
                  this->faceM11IntegralMatrixBuffer[j-1],
                  this->globalRange[j-1], this->localRange[j-1], 
                  this->deviceHeight[j-1], this->deviceWidth[j-1], this->deviceDepth[j-1],
                  this->deviceLocalSize[j-1], this->subt[j-1],
                  this->event);

           this->event.wait();
           tEnd = std::chrono::high_resolution_clock::now();
           tt = std::chrono::duration_cast<std::chrono::microseconds>
                                          (tEnd-tStart).count();
           if (timing) std::cout << "Exc time = " <<  tt << " u Sec" << std::endl;
           //fmgTime[j] += tt;
           residTime += tt;

           #ifdef DEBUG
           std::cout << "residual+interResidual" << std::endl;
           this->printData(this->residualBuffer[j-1], j-1, true, false);
           #endif
           #ifndef DEBUG
           this->printData(this->residualBuffer[j-1], j-1, false, true);
           #endif

         } else
         {   // do prolongation

            tStart = std::chrono::high_resolution_clock::now();
            if (j - 1 == mgLevel) 
            {
               std::cout << "==> Cubic Interpolation, E_" << j - 1 << " <-- e_" << j << 
                 ",\te_" << j - 1 << " =  E_" << j - 1 << " + e_"<< j - 1  << std::endl;
               this->mgProlongation->cubicInterpolation(this->context, this->queue,
                  this->errorBuffer[j], this->errorBuffer[j-1],
                  this->intermediateBuffer[j-1],
                  this->restProlGlobalRange[j], this->localRange[j],
                  this->restProlGlobalRange[j-1], this->localRange[j-1],
                  this->deviceHeight[j], this->deviceWidth[j], this->deviceDepth[j],
                  this->deviceHeight[j-1], this->deviceWidth[j-1], this->deviceDepth[j-1],
                  this->deviceLocalSize[j-1], this->subt[j-1], this->event);

            }else 
            {
                std::cout << "==> Linear Interpolation, E_" << j - 1 << " <-- e_" << j << 
                 ",\te_" << j - 1 << " =  E_" << j - 1 << " + e_"<< j - 1  << std::endl;
               this->mgProlongation->linearInterpolation(this->queue,
                  this->errorBuffer[j], this->errorBuffer[j-1],
                  this->intermediateBuffer[j-1],
                  this->restProlGlobalRange[j], this->localRange[j],
                  this->restProlGlobalRange[j-1], this->localRange[j-1],
                  this->deviceHeight[j], this->deviceWidth[j], this->deviceDepth[j],
                  this->deviceHeight[j-1], this->deviceWidth[j-1], this->deviceDepth[j-1],
                  this->deviceLocalSize[j-1], this->subt[j-1], this->event);
            }
            
            this->event.wait();
            tEnd = std::chrono::high_resolution_clock::now();
            tt = std::chrono::duration_cast<std::chrono::microseconds>
                                           (tEnd-tStart).count();
            if (timing) std::cout << "Exc time = " <<  tt << " u Sec" << std::endl;
            fmgTime[j] += tt;
            prolTime += tt;

            #ifdef DEBUG
            std::cout << "prol+error" << std::endl;
            this->printData(this->errorBuffer[j-1], j-1, true, false);
            #endif

            std::cout<< "==> Relaxation in level " << j - 1 << std::endl;
            tStart = std::chrono::high_resolution_clock::now();

            this->mgRelaxation->relaxing(this->queue,
                  this->errorBuffer[j-1], this->residualBuffer[j-1],
                  this->intermediateBuffer[j-1],
                  this->polynomialOrder,
                  this->volumeIntegralMatrixBuffer[j-1],
                  this->faceIntegralMatrixBuffer[j-1],
                  this->faceM11IntegralMatrixBuffer[j-1],
                  this->globalRange[j-1], this->localRange[j-1], nu_2, 
                  this->deviceHeight[j-1], this->deviceWidth[j-1], this->deviceDepth[j-1],
                  this->deviceLocalSize[j-1],
                  this->bufferOrigin, this->hostOrigin, this->region[j-1],  
                  this->subt[j-1], this->event);
        
           this->event.wait();
           tEnd = std::chrono::high_resolution_clock::now();
           tt = std::chrono::duration_cast<std::chrono::microseconds>
                                           (tEnd-tStart).count();
           if (timing) std::cout << "Exc time = " <<  tt << " u Sec" << std::endl;
           fmgTime[j] += tt;
           relaxTime += tt;

           #ifdef DEBUG
           std::cout << "relax+error" << std::endl;
           this->printData(this->errorBuffer[j-1], j-1, true, false);
           #endif
 
           if (j - 1 == mgLevel)
           {
           // At finest level of each V-Cycle in middle step, residual is calculated to
           // use in following for restrcition
             std::cout << "==> r_" << j - 1 << " = r_" << j - 1 << " - A_" << j - 1 << 
                     " * e_" << j - 1 << std::endl;
             tStart = std::chrono::high_resolution_clock::now();

             this->mgResidual->calculateResidual(this->queue, 
                 this->errorBuffer[j-1], this->residualBuffer[j-1], 
                 this->interResidualBuffer[j-1],
                 this->polynomialOrder,
                 this->volumeIntegralMatrixBuffer[j-1],
                 this->faceIntegralMatrixBuffer[j-1],
                 this->faceM11IntegralMatrixBuffer[j-1],
                 this->globalRange[j-1], this->localRange[j-1], 
                 this->deviceHeight[j-1], this->deviceWidth[j-1], this->deviceDepth[j-1],
                 this->deviceLocalSize[j-1], this->subt[j-1],
                 this->event);

             this->event.wait();
             this->copyBuffer(this->interResidualBuffer[j-1], 
                              this->residualBuffer[j-1], j-1);
             this->event.wait();

             tEnd = std::chrono::high_resolution_clock::now();
             tt = std::chrono::duration_cast<std::chrono::microseconds>
                                            (tEnd-tStart).count();
             if (timing) std::cout << "Exc time = " <<  tt << " u Sec" << std::endl;
             fmgTime[j] += tt;
             residTime += tt;

             #ifdef DEBUG
             std::cout << "residual+interResidual" << std::endl;
             this->printData(this->residualBuffer[j-1], j-1, true, false);
             #endif
             #ifndef DEBUG
             this->printData(this->residualBuffer[j-1], j-1, false, true);
             #endif
           }
         }
      }
      
      // go down to coursest grid in middle V-Cycle
      for (j = mgLevel; j < this->multiGridLevels; ++j)
      {
          std::cout << "==> Restriction, r_" << j << " --> r_" << j + 1 << std::endl;
          tStart = std::chrono::high_resolution_clock::now();

          this->mgRestriction->doRestriction(this->queue, this->residualBuffer[j],  
                     this->interResidualBuffer[j+1], 
                     this->restProlGlobalRange[j], this->localRange[j],
                     this->deviceHeight[j], this->deviceWidth[j], this->deviceDepth[j], 
                     this->deviceWidth[j+1], this->deviceLocalSize[j], this->subt[j],
                     this->event);

           this->event.wait();
           tEnd = std::chrono::high_resolution_clock::now();
           tt = std::chrono::duration_cast<std::chrono::microseconds>
                                          (tEnd-tStart).count();
           if (timing) std::cout << "Exc time = " <<  tt << " u Sec" << std::endl;
           fmgTime[j] += tt;
           restTime += tt;

           #ifdef DEBUG
           std::cout << "Rest+interResidual" << std::endl;
           this->printData(this->interResidualBuffer[j+1], j+1, true, true);
           #endif

           // Set error buffer at each V-Cycle in middle full multigrid
           T value = 0.0;
           try
           {
              this->queue.enqueueFillBuffer(this->errorBuffer[j+1], value, 0,
                            this->deviceWidth[j+1] * this->deviceHeight[j+1] * 
                            this->deviceDepth[j+1] * sizeof(T), NULL, NULL);
           }catch (const cl::Error& error)
           {
               std::cout << "  -> fullMultiGrid class, Problem in enqueue fill buffer" 
                         << std::endl;
               std::cout << "  -> " << getErrorString(error) << std::endl;
               exit(0);
           }

           try
           {
              this->queue.finish();
           }catch (const cl::Error& error)
           {
               std::cout << "  -> fullMultiGrid class, Problem in finishing fill buffer" 
                         << std::endl;
               std::cout << "  -> " << getErrorString(error) << std::endl;
               exit(0);
          }

          std::cout<< "==> Relaxation in level " << j + 1 << std::endl;
          tStart = std::chrono::high_resolution_clock::now();

          this->mgRelaxation->relaxing(this->queue,  
                  this->errorBuffer[j+1], this->interResidualBuffer[j+1], 
                  this->intermediateBuffer[j+1],
                  this->polynomialOrder,
                  this->volumeIntegralMatrixBuffer[j+1],
                  this->faceIntegralMatrixBuffer[j+1],
                  this->faceM11IntegralMatrixBuffer[j+1],
                  this->globalRange[j+1], this->localRange[j+1], nu_1,
                  this->deviceHeight[j+1], this->deviceWidth[j+1], this->deviceDepth[j+1],
                  this->deviceLocalSize[j+1], 
                  this->bufferOrigin, this->hostOrigin, this->region[j+1],
                  this->subt[j+1], this->event);

          this->event.wait();
          tEnd = std::chrono::high_resolution_clock::now();
          tt = std::chrono::duration_cast<std::chrono::microseconds>(tEnd-tStart).count();
          if (timing) std::cout << "Exc time = " <<  tt << " u Sec" << std::endl;
          fmgTime[j] += tt;
          relaxTime += tt;

          #ifdef DEBUG
          std::cout << "Relax+error" << std::endl;
          this->printData(this->errorBuffer[j+1], j+1, true, false); 
          #endif


          std::cout << "==> r_" << j + 1 << " = r_" << j + 1 << " - A_" << j + 1 << 
                     " * e_" << j + 1 << std::endl;
          tStart = std::chrono::high_resolution_clock::now();

          this->mgResidual->calculateResidual(this->queue, 
                  this->errorBuffer[j+1], this->interResidualBuffer[j+1],
                  this->residualBuffer[j+1], 
                  this->polynomialOrder,
                  this->volumeIntegralMatrixBuffer[j+1],
                  this->faceIntegralMatrixBuffer[j+1],
                  this->faceM11IntegralMatrixBuffer[j+1],
                  this->globalRange[j+1], this->localRange[j+1], 
                  this->deviceHeight[j+1], this->deviceWidth[j+1], this->deviceDepth[j+1],
                  this->deviceLocalSize[j+1], this->subt[j+1],
                  this->event);

          this->event.wait();
          tEnd = std::chrono::high_resolution_clock::now();
          tt = std::chrono::duration_cast<std::chrono::microseconds>(tEnd-tStart).count();
          if (timing) std::cout << "Exc time = " <<  tt << " u Sec" << std::endl;
          fmgTime[j] += tt;
          residTime += tt;

          #ifdef DEBUG
          std::cout << "residual+residual" << std::endl;
          this->printData(this->residualBuffer[j+1], j+1, true, true);
          #endif
          #ifndef DEBUG 
          this->printData(this->residualBuffer[j+1], j+1, false, true);
          #endif
      }

      // go up to finest grid from coursest grid in last deep V-Cycle
      if (mgLevel == 0)
      {
         for(j = this->multiGridLevels; j > mgLevel; --j)
         {
           // do prolongation
           
           if (j - 1 == 0)
           {
              std::cout << "==> Cubic Interpolation, E_" << j - 1 << " <-- e_" << j << 
                 ",\tx =  E_" << j - 1 << " + x"  << std::endl;
              tStart = std::chrono::high_resolution_clock::now();

              this->mgProlongation->cubicInterpolation(this->context, this->queue, 
                  this->errorBuffer[j], this->xBuffer,
                  this->intermediateBuffer[j-1],
                  this->restProlGlobalRange[j], this->localRange[j],
                  this->restProlGlobalRange[j-1], this->localRange[j-1],
                  this->deviceHeight[j], this->deviceWidth[j], this->deviceDepth[j],
                  this->deviceHeight[j-1], this->deviceWidth[j-1], this->deviceDepth[j-1],
                  this->deviceLocalSize[j-1], this->subt[j-1], this->event);
           
              this->event.wait();

              tEnd = std::chrono::high_resolution_clock::now();
              tt = std::chrono::duration_cast<std::chrono::microseconds>
                                                (tEnd-tStart).count();

              if (timing) std::cout << "Exc time = " <<  tt << " u Sec" << std::endl;
              fmgTime[j] += tt;
              prolTime += tt;

              std::cout<< "==> Relaxation in level " << j - 1 << std::endl;
              tStart = std::chrono::high_resolution_clock::now();

              this->mgRelaxation->relaxing(this->queue,
                  this->xBuffer, this->bBuffer, 
                  this->intermediateBuffer[j-1],
                  this->polynomialOrder,
                  this->volumeIntegralMatrixBuffer[j-1],
                  this->faceIntegralMatrixBuffer[j-1],
                  this->faceM11IntegralMatrixBuffer[j-1],
                  this->globalRange[j-1], this->localRange[j-1], nu_2, 
                  this->deviceHeight[j-1], this->deviceWidth[j-1], this->deviceDepth[j-1],
                  this->deviceLocalSize[j-1], 
                  this->bufferOrigin, this->hostOrigin, this->region[j-1], 
                  this->subt[j-1], this->event);
            
             this->event.wait();

             tEnd = std::chrono::high_resolution_clock::now();
             tt = std::chrono::duration_cast<std::chrono::microseconds>
                                                (tEnd-tStart).count();

             if (timing) std::cout << "Exc time = " <<  tt << " u Sec" << std::endl;
             fmgTime[j] += tt;
             relaxTime += tt;

             std::cout << "==> r_" << j - 1 << " = b - A_" << j - 1 << 
                      " * x" << std::endl;
             tStart = std::chrono::high_resolution_clock::now();

             this->mgResidual->calculateResidual(this->queue, 
                  this->xBuffer, this->bBuffer, 
                  this->residualBuffer[j-1], 
                  this->polynomialOrder,
                  this->volumeIntegralMatrixBuffer[j-1],
                  this->faceIntegralMatrixBuffer[j-1],
                  this->faceM11IntegralMatrixBuffer[j-1],
                  this->globalRange[j-1], this->localRange[j-1], 
                  this->deviceHeight[j-1], this->deviceWidth[j-1], this->deviceDepth[j-1],
                  this->deviceLocalSize[j-1], 
                  this->subt[j-1], this->event);

              this->event.wait();

              tEnd = std::chrono::high_resolution_clock::now();
              tt = std::chrono::duration_cast<std::chrono::microseconds>
                                                  (tEnd-tStart).count();
              if (timing) std::cout << "Exc time = " <<  tt << " u Sec" << std::endl;
              //fmgTime[j] += tt;
              residTime += tt;

              #ifdef DEBUG
              std::cout << "residual+residual" << std::endl;
              normResidualEnd =  this->printData(this->residualBuffer[j-1], 
                                                 j - 1, true, true);
              #endif
              #ifndef DEBUG
              normResidualEnd = this->printData(this->residualBuffer[j-1], 
                                                j - 1, false, true);
              #endif
            } else
            {
               std::cout << "==> linear interpolation, E_" << j - 1 << " <-- e_" << j << 
                 ",\te_" << j - 1 << " =  E_" << j - 1 << " + e_"<< j - 1  << std::endl;
               tStart = std::chrono::high_resolution_clock::now();

               this->mgProlongation->linearInterpolation(this->queue, 
                  this->errorBuffer[j], this->errorBuffer[j-1],
                  this->intermediateBuffer[j-1],
                  this->restProlGlobalRange[j], this->localRange[j],
                  this->restProlGlobalRange[j-1], this->localRange[j-1],
                  this->deviceHeight[j], this->deviceWidth[j], this->deviceDepth[j],
                  this->deviceHeight[j-1], this->deviceWidth[j-1], this->deviceDepth[j-1],
                  this->deviceLocalSize[j-1], this->subt[j-1], this->event);
           
               this->event.wait();

               tEnd = std::chrono::high_resolution_clock::now();
               tt = std::chrono::duration_cast<std::chrono::microseconds>
                                                   (tEnd-tStart).count();
               if (timing) std::cout << "Exc time = " <<  tt << " u Sec" << std::endl;
               fmgTime[j] += tt;
               prolTime += tt;

               #ifdef DEBUG
               std::cout << "prol+error" << std::endl;
               this->printData(this->errorBuffer[j-1], j-1, true, false);
               #endif

               std::cout<< "==> Relaxation in level " << j - 1 << std::endl;
               tStart = std::chrono::high_resolution_clock::now();

               this->mgRelaxation->relaxing(this->queue, 
                  this->errorBuffer[j-1], this->residualBuffer[j-1], 
                  this->intermediateBuffer[j-1],
                  this->polynomialOrder,
                  this->volumeIntegralMatrixBuffer[j-1],
                  this->faceIntegralMatrixBuffer[j-1],
                  this->faceM11IntegralMatrixBuffer[j-1],
                  this->globalRange[j-1], this->localRange[j-1], nu_2, 
                  this->deviceHeight[j-1], this->deviceWidth[j-1], this->deviceDepth[j-1],
                  this->deviceLocalSize[j-1], 
                  this->bufferOrigin, this->hostOrigin, this->region[j-1], 
                  this->subt[j-1], this->event);
            
               this->event.wait();

               tEnd = std::chrono::high_resolution_clock::now();
               tt = std::chrono::duration_cast<std::chrono::microseconds>
                                                  (tEnd-tStart).count();
               if (timing) std::cout << "Exc time = " <<  tt << " u Sec" << std::endl;
               fmgTime[j] += tt;
               relaxTime += tt;

               #ifdef DEBUG
               std::cout << "relax+error" << std::endl;
               this->printData(this->errorBuffer[j-1], j-1, true, false);
               #endif

/*               std::cout << "==> r_" << j - 1 << " = b - A_" << j - 1 << 
                      " * x" << std::endl;
               tStart = std::chrono::high_resolution_clock::now();
              this->mgResidual->calculateResidual(this->queue, this->errorBuffer[j-1],this->residualBuffer[j-1], 
                            this->interResidualBuffer[j-1], this->matrixBuffer[j-1],
                            this->globalRange[j-1], this->localRange[j-1], 
                            this->deviceHeight[j-1], this->deviceWidth[j-1], this->deviceDepth[j-1],
                            this->deviceLocalSize[j-1], this->spatialStepSize[j-1], this->subt[j-1], this->event);
               this->event.wait();
               this->copyBuffer(this->interResidualBuffer[j-1], this->residualBuffer[j-1], j-1);
               this->event.wait();
               tEnd = std::chrono::high_resolution_clock::now();
               tt = std::chrono::duration_cast<std::chrono::microseconds>(tEnd-tStart).count();
               if (timing) std::cout << "Exc time = " <<  tt << " u Sec" << std::endl;
               //fmgTime[j] += tt;
               residTime += tt;

               #ifdef DEBUG
               std::cout << "residual+residual" << std::endl;
               this->printData(this->residualBuffer[j-1], j-1, true, false);
               #endif
               #ifndef DEBUG
               this->printData(this->residualBuffer[j-1], j-1, false, true);
               #endif
  */         }
        }
     }
  }
  std::cout<< "\n==> Full Multigrid process was done!\n " << std::endl;

  double Seconds = 0.0;
  for (j = 0; j <= this->multiGridLevels; ++j)
  {
    Seconds += fmgTime[j];
  }

  this->readDataFromDevice();

  this->calcError(x_exact, this->x, error);
  
  double normErrorEnd = this->norm_L2(error, this->domainHeight[0] * this->domainWidth[0]                 * this->domainDepth[0], this->spatialStepSize[0]);

  std::cout << "   ->||e||_L2 = " << normErrorEnd << std::endl;

  if(plotting) this->mgGnuplot->plot(error, this->domainHeight[0], this->domainWidth[0],
                        this->domainDepth[0], "error", 0);

  this->printResult(Seconds, normResidualEnd / normResidualInit, 
       normErrorEnd / normErrorInit, "Full Multigrid", 
       this->relaxationMethod, this->omega, nu_1, nu_2, 1);

  if(timing)
  {
    for (j = 0; j <= this->multiGridLevels; ++j)
    {
       std::cout << "Time in level " << j << " = " << fmgTime[j] << " u sec " << std::endl;
    }

    std::cout << "Time for prolongation = " << prolTime << " u sec" << std::endl;
    std::cout << "Time for restriction = " << restTime << " u sec" <<std::endl;
    std::cout << "Time for residual calculation = " << residTime << " u sec" << std::endl;
    std::cout << "Time for relaxation = " << relaxTime << " u sec " <<std::endl;
    std::cout << std::setfill(' ') << std::setw(25);
    std::cout << std::left << "Prolongation time: ";
    std::cout << std::right  << this->mgProlongation->getTime() << " u sec" << std::endl;
    std::cout << std::setfill(' ') << std::setw(25);
    std::cout << std::left << "Restriction time: ";
    std::cout << std::right  << this->mgRestriction->getTime() << " u sec" << std::endl;
    std::cout << std::setfill(' ') << std::setw(25);
    std::cout << std::left << "Residual time: ";
    std::cout << std::right  << this->mgResidual->getTime() << " u sec" << std::endl;
    std::cout << std::setfill(' ') << std::setw(25);
    std::cout << std::left << "Relaxation time: ";
    std::cout << std::right  << this->mgRelaxation->getTime() << " u sec" << std::endl;

  }
}


