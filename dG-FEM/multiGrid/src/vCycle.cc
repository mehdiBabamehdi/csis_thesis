/*-----------------------------------------------------------------------*\
           ___________________________________________________
          /                                                  /|
         /                                                  / |
        |--------------------------------------------------|  |
        |    ______ __     __  ______    _____  __   __    |  |
        |   |   ___|  \   /  |/   ___|  |  _  \|  | |  |   |  |
        |   |  |_  |   \_/   |   /      | |_)  |  | |  |   |  |
        |   |    | |         |  |   ____|   __/|  | |  |   |  |
        |   |   _| |   ___   |  |  |_  _|  |   |  |_|  |   |  |
        |   |  |   |  |   |  |   \__/ / |  |   |       |   |  |
        |   |__|   |__|   |__|\______/  |__|    \_____/    |  |
        |                                                  |  |
        |      Full      Multigrid      On     GPGPU       | /
        |__________________________________________________|/

--------------------------------------------------------------------------

Author

    Mehdi Baba Mehdi
    mehdi.baba_mehdi@uni-wuppertal.de

Date  
    Wed 03.05.2020
\*-----------------------------------------------------------------------*/


#include "../includes/vCycle.hpp"

template <class T>
vCycle<T>::vCycle( T* _RHS,
           int _origCellNo_x,  int _origCellNo_y,  int _origCellNo_z,
          std::string _relaxationMethod, T _omega,   
          cl_device_type _deviceType, int _deviceID, 
          int _nu_1, int _nu_2, bool _plotting, int _maxVCycleSteps)
          :
          multiGrid<T>(_RHS, _origCellNo_x, _origCellNo_y, _origCellNo_z, 
                       _relaxationMethod, _omega, _deviceType, _deviceID),
          nu_1{_nu_1},
          nu_2{_nu_2},
          plotting{_plotting},
          maxVCycleSteps{_maxVCycleSteps}
{
}


template <class T>
vCycle<T>::vCycle(std::ifstream _RHSFILENAME,
           int _origCellNo_x,  int _origCellNo_y,  int _origCellNo_z,
          std::string _relaxationMethod, T _omega,   
          cl_device_type _deviceType, int _deviceID, 
          int _nu_1, int _nu_2, bool _plotting, int _maxVCycleSteps)
          :
          multiGrid<T>(_RHSFILENAME, _origCellNo_x, _origCellNo_y, _origCellNo_z, 
                       _relaxationMethod, _omega, _deviceType, _deviceID),
          nu_1{_nu_1},
          nu_2{_nu_2},
          plotting{_plotting},
          maxVCycleSteps{_maxVCycleSteps}
{
}


template <class T>
vCycle<T>::vCycle( int _origCellNo_x,  int _origCellNo_y, 
            int _origCellNo_z,
           std::string _relaxationMethod, T _omega,   
           cl_device_type _deviceType, int _deviceID, 
           int _nu_1, int _nu_2, bool _plotting, int _maxVCycleSteps)
           :
           multiGrid<T>(_origCellNo_x, _origCellNo_y, _origCellNo_z, 
                        _relaxationMethod, _omega, _deviceType, _deviceID),
           nu_1{_nu_1},
           nu_2{_nu_2},
           plotting{_plotting},
           maxVCycleSteps{_maxVCycleSteps}
{
}

template <class T>
vCycle<T>::vCycle( multiGrid<T>& _vcycMultiGrid,
           int _nu_1, int _nu_2, bool _plotting, int _maxVCycleSteps)
           :
           multiGrid<T>(_vcycMultiGrid),
           nu_1{_nu_1},
           nu_2{_nu_2},
           plotting{_plotting},
           maxVCycleSteps{_maxVCycleSteps}

{
}


template <class T>
vCycle<T>::vCycle(int _nu_1, int _nu_2, bool _plotting, int _maxVCycleSteps)
           :
           multiGrid<T>(),
           nu_1{_nu_1},
           nu_2{_nu_2},
           plotting{_plotting},
           maxVCycleSteps{_maxVCycleSteps}
{
}


template <class T>
vCycle<T>::~vCycle()
{
  this->volumeIntegralMatrixBuffer.clear();
  this->residualBuffer.clear();
  this->errorBuffer.clear();
  this->globalRange.clear();
  this->localRange.clear();
  this->restProlGlobalRange.clear();
  this->deviceWidth.clear();
  this->deviceHeight.clear();
  this->deviceDepth.clear();
  this->deviceDataSize.clear();
  this->domainWidth.clear();
  this->domainHeight.clear();
  this->domainDepth.clear();
  delete[] this->residual_h; 
  delete[] this->x; 
  delete[] this->RHS; 
}

  

template<class T>
void vCycle<T>::solve()
{
  this->setDomainSize();
  
  vcycTime = new double[this->multiGridLevels + 1]();

  int j;

  T* x_exact = new T[this->domainWidth[0] * this->domainHeight[0] * this->domainDepth[0]];
  T* error   = new T[this->domainWidth[0] * this->domainHeight[0] * this->domainDepth[0]];

 
  /* Initialization of device and buffers */
  this->buildProgram(this->omega);
  this->createBuffers();
  this->setNDRange();
  this->writeDataToDevice();
  
  this->mgRelaxation   = new relaxation<T>(this->program, this->queue, this->context, 
                                           this->relaxationMethod, this->omega);
  this->mgResidual     = new residual<T>(this->program);
  this->mgRestriction  = new restriction<T>(this->program, this->queue, this->context, 
                                            this->nodePerCellPerDir);
  this->mgProlongation = new prolongation<T>(this->program, this->queue, this->context,
                                             this->nodePerCellPerDir);
  this->mgGnuplot      = new gnuplot<T>();

  this->printSettingData("V-Cycle");
    
  std::cout << "__________________________________________________\n" << std::endl;

  this->calcX_exact(x_exact);
  this->calcError(x_exact, this->x, error);

  double normXexact = this->norm_L2(x_exact, this->domainHeight[0] * this->domainWidth[0]                     * this->domainDepth[0], this->spatialStepSize[0]);

  double normErrorInit = this->norm_L2(error, this->domainHeight[0] * this->domainWidth[0]                       * this->domainDepth[0], this->spatialStepSize[0]);

  std::cout << "   ->||e||_L2 = " << normErrorInit  << std::endl;
 
  if(plotting) this->mgGnuplot->plot(error, this->domainHeight[0], this->domainWidth[0],
                                            this->domainDepth[0], "error", 0);
  
  this->mgResidual->calculateResidual(this->queue, this->xBuffer, this->bBuffer, 
                       this->residualBuffer[0],
                       this->polynomialOrder,
                       this->volumeIntegralMatrixBuffer[0],
                       this->M11Buffer[0],
                       this->M12Buffer[0],
                       this->M21Buffer[0],
                       this->M22Buffer[0],
                       this->globalRange[0], this->localRange[0], 
                       this->domainCellNo[0],
                       this->event);
  
  //this->printData(this->residualBuffer[0], 0, true, false);

  this->event.wait();
  double normResidualInit = this->printData(this->residualBuffer[0], 0, false, false);
  std::cout << "   ->||r||_L2 = " << normResidualInit  << std::endl;
  double normErrorEnd;

  std::cout<< "\n\t >>> Start V-Cycle Multigrid Process <<<\n" << std::endl;
  while(true)
  {
    vCycleStep++;
    
    std::cout << "V-Cycle step = " << vCycleStep << std::endl;

    vCycleAlgorithm();
 
    this->readDataFromDevice();

    this->calcError(x_exact, this->x, error);
    normErrorEnd = this->norm_L2(error, this->domainHeight[0] * 
                                 this->domainWidth[0] * this->domainDepth[0], 
                                 this->spatialStepSize[0]);

    std::cout << "   ->||e||_L2 = " << normErrorEnd  << std::endl;

    if ((normErrorEnd < this->tol) || vCycleStep == maxVCycleSteps)
    {
       break;
    } else
    {
      for (int i = 0; i < this->multiGridLevels; ++i)
      {
         
        T value = 0.0;
        try
        {
           this->queue.enqueueFillBuffer(this->errorBuffer[i], value, 0,
                                         this->deviceWidth[i] * this->deviceHeight[i] * 
                                         this->deviceDepth[i] * sizeof(T), NULL, NULL);
        }catch (const cl::Error& error)
        {
           std::cout << "  -> fullMultiGrid class, Problem in enqueue fill buffer" << 
                        std::endl;
           std::cout << "  -> " << getErrorString(error) << std::endl;
           exit(0);
        }
   
        try
        {
           this->queue.finish();
        }catch (const cl::Error& error)
        {
           std::cout << "  -> fullMultiGrid class, Problem in finishing fill buffer" << 
                        std::endl;
           std::cout << "  -> " << getErrorString(error) << std::endl;
           exit(0);
        }
      }
    }
  }
 
  double Seconds = 0; 
  for (j = 0; j <= this->multiGridLevels; ++j)
  {
     Seconds += vcycTime[j];
  }

  if(plotting) this->mgGnuplot->plot(error, this->domainHeight[0], this->domainWidth[0],
                                            this->domainDepth[0], "error", 0);

  double normResidualEnd = this->printData(this->residualBuffer[0], 0, false, true);

  this->printResult(Seconds, normResidualEnd / normResidualInit, 
                    normErrorEnd / normErrorInit, "V-Cycle", 
                    this->relaxationMethod, this->omega, nu_1, nu_2, vCycleStep);
  
  if(timing)
  {
    for (j = 0; j <= this->multiGridLevels; ++j)
    {
       std::cout << "Time in level " << j << " = " << vcycTime[j] << " u sec " << 
                    std::endl;
    }

    std::cout << "Time for prolongation = " << prolTime << " u sec" << std::endl;
    std::cout << "Time for restriction = " << restTime << " u sec" <<std::endl;
    std::cout << "Time for residual calculation = " << residTime << " u sec" << std::endl;
    std::cout << "Time for relaxation = " << relaxTime << " u sec " <<std::endl;
    std::cout << std::setfill(' ') << std::setw(25);
    std::cout << std::left << "Prolongation time: ";
    std::cout << std::right  << this->mgProlongation->getTime() << " u sec" << std::endl;
    std::cout << std::setfill(' ') << std::setw(25);
    std::cout << std::left << "Restriction time: ";
    std::cout << std::right  << this->mgRestriction->getTime() << " u sec" << std::endl;
    std::cout << std::setfill(' ') << std::setw(25);
    std::cout << std::left << "Residual time: ";
    std::cout << std::right  << this->mgResidual->getTime() << " u sec" << std::endl;
    std::cout << std::setfill(' ') << std::setw(25);
    std::cout << std::left << "Relaxation time: ";
    std::cout << std::right  << this->mgRelaxation->getTime() << " u sec" << std::endl;

  }
}



template<class T>
void vCycle<T>::vCycleAlgorithm()
{

  int j;
    
  auto tStart = std::chrono::high_resolution_clock::now();
  std::cout<< "==> Relaxation in level " << 0 << std::endl;
  this->mgRelaxation->relaxing(this->queue, 
                      this->xBuffer, this->bBuffer, 
                      this->intermediateBuffer[0],
                      this->polynomialOrder,
                      this->volumeIntegralMatrixBuffer[0],
                      this->M11Buffer[0],
                      this->M12Buffer[0],
                      this->M21Buffer[0],
                      this->M22Buffer[0],
                      this->globalRange[0], this->localRange[0], nu_1, 
                      this->domainCellNo[0],
                      this->bufferOrigin, this->hostOrigin, this->region[0],
                      this->event);
  
  this->event.wait();

  //  std::cout << "X" << std::endl;      
  //  this->printData(this->xBuffer, 0, true, false);

  auto tEnd = std::chrono::high_resolution_clock::now();
  double tt = std::chrono::duration_cast<std::chrono::microseconds>(tEnd-tStart).count();
  if (timing) std::cout << "Exc time = " <<  tt << " u Sec" << std::endl;
  vcycTime[0] += tt;
  relaxTime += tt;

  tStart = std::chrono::high_resolution_clock::now();
  std::cout<< "==> Calculation Residual in level " << 0 << std::endl;
  this->mgResidual->calculateResidual(this->queue, this->xBuffer, this->bBuffer, 
                   this->residualBuffer[0], 
                   this->polynomialOrder,
                   this->volumeIntegralMatrixBuffer[0],
                   this->M11Buffer[0],
                   this->M12Buffer[0],
                   this->M21Buffer[0],
                   this->M22Buffer[0],
                   this->globalRange[0], this->localRange[0], 
                   this->domainCellNo[0],
                   this->event);

  this->event.wait();

  tEnd = std::chrono::high_resolution_clock::now();
  tt = std::chrono::duration_cast<std::chrono::microseconds>(tEnd-tStart).count();
  if (timing) std::cout << "Exc time = " <<  tt << " u Sec" << std::endl;
  vcycTime[0] += tt;
  residTime += tt;

  #ifdef DEBUG
  std::cout << "residual" << std::endl;      
  this->printData(this->residualBuffer[0], 0, true, true);
  #endif
  #ifndef DEBUG
  this->printData(this->residualBuffer[0], 0, false, true);
  #endif

   /*
   * <- prolongation
   * -> restriction
   */  
  // Go down to coursest grid in first step
  for(j = 0; j < this->multiGridLevels; ++j)
  { 
    // do restriction
    tStart = std::chrono::high_resolution_clock::now();
    std::cout << "==> Restriction, " << j << " --> " << j + 1 << std::endl;
    this->mgRestriction->doRestriction(this->queue, this->residualBuffer[j],
            this->interResidualBuffer[j+1],
            this->restProlGlobalRange[j + 1], this->localRange[j + 1],
            this->domainCellNo[j + 1], 
            this->event);

    this->event.wait();

    tEnd = std::chrono::high_resolution_clock::now();
    tt = std::chrono::duration_cast<std::chrono::microseconds>(tEnd-tStart).count();
    if (timing) std::cout << "Exc time = " <<  tt << " u Sec" << std::endl;
    vcycTime[j] += tt;
    restTime += tt;

    #ifdef DEBUG
    std::cout << "Rest+interResidual" << std::endl;
    this->printData(this->interResidualBuffer[j+1], j+1, true, true);
    #endif

    tStart = std::chrono::high_resolution_clock::now();
    std::cout<< "==> Relaxation in level " << j + 1 << std::endl;
    this->mgRelaxation->relaxing(this->queue,
                  this->errorBuffer[j+1], this->interResidualBuffer[j+1],
                  this->intermediateBuffer[j+1],
                  this->polynomialOrder,
                  this->volumeIntegralMatrixBuffer[j+1],
                  this->M11Buffer[j+1],
                  this->M12Buffer[j+1],
                  this->M21Buffer[j+1],
                  this->M22Buffer[j+1],
                  this->globalRange[j+1], this->localRange[j+1], nu_1,
                  this->domainCellNo[j+1], 
                  this->bufferOrigin, this->hostOrigin, this->region[j+1],
                  this->event);

    this->event.wait();

    
    tEnd = std::chrono::high_resolution_clock::now();
    tt = std::chrono::duration_cast<std::chrono::microseconds>(tEnd-tStart).count();
    if (timing) std::cout << "Exc time = " <<  tt << " u Sec" << std::endl;
    vcycTime[j+1] += tt;
    relaxTime += tt;

    #ifdef DEBUG
    std::cout << "Relax+error" << std::endl;
    this->printData(this->errorBuffer[j+1], j+1, true, false);
    #endif

    tStart = std::chrono::high_resolution_clock::now();
    std::cout << "==> r_" << j + 1 << " = r_" << j + 1 << " - A_" << j + 1 <<
                      " * e_" << j + 1 << std::endl;
    this->mgResidual->calculateResidual(this->queue, this->errorBuffer[j+1],
                 this->interResidualBuffer[j+1],
                 this->residualBuffer[j+1], 
                 this->polynomialOrder,
                 this->volumeIntegralMatrixBuffer[j+1],
                 this->M11Buffer[j+1],
                 this->M12Buffer[j+1],
                 this->M21Buffer[j+1],
                 this->M22Buffer[j+1],
                 this->globalRange[j+1], this->localRange[j+1],
                 this->domainCellNo[j+1], 
                 this->event);

    this->event.wait();
    tEnd = std::chrono::high_resolution_clock::now();
    tt = std::chrono::duration_cast<std::chrono::microseconds>(tEnd-tStart).count();
    if (timing) std::cout << "Exc time = " <<  tt << " u Sec" << std::endl;
    vcycTime[j+1] += tt;
    residTime += tt;
    
    #ifdef DEBUG
    std::cout << "residual+residual" << std::endl;
    this->printData(this->residualBuffer[j+1], j+1, true, true);
    #endif
    #ifndef DEBUG
    this->printData(this->residualBuffer[j+1], j+1, false, true);
    #endif
  }
      
  // go up to finest grid from coursest grid
  for(j = this->multiGridLevels; j > 0; --j)
  {
    // do prolongation
           
    if (j - 1 == 0)
      {
        tStart = std::chrono::high_resolution_clock::now();
        std::cout << "==> Prolongation, E_" << j - 1 << " <-- e_" << j <<
                 ",\tx =  E_" << j - 1 << " + x"  << std::endl;
        this->mgProlongation->linearInterpolation(this->queue,
                 this->errorBuffer[j], this->xBuffer,
                 this->restProlGlobalRange[j], this->localRange[j],
                 this->domainCellNo[j],
                 this->event);
           
        this->event.wait();

        tEnd = std::chrono::high_resolution_clock::now();
        tt = std::chrono::duration_cast<std::chrono::microseconds>(tEnd-tStart).count();
        if (timing) std::cout << "Exc time = " <<  tt << " u Sec" << std::endl;
        vcycTime[j] += tt;
        prolTime += tt;
        
        tStart = std::chrono::high_resolution_clock::now();
        std::cout<< "==> Relaxation in level " << j - 1 << std::endl;
        this->mgRelaxation->relaxing(this->queue,
                 this->xBuffer, this->bBuffer,
                 this->intermediateBuffer[j-1],
                 this->polynomialOrder,
                 this->volumeIntegralMatrixBuffer[j-1],
                 this->M11Buffer[j-1],
                 this->M12Buffer[j-1],
                 this->M21Buffer[j-1],
                 this->M22Buffer[j-1],
                 this->globalRange[j-1], this->localRange[j-1], nu_2,
                 this->domainCellNo[j-1], 
                 this->bufferOrigin, this->hostOrigin, this->region[j-1], 
                 this->event);
            
        this->event.wait();

        tEnd = std::chrono::high_resolution_clock::now();
        tt = std::chrono::duration_cast<std::chrono::microseconds>(tEnd-tStart).count();
        if (timing) std::cout << "Exc time = " <<  tt << " u Sec" << std::endl;
        vcycTime[j-1] += tt;
        relaxTime += tt;

        tStart = std::chrono::high_resolution_clock::now();
        std::cout << "==> r_" << j - 1 << " = b - A_" << j - 1 <<
                      " * x" << std::endl;
        this->mgResidual->calculateResidual(this->queue, this->xBuffer, this->bBuffer,
                this->residualBuffer[j-1], 
                this->polynomialOrder,
                this->volumeIntegralMatrixBuffer[j-1],
                this->M11Buffer[j-1],
                this->M12Buffer[j-1],
                this->M21Buffer[j-1],
                this->M22Buffer[j-1],
                this->globalRange[j-1], this->localRange[j-1],
                this->domainCellNo[j-1],
                this->event);

        this->event.wait();

        tEnd = std::chrono::high_resolution_clock::now();
        tt = std::chrono::duration_cast<std::chrono::microseconds>(tEnd-tStart).count();
        if (timing) std::cout << "Exc time = " <<  tt << " u Sec" << std::endl;
        vcycTime[j-1] += tt;
        residTime += tt;

        #ifdef DEBUG
        std::cout << "residual+residual" << std::endl;
        this->printData(this->residualBuffer[j-1], j-1, true, true);
        #endif
        #ifndef DEBUG
        this->printData(this->residualBuffer[j-1], j-1, false, true);
        #endif
     } else
     {
        std::cout << "==> Prolongation, E_" << j - 1 << " <-- e_" << j <<
                 ",\te_" << j - 1 << " =  E_" << j - 1 << " + e_"<< j - 1  << std::endl;
        tStart = std::chrono::high_resolution_clock::now();

         this->mgProlongation->linearInterpolation(this->queue,
                 this->errorBuffer[j], this->errorBuffer[j-1],
                 this->restProlGlobalRange[j], this->localRange[j],
                 this->domainCellNo[j], 
                 this->event);
           
         this->event.wait();

         tEnd = std::chrono::high_resolution_clock::now();
         tt = std::chrono::duration_cast<std::chrono::microseconds>(tEnd-tStart).count();
         if (timing) std::cout << "Exc time = " <<  tt << " u Sec" << std::endl;
         vcycTime[j] += tt;
         prolTime += tt;

         #ifdef DEBUG
         std::cout << "prol+error" << std::endl;
         this->printData(this->errorBuffer[j-1], j-1, true, false);
         #endif

         tStart = std::chrono::high_resolution_clock::now();
         std::cout<< "==> Relaxation in level " << j - 1 << std::endl;
         this->mgRelaxation->relaxing(this->queue,
                 this->errorBuffer[j-1], this->residualBuffer[j-1],
                 this->intermediateBuffer[j-1],
                 this->polynomialOrder,
                 this->volumeIntegralMatrixBuffer[j-1],
                 this->M11Buffer[j-1],
                 this->M12Buffer[j-1],
                 this->M21Buffer[j-1],
                 this->M22Buffer[j-1],
                 this->globalRange[j-1], this->localRange[j-1], nu_2,
                 this->domainCellNo[j-1], 
                 this->bufferOrigin, this->hostOrigin, this->region[j-1], 
                 this->event);
            
         this->event.wait();
 
         tEnd = std::chrono::high_resolution_clock::now();
         tt = std::chrono::duration_cast<std::chrono::microseconds>(tEnd-tStart).count();
         if (timing) std::cout << "Exc time = " <<  tt << " u Sec" << std::endl;
         vcycTime[j-1] += tt;
         relaxTime += tt;

         #ifdef DEBUG
         std::cout << "relax+error" << std::endl;
         this->printData(this->errorBuffer[j-1], j-1, true, false);
         #endif

         tStart = std::chrono::high_resolution_clock::now();
         std::cout << "==> r_" << j - 1 << " = r_" << j - 1 << " - A_" << j - 1 <<
                      " * e_" << j - 1 << std::endl;
         this->mgResidual->calculateResidual(this->queue, this->errorBuffer[j-1],
                this->residualBuffer[j-1],
                this->interResidualBuffer[j-1], 
                this->polynomialOrder,
                this->volumeIntegralMatrixBuffer[j-1],
                this->M11Buffer[j-1],
                this->M12Buffer[j-1],
                this->M21Buffer[j-1],
                this->M22Buffer[j-1],
                this->globalRange[j-1], this->localRange[j-1],
                this->domainCellNo[j-1], 
                this->event);

         this->event.wait();

         this->copyBuffer(this->interResidualBuffer[j-1], this->residualBuffer[j-1], j-1);
         this->event.wait();

 
         tEnd = std::chrono::high_resolution_clock::now();
         tt = std::chrono::duration_cast<std::chrono::microseconds>(tEnd-tStart).count();
         if (timing) std::cout << "Exc time = " <<  tt << " u Sec" << std::endl;
         //vcycTime[j-1] += tt;
         //residTime += tt;

         #ifdef DEBUG
         std::cout << "residual+interResidual" << std::endl;
         this->printData(this->residualBuffer[j-1], j-1, true, false);
         #endif
         #ifndef DEBUG
         this->printData(this->residualBuffer[j-1], j-1, false, true);
         #endif
     }
  }
  
  std::cout<< "\n==> V-Cycle Multigrid process was done!\n " << std::endl;
}
