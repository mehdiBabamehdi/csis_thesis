
/*-----------------------------------------------------------------------*\
 	      ___________________________________________________
	     /                                                  /|
	    /__________________________________________________/ |
	   |                                                  |  |
	   |      __  _______     _______    _____  __   __   |  |
	   |     |  |/   ____|   /   ____|  |  _  \|  | |  |  |  |
	   |     |  |   /       |   /       | |_)  |  | |  |  |  |
	   |   __|  |  |   _____|  |   _____|   __/|  | |  |  |  |
	   |  /  _  |  |  |_   _|  |  |_   _|  |   |  |_|  |  |  |
	   | |  (_| |   \__/  / |   \__/  / |  |   |       |  |  |
	   |  \_____|\_______/   \_______/  |__|    \_____/   |  |
	   |                                                  |  |
           |     discontinuous Galerkin    On      GPGPU      | /
	   |__________________________________________________|/

--------------------------------------------------------------------------

Author

    Mehdi Baba Mehdi
    mehdi.baba_mehdi@uni-wuppertal.de

Date  
    Wed 08.09.2020
\*-----------------------------------------------------------------------*/
/*
 * NOTE: T is type defined as preprocesor in "multigrid class" when openCL
 *       program is build. It automatically gets the type of template.
 *
-------------------------------------------------------------------------*/

__global int node_i[64] = {2, 3, 4, 5, 2, 2, 3, 4, 3, 4, 5, 5, 2, 2, 2, 3, 4, 3, 4, 5, 5, 
                           3, 4, 3, 4, 3, 4, 3, 4, 2, 2, 2, 2, 5, 5, 5, 5, 2, 2, 2, 3, 4, 
                           3, 4, 3, 4, 5, 5, 5, 2, 2, 3, 4, 3, 4, 3, 4, 1, 1, 1, 2, 3, 4, 
                           5};

__global int node_j[64] = {2, 2, 2, 2, 3, 4, 3, 3, 4, 4, 3, 4, 2, 2, 2, 2, 2, 2, 2, 2, 2, 
                           3, 3, 4, 4, 3, 3, 4, 4, 3, 4, 3, 4, 3, 4, 3, 4, 5, 5, 5, 5, 5, 
                           5, 5, 5, 5, 5, 5, 1, 3, 4, 3, 3, 4, 4, 2, 2, 5, 3, 4, 5, 5, 5, 
                           5};

__global int node_k[64] = {2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 4, 5, 3, 3, 4, 4, 3, 4,
                           3, 3, 3, 3, 4, 4, 4, 4, 3, 3, 4, 4, 3, 3, 4, 4, 3, 4, 2, 3, 3,
                           4, 4, 2, 2, 3, 4, 2, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5,
                           5};


__global int adjacentCell_z[faceNo+1] = {-1, 0, 0, 0, 0, 0, 1}; 
__global int adjacentCell_x[faceNo+1] = {0 ,-1, 0, 0, 0, 1, 0}; 
__global int adjacentCell_y[faceNo+1] = {0 , 0,-1, 0, 1, 0, 0}; 



/*
 * The kernel used to implement Jacobi iterative scheme
 */
__kernel void jacobi(__global T* x, 
                     int maxCellNo_x, 
                     int maxCellNo_y, 
                     int maxCellNo_z,
                     __global T* volumeIntegralMatrix, 
                     __global T* M11_1,
                     __global T* M12_1,
                     __global T* M21_1,
                     __global T* M22_1,
                     __global T* M11_2,
                     __global T* M12_2,
                     __global T* M21_2,
                     __global T* M22_2,
                     __global T* M11_3,
                     __global T* M12_3,
                     __global T* M21_3,
                     __global T* M22_3,
                     __global T* M11_4,
                     __global T* M12_4,
                     __global T* M21_4,
                     __global T* M22_4,
                     __global T* M11_5,
                     __global T* M12_5,
                     __global T* M21_5,
                     __global T* M22_5,
                     __global T* M11_6,
                     __global T* M12_6,
                     __global T* M21_6,
                     __global T* M22_6,
                     __global T* b,
                     __global T* y,
                     __local T* local_x, 
                     int localHeight, 
                     int localWidth, 
                     int localDepth,
                     int cellInLocalMem)

{
   int nodePerCellPerDir = polynomialOrder + 1;

   // size of work group in output
   int groupStartCol   = get_group_id(0) * get_local_size(0);
   int groupStartRow   = get_group_id(1) * get_local_size(1);
   int groupStartSlice = get_group_id(2) * get_local_size(2);

   // local ID for work-items
   int localCol   = get_local_id(0);
   int localRow   = get_local_id(1);
   int localSlice = get_local_id(2);

   // global ID of each work-item
   int globalCol   = groupStartCol   + localCol;
   int globalRow   = groupStartRow   + localRow;
   int globalSlice = groupStartSlice + localSlice;
   
   // global cell No. in each dir
   int cellNo_z = (int) globalSlice / nodePerCellPerDir;
   int cellNo_x = (int) globalRow   / nodePerCellPerDir;
   int cellNo_y = (int) globalCol   / nodePerCellPerDir;


   //* * * * * * * * * * * * * * Copy data to local memory * * * * * * * * * * * * * *//   
   // reading data from global memory starts from previous cell and continues
   // to two cells afterward. 
   for (int k = localSlice; k < localDepth; k += get_local_size(2))
   {
      int currentGlobalSlice  = groupStartSlice + k;
      int localCellNo_z = (int) k / nodePerCellPerDir;  // local cell no in the loop
      int localNodeNo_z = k % nodePerCellPerDir;    // local nodeNo in localCell in z dir in loop
      int currentGlobalCell_z = cellNo_z + localCellNo_z - 1; // global cell number in loop

      // step down rows
      for (int i = localRow; i < localHeight; i += get_local_size(1))
      {
        int currentGlobalRow    = groupStartRow + i;
        int localCellNo_x = (int) i / nodePerCellPerDir;
        int localNodeNo_x = i % nodePerCellPerDir;
        int currentGlobalCell_x = cellNo_x + localCellNo_x - 1;

        // step across columns
        for (int j = localCol; j < localWidth; j += get_local_size(0))
        {
            int currentGlobalCol    = groupStartCol + j;
            int localCellNo_y = (int) j / nodePerCellPerDir;
            int localNodeNo_y = j % nodePerCellPerDir;
            int currentGlobalCell_y = cellNo_y + localCellNo_y - 1;

            // perform copy to local memory
              if (currentGlobalCell_z < 0 || currentGlobalCell_z > maxCellNo_z -1 ||
                  currentGlobalCell_x < 0 || currentGlobalCell_x > maxCellNo_x -1 ||
                  currentGlobalCell_y < 0 || currentGlobalCell_y > maxCellNo_y -1 )
              {
                 *(local_x + (localCellNo_z * cellInLocalMem * cellInLocalMem + 
                              localCellNo_x * cellInLocalMem +
                              localCellNo_y) * nodePerCell + 
                              localNodeNo_z * nodePerCellPerDir * nodePerCellPerDir +
                              localNodeNo_x * nodePerCellPerDir +
                              localNodeNo_y) = 0.0; 
              } else
              {   
                 // see the description above to see the reason of '- nodePerCellPerDir'
                 *(local_x + (localCellNo_z * cellInLocalMem * cellInLocalMem + 
                              localCellNo_x * cellInLocalMem +
                              localCellNo_y) * nodePerCell + 
                              localNodeNo_z * nodePerCellPerDir * nodePerCellPerDir +
                              localNodeNo_x * nodePerCellPerDir +
                              localNodeNo_y) 
                 = 
                 *(x + (currentGlobalCell_z  * maxCellNo_x * maxCellNo_y +
                        currentGlobalCell_x  * maxCellNo_y +
                        currentGlobalCell_y) * nodePerCell +
                        localNodeNo_z * nodePerCellPerDir * nodePerCellPerDir +
                        localNodeNo_x * nodePerCellPerDir + 
                        localNodeNo_y);
              }
         }
      }
   }

   barrier(CLK_LOCAL_MEM_FENCE);

   //* * * * * * * * * * * * * * Store local stencil * * * * * * * * * * * * * *//

   // create local stiffness matrix 
   // Here we only create the row corresponding to the node relaxed 
   // by the current work item
   
   // size of stiffness stencil matrix is 7 * node number ^ 3 
   // 7 = 6 neighbours + the centeral cell 
   T localRowOfStiffMatrix[nodePerCell * (faceNo + 1)] = {0.0};

   // local ID of current work item
   int localId = localSlice * get_local_size(2) * get_local_size(1) + 
                 localRow   * get_local_size(1) + 
                 localCol;

   // nodeNumber and cellNumber determine the corresponding node in the specific
   // cell which current work item should work on

   // The nodes of cells current work item should work on
   int nodeNumber_y = node_j[localId] % nodePerCellPerDir;
   int nodeNumber_x = node_i[localId] % nodePerCellPerDir;
   int nodeNumber_z = node_k[localId] % nodePerCellPerDir;

   // global cell number on which current work item should work on
   int cellNumber_y = cellNo_y + node_j[localId] / nodePerCellPerDir;
   int cellNumber_x = cellNo_x + node_i[localId] / nodePerCellPerDir;
   int cellNumber_z = cellNo_z + node_k[localId] / nodePerCellPerDir;
   
   // local cell number on which current work item should work on
   int startCell = 1;
   int localCell_y = startCell + node_j[localId] / nodePerCellPerDir;
   int localCell_x = startCell + node_i[localId] / nodePerCellPerDir;
   int localCell_z = startCell + node_k[localId] / nodePerCellPerDir;

   // number of node in the cell which current work item work on
   int localNodeNo = nodeNumber_z * nodePerCellPerDir * nodePerCellPerDir + 
                     nodeNumber_x * nodePerCellPerDir +
                     nodeNumber_y;

   for (int i = 0; i < nodePerCell; ++i)
   {
     localRowOfStiffMatrix[nodePerCell * faceNo / 2 + i] +=
                 volumeIntegralMatrix[localNodeNo * nodePerCell + i] +
                                M11_1[localNodeNo * nodePerCell + i] +
                                M11_2[localNodeNo * nodePerCell + i] +
                                M11_3[localNodeNo * nodePerCell + i] +
                                M11_4[localNodeNo * nodePerCell + i] +
                                M11_5[localNodeNo * nodePerCell + i] +
                                M11_6[localNodeNo * nodePerCell + i];

    
     if (cellNumber_z != 0 || cellNumber_z != 1)
     {
       localRowOfStiffMatrix[nodePerCell * faceNo / 2 + i] +=
                                M22_1[localNodeNo * nodePerCell + i];
       localRowOfStiffMatrix[i] += 
                      M12_1[localNodeNo * nodePerCell + i] +
                      M21_1[localNodeNo * nodePerCell + i];
     }

     if (cellNumber_x != 0 || cellNumber_x != 1)
     {
       localRowOfStiffMatrix[nodePerCell * faceNo / 2 + i] +=
                                M22_2[localNodeNo * nodePerCell + i];
       localRowOfStiffMatrix[nodePerCell * 1 + i] +=
                      M12_2[localNodeNo * nodePerCell + i] +
                      M21_2[localNodeNo * nodePerCell + i];
     }
     
     if (cellNumber_y != 0 || cellNumber_y != 1)
     {
       localRowOfStiffMatrix[nodePerCell * faceNo / 2 + i] +=
                                M22_3[localNodeNo * nodePerCell + i];
       localRowOfStiffMatrix[nodePerCell * 2 + i] +=
                      M12_3[localNodeNo * nodePerCell + i] +
                      M21_3[localNodeNo * nodePerCell + i];
     }
     
     if (cellNumber_y != maxCellNo_y - 1)
     {
       localRowOfStiffMatrix[nodePerCell * faceNo / 2 + i] +=
                                M22_4[localNodeNo * nodePerCell + i];
       localRowOfStiffMatrix[nodePerCell * 4 + i] +=
                      M12_4[localNodeNo * nodePerCell + i] +
                      M21_4[localNodeNo * nodePerCell + i];
     }
     
     if (cellNumber_x != maxCellNo_x - 1)
     {
       localRowOfStiffMatrix[nodePerCell * faceNo / 2 + i] +=
                                M22_5[localNodeNo * nodePerCell + i];
       localRowOfStiffMatrix[nodePerCell * 5 + i] +=
                      M12_5[localNodeNo * nodePerCell + i] +
                      M21_5[localNodeNo * nodePerCell + i];
     }
     
     if (cellNumber_z != maxCellNo_z - 1)
     {
       localRowOfStiffMatrix[nodePerCell * faceNo / 2 + i] +=
                                M22_6[localNodeNo * nodePerCell + i];
       localRowOfStiffMatrix[nodePerCell * 6 + i] +=
                      M12_6[localNodeNo * nodePerCell + i] +
                      M21_6[localNodeNo * nodePerCell + i];
     }
   }

   int globalIndex = 
            (cellNumber_z  * maxCellNo_x * maxCellNo_y +
             cellNumber_x  * maxCellNo_y +
             cellNumber_y) * nodePerCell +
             nodeNumber_z  * nodePerCellPerDir * nodePerCellPerDir +
             nodeNumber_x  * nodePerCellPerDir + 
             nodeNumber_y;
 
   //* * * * * * * * * * * * * * Apply stencil * * * * * * * * * * * * * *//
 
     // Calculate right hand-side
     
     T aii = localRowOfStiffMatrix[faceNo / 2 * nodePerCell + localNodeNo];

     T sum;
     for (int k = 0; k < 10; ++k) 
     {
        sum = b[globalIndex] / aii * omega;
        for (int fi = 0; fi < faceNo + 1; ++fi)
        {
           for (int ni = 0; ni < nodePerCell; ++ni)
           {
              if (fi == faceNo / 2 && ni == localNodeNo)
              {
                 sum += local_x[((localCell_z + adjacentCell_z[fi])  *
                                                cellInLocalMem  * cellInLocalMem +
                                 (localCell_x + adjacentCell_x[fi])  * cellInLocalMem +
                                 (localCell_y + adjacentCell_y[fi])) * nodePerCell + ni] *
                                 (1.0 - omega);
              } else
              {
                 sum += localRowOfStiffMatrix[fi * nodePerCell + ni] *
                        local_x[((localCell_z + adjacentCell_z[fi])  *
                                                cellInLocalMem  * cellInLocalMem +
                                 (localCell_x + adjacentCell_x[fi])  * cellInLocalMem +
                                 (localCell_y + adjacentCell_y[fi])) * nodePerCell + ni] /
                                  aii * omega;
              }
           }
        }
     
        int localIdx = (localCell_z * cellInLocalMem * cellInLocalMem +
                        localCell_x * cellInLocalMem +
                        localCell_y) * nodePerCell + localNodeNo;

        local_x[localIdx] = sum;
        barrier(CLK_LOCAL_MEM_FENCE);
     }

     y[globalIndex] = sum;
}



 /*
  * The kernel calculates the residual of r = b - Ax
  */
__kernel void residual(__global T* x,
                       int maxCellNo_x,
                       int maxCellNo_y,
                       int maxCellNo_z,
                       __global T* volumeIntegralMatrix,
                       __global T* M11_1,
                       __global T* M12_1,
                       __global T* M21_1,
                       __global T* M22_1,
                       __global T* M11_2,
                       __global T* M12_2,
                       __global T* M21_2,
                       __global T* M22_2,
                       __global T* M11_3,
                       __global T* M12_3,
                       __global T* M21_3,
                       __global T* M22_3,
                       __global T* M11_4,
                       __global T* M12_4,
                       __global T* M21_4,
                       __global T* M22_4,
                       __global T* M11_5,
                       __global T* M12_5,
                       __global T* M21_5,
                       __global T* M22_5,
                       __global T* M11_6,
                       __global T* M12_6,
                       __global T* M21_6,
                       __global T* M22_6,
                       __global T* b,
                       __global T* residual,
                       __local T* local_x,
                       int localHeight,
                       int localWidth,
                       int localDepth,
                       int cellInLocalMem)

{
   int nodePerCellPerDir = polynomialOrder + 1;

   // size of work group in output
   int groupStartCol   = get_group_id(0) * get_local_size(0);
   int groupStartRow   = get_group_id(1) * get_local_size(1);
   int groupStartSlice = get_group_id(2) * get_local_size(2);

   // local ID for work-items
   int localCol   = get_local_id(0);
   int localRow   = get_local_id(1);
   int localSlice = get_local_id(2);

   // global ID of each work-item
   int globalCol   = groupStartCol   + localCol;
   int globalRow   = groupStartRow   + localRow;
   int globalSlice = groupStartSlice + localSlice;
   
   // global cell No. in each dir
   int cellNo_z = (int) globalSlice / nodePerCellPerDir;
   int cellNo_x = (int) globalRow   / nodePerCellPerDir;
   int cellNo_y = (int) globalCol   / nodePerCellPerDir;

   //* * * * * * * * * * * * * * Copy data to local memory * * * * * * * * * * * * * *//   
   // reading data from global memory starts from previous cell and continues
   // to two cells afterward. 
   for (int k = localSlice; k < localDepth; k += get_local_size(2))
   {
      int currentGlobalSlice  = groupStartSlice + k;
      int localCellNo_z       = (int) k / nodePerCellPerDir;  // local cell no in the loop
      int localNodeNo_z       = k % nodePerCellPerDir;    // local nodeNo in localCell in z dir in loop
      int currentGlobalCell_z = cellNo_z + localCellNo_z - 1; // global cell number in loop

      // step down rows
      for (int i = localRow; i < localHeight; i += get_local_size(1))
      {
        int currentGlobalRow    = groupStartRow + i;
        int localCellNo_x       = (int) i / nodePerCellPerDir;
        int localNodeNo_x       = i % nodePerCellPerDir;
        int currentGlobalCell_x = cellNo_x + localCellNo_x - 1;

        // step across columns
        for (int j = localCol; j < localWidth; j += get_local_size(0))
        {
            int currentGlobalCol    = groupStartCol + j;
            int localCellNo_y       = (int) j / nodePerCellPerDir;
            int localNodeNo_y       = j % nodePerCellPerDir;
            int currentGlobalCell_y = cellNo_y + localCellNo_y - 1;

            // perform copy to local memory
            if (currentGlobalCell_z < 0 || currentGlobalCell_z > maxCellNo_z -1 ||
                currentGlobalCell_x < 0 || currentGlobalCell_x > maxCellNo_x -1 ||
                currentGlobalCell_y < 0 || currentGlobalCell_y > maxCellNo_y -1 )
            {
               local_x[(localCellNo_z * cellInLocalMem * cellInLocalMem +
                        localCellNo_x * cellInLocalMem +
                        localCellNo_y) * nodePerCell +
                        localNodeNo_z * nodePerCellPerDir * nodePerCellPerDir +
                        localNodeNo_x * nodePerCellPerDir +
                        localNodeNo_y] = 0.0;
              } else
              {
                 // see the description above to see the reason of '- nodePerCellPerDir'
                 local_x[(localCellNo_z * cellInLocalMem * cellInLocalMem +
                          localCellNo_x * cellInLocalMem +
                          localCellNo_y) * nodePerCell +
                          localNodeNo_z * nodePerCellPerDir * nodePerCellPerDir +
                          localNodeNo_x * nodePerCellPerDir +
                          localNodeNo_y]
                 = 
                 x[(currentGlobalCell_z  * maxCellNo_x * maxCellNo_y +
                    currentGlobalCell_x  * maxCellNo_y +
                    currentGlobalCell_y) * nodePerCell +
                    localNodeNo_z * nodePerCellPerDir * nodePerCellPerDir +
                    localNodeNo_x * nodePerCellPerDir +
                    localNodeNo_y];
             }
         }
      }
   }

   barrier(CLK_LOCAL_MEM_FENCE);

   //* * * * * * * * * * * * * * Store local stencil * * * * * * * * * * * * * *//

   // create local stiffness matrix 
   // Here we only create the row corresponding to the node relaxed 
   // by the current work item
   
   // size of stiffness stencil matrix is 7 * node number ^ 3 
   // 7 = 6 neighbours + the centeral cell 
   T localRowOfStiffMatrix[nodePerCell * (faceNo + 1)] = {0.0};

   // local ID of current work item
   int localId = localSlice * get_local_size(2) * get_local_size(1) + 
                 localRow   * get_local_size(1) + 
                 localCol;

   // nodeNumber and cellNumber determine the corresponding node in the specific
   // cell which current work item should work on

   // The nodes of cells current work item should work on
   int nodeNumber_y = node_j[localId] % nodePerCellPerDir;
   int nodeNumber_x = node_i[localId] % nodePerCellPerDir;
   int nodeNumber_z = node_k[localId] % nodePerCellPerDir;

   // global cell number on which current work item should work on
   int cellNumber_y = cellNo_y + node_j[localId] / nodePerCellPerDir;
   int cellNumber_x = cellNo_x + node_i[localId] / nodePerCellPerDir;
   int cellNumber_z = cellNo_z + node_k[localId] / nodePerCellPerDir;
   
   // local cell number on which current work item should work on
   int startCell = 1;
   int localCell_y = startCell + node_j[localId] / nodePerCellPerDir;
   int localCell_x = startCell + node_i[localId] / nodePerCellPerDir;
   int localCell_z = startCell + node_k[localId] / nodePerCellPerDir;

   // number of node in the cell which current work item work on
   int localNodeNo = nodeNumber_z * nodePerCellPerDir * nodePerCellPerDir + 
                     nodeNumber_x * nodePerCellPerDir +
                     nodeNumber_y;

   for (int i = 0; i < nodePerCell; ++i)
   {
     localRowOfStiffMatrix[nodePerCell * faceNo / 2 + i] +=
                 volumeIntegralMatrix[localNodeNo * nodePerCell + i] +
                                M11_1[localNodeNo * nodePerCell + i] +
                                M11_2[localNodeNo * nodePerCell + i] +
                                M11_3[localNodeNo * nodePerCell + i] +
                                M11_4[localNodeNo * nodePerCell + i] +
                                M11_5[localNodeNo * nodePerCell + i] +
                                M11_6[localNodeNo * nodePerCell + i];

    
     if (cellNumber_z != 0 || cellNumber_z != 1)
     {
       localRowOfStiffMatrix[nodePerCell * faceNo / 2 + i] +=
                                M22_1[localNodeNo * nodePerCell + i];
       localRowOfStiffMatrix[i] += 
                      M12_1[localNodeNo * nodePerCell + i] +
                      M21_1[localNodeNo * nodePerCell + i];
     }

     if (cellNumber_x != 0 || cellNumber_x != 1)
     {
       localRowOfStiffMatrix[nodePerCell * faceNo / 2 + i] +=
                                M22_2[localNodeNo * nodePerCell + i];
       localRowOfStiffMatrix[nodePerCell * 1 + i] +=
                      M12_2[localNodeNo * nodePerCell + i] +
                      M21_2[localNodeNo * nodePerCell + i];
     }
     
     if (cellNumber_y != 0 || cellNumber_y != 1)
     {
       localRowOfStiffMatrix[nodePerCell * faceNo / 2 + i] +=
                                M22_3[localNodeNo * nodePerCell + i];
       localRowOfStiffMatrix[nodePerCell * 2 + i] +=
                      M12_3[localNodeNo * nodePerCell + i] +
                      M21_3[localNodeNo * nodePerCell + i];
     }
     
     if (cellNumber_y != maxCellNo_y - 1)
     {
       localRowOfStiffMatrix[nodePerCell * faceNo / 2 + i] +=
                                M22_4[localNodeNo * nodePerCell + i];
       localRowOfStiffMatrix[nodePerCell * 4 + i] +=
                      M12_4[localNodeNo * nodePerCell + i] +
                      M21_4[localNodeNo * nodePerCell + i];
     }
     
     if (cellNumber_x != maxCellNo_x - 1)
     {
       localRowOfStiffMatrix[nodePerCell * faceNo / 2 + i] +=
                                M22_5[localNodeNo * nodePerCell + i];
       localRowOfStiffMatrix[nodePerCell * 5 + i] +=
                      M12_5[localNodeNo * nodePerCell + i] +
                      M21_5[localNodeNo * nodePerCell + i];
     }
     
     if (cellNumber_z != maxCellNo_z - 1)
     {
       localRowOfStiffMatrix[nodePerCell * faceNo / 2 + i] +=
                                M22_6[localNodeNo * nodePerCell + i];
       localRowOfStiffMatrix[nodePerCell * 6 + i] +=
                      M12_6[localNodeNo * nodePerCell + i] +
                      M21_6[localNodeNo * nodePerCell + i];
     }
   }


   int globalIndex = (cellNumber_z  * maxCellNo_x * maxCellNo_y +
                      cellNumber_x  * maxCellNo_y +
                      cellNumber_y) * nodePerCell +
                      nodeNumber_z  * nodePerCellPerDir * nodePerCellPerDir +
                      nodeNumber_x  * nodePerCellPerDir + 
                      nodeNumber_y;
 
   //* * * * * * * * * * * * * * Apply stencil * * * * * * * * * * * * * *//
   
     // Calculate right hand-side
     T sum = b[globalIndex];

     for (int fi = 0; fi < faceNo + 1; ++fi)
     {
        for (int ni = 0; ni < nodePerCell; ++ni)
        {
           sum -= localRowOfStiffMatrix[fi * nodePerCell + ni] *  
                  local_x[((localCell_z + adjacentCell_z[fi])  *
                                              cellInLocalMem   * cellInLocalMem +
                          (localCell_x + adjacentCell_x[fi])   * cellInLocalMem +
                         (localCell_y + adjacentCell_y[fi]))  * nodePerCell + ni];
        }
     }                                                    

     residual[globalIndex] = sum; 
}



/*
 * The kernel implements restriction of multigrid (going down to coarse grid)
 */
__kernel void restriction(__global T* x, 
                          int maxDistCellNo_x, 
                          int maxDistCellNo_y, 
                          int maxDistCellNo_z,
                          __global T* restStencil, 
                          __global T* y, 
                          __local T* local_x, 
                          int localWidth, 
                          int localHeight, 
                          int localDepth, 
                          int intplCellLocalMem)
{ 
   int nodePerCellPerDir = polynomialOrder + 1;

   int widthCell = (int) intplCellLocalMem / 2;

   int ghostCellPad = 1;
   // size of work group in output
   int groupStartCol   = get_group_id(0) * get_local_size(0);
   int groupStartRow   = get_group_id(1) * get_local_size(1);
   int groupStartSlice = get_group_id(2) * get_local_size(2);

   // local ID for work-items
   int localCol   = get_local_id(0);
   int localRow   = get_local_id(1);
   int localSlice = get_local_id(2);

   // global ID of each work-item
   int globalCol   = groupStartCol   + localCol;
   int globalRow   = groupStartRow   + localRow;
   int globalSlice = groupStartSlice + localSlice;
   
   // global cell No. in each dir
   // +1 is for skipping the ghost cells
   int cellNo_z = (int) globalSlice / nodePerCellPerDir * 2 + ghostCellPad;
   int cellNo_x = (int) globalRow   / nodePerCellPerDir * 2 + ghostCellPad;
   int cellNo_y = (int) globalCol   / nodePerCellPerDir * 2 + ghostCellPad;


   //* * * * * * * * * * * * * * Copy data to local memory * * * * * * * * * * * * * *//   
   // reading data from global memory starts from previous cell and continues
   // to two cells afterward.
   for (int k = localSlice; k < localDepth; k += get_local_size(2))
   {
      int currentGlobalSlice  = groupStartSlice + k;

      // local cell no in the loop
      int localCellNo_z       = (int) k / nodePerCellPerDir;

      // local nodeNo in localCell in z dir in loop
      int localNodeNo_z       = k % nodePerCellPerDir;

      // global cell number in loop
      int currentGlobalCell_z = cellNo_z + localCellNo_z;

      // step down rows
      for (int i = localRow; i < localHeight; i += get_local_size(1))
      {
        int currentGlobalRow    = groupStartRow + i;
        int localCellNo_x       = (int) i / nodePerCellPerDir;
        int localNodeNo_x       = i % nodePerCellPerDir;
        int currentGlobalCell_x = cellNo_x + localCellNo_x;

        // step across columns
        for (int j = localCol; j < localWidth; j += get_local_size(0))
        {
            int currentGlobalCol    = groupStartCol + j;
            int localCellNo_y       = (int) j / nodePerCellPerDir;
            int localNodeNo_y       = j % nodePerCellPerDir;
            int currentGlobalCell_y = cellNo_y + localCellNo_y;

            // perform copy to local memory
           /* if (currentGlobalRow   < deviceHeight && currentGlobalCol < deviceWidth && 
                currentGlobalSlice < deviceDepth)
            {*/
              if (currentGlobalCell_z < 0 || currentGlobalCell_z > maxDistCellNo_z -1 ||
                  currentGlobalCell_x < 0 || currentGlobalCell_x > maxDistCellNo_x -1 ||
                  currentGlobalCell_y < 0 || currentGlobalCell_y > maxDistCellNo_y -1 )
              {
                 local_x[(localCellNo_z * intplCellLocalMem * intplCellLocalMem + 
                          localCellNo_x * intplCellLocalMem +
                          localCellNo_y) * nodePerCell + 
                          localNodeNo_z * nodePerCellPerDir * nodePerCellPerDir +
                          localNodeNo_x * nodePerCellPerDir +
                          localNodeNo_y] = 0.0;
              } else
              {   
                 local_x[(localCellNo_z * intplCellLocalMem * intplCellLocalMem + 
                          localCellNo_x * intplCellLocalMem +
                          localCellNo_y) * nodePerCell + 
                          localNodeNo_z * nodePerCellPerDir * nodePerCellPerDir +
                          localNodeNo_x * nodePerCellPerDir +
                          localNodeNo_y]
                 = 
                 x[(currentGlobalCell_z  * maxDistCellNo_x * maxDistCellNo_y +
                    currentGlobalCell_x  * maxDistCellNo_y +
                    currentGlobalCell_y) * nodePerCell +
                    localNodeNo_z * nodePerCellPerDir * nodePerCellPerDir +
                    localNodeNo_x * nodePerCellPerDir + 
                    localNodeNo_y];
              }
           // }
         }
      }
   }

   barrier(CLK_LOCAL_MEM_FENCE);

   //* * * * * * * * * * * * * * Apply stencil * * * * * * * * * * * * * *//
   
   int localId = localSlice * get_local_size(1) * get_local_size(0) +
                 localRow   * get_local_size(0) +
                 localCol;
   
   int distIdx = (((cellNo_z - ghostCellPad) / 2 + ghostCellPad)  * 
                                             maxDistCellNo_x * maxDistCellNo_y / 4 +
                  ((cellNo_x - ghostCellPad) / 2 + ghostCellPad)  * maxDistCellNo_y / 2 + 
                  ((cellNo_y - ghostCellPad) / 2 + ghostCellPad)) * nodePerCell + localId;

   T sum = 0.0;
   int loopIdx = 0;

   for (int k = 0; k < intplCellLocalMem; ++k)
   {
      for (int i = 0; i < intplCellLocalMem; ++i)
      {
         for (int j = 0; j < intplCellLocalMem; ++j)
         {
            for (int ni = 0; ni < nodePerCell; ++ni)
            {
                loopIdx = (k * intplCellLocalMem * intplCellLocalMem +
                                    i * intplCellLocalMem +
                                    j) * nodePerCell; 
                sum += restStencil[loopIdx * nodePerCell + localId * nodePerCell + ni] * 
                                    local_x[loopIdx + ni];
            }
         }
      }
   }        
   
   y[distIdx] = sum;
}


/* /////////////////////////////////////////////////////////////////////////
//                                                                        //
//                           LINEAR INTERPOLATION                         //
//                                                                        //
//////////////////////////////////////////////////////////////////////////*/

/*
 * The kernel implements linear interpolation of multigrid (going up to fine grid)
 * The process of implementaion of the interpolation technique is perform is a way that
 *     the stencil applied to Grid 'H' and write it into Grid 'h'.
 *     Therefore the parameters are from source grid, e.g. domainSize, globalWorkItem, ...
 */
__kernel void prolongation (__global T* x_src,
                            int maxSrcCellNo_x,
                            int maxSrcCellNo_y,
                            int maxSrcCellNo_z,
                            __global T* prolStencil,
                            __global T* y,
                            __local T* local_x,
                            int localWidth,
                            int localHeight,
                            int localDepth,
                            int intplCellLocalMem,
                            int localCellNo)
{
   int nodePerCellPerDir = polynomialOrder + 1;

   int ghostCellPad = 1;

   // size of work group in output
   int groupStartCol   = get_group_id(0) * get_local_size(0);
   int groupStartRow   = get_group_id(1) * get_local_size(1);
   int groupStartSlice = get_group_id(2) * get_local_size(2);

   // local ID for work-items
   int localCol   = get_local_id(0);
   int localRow   = get_local_id(1);
   int localSlice = get_local_id(2);

   // global ID of each work-item
   int globalCol   = groupStartCol   + localCol;
   int globalRow   = groupStartRow   + localRow;
   int globalSlice = groupStartSlice + localSlice;

   // global cell No. in each dir -> we skip ghost cell
   int cellNo_z = (int) globalSlice / nodePerCellPerDir + ghostCellPad;
   int cellNo_x = (int) globalRow   / nodePerCellPerDir + ghostCellPad;
   int cellNo_y = (int) globalCol   / nodePerCellPerDir + ghostCellPad;

   //* * * * * * * * * * * * * * Copy data to local memory * * * * * * * * * * * * * *//

   // reading data from global memory starts from previous cell and continues
   // to two cells afterward. 
   for (int k = localSlice; k < localDepth; k += get_local_size(2))
   {
      int currentGlobalSlice  = groupStartSlice + k;

      // local cell no in the loop
      int localCellNo_z       = (int) k / nodePerCellPerDir;

      // local nodeNo in localCell in z dir in loop
      int localNodeNo_z       = k % nodePerCellPerDir;

      // global cell number in loop
      int currentGlobalCell_z = cellNo_z + localCellNo_z;

      // step down rows
      for (int i = localRow; i < localHeight; i += get_local_size(1))
      {
        int currentGlobalRow    = groupStartRow + i;
        int localCellNo_x       = (int) i / nodePerCellPerDir;
        int localNodeNo_x       = i % nodePerCellPerDir;
        int currentGlobalCell_x = cellNo_x + localCellNo_x;

        // step across columns
        for (int j = localCol; j < localWidth; j += get_local_size(0))
        {
            int currentGlobalCol    = groupStartCol + j;
            int localCellNo_y       = (int) j / nodePerCellPerDir;
            int localNodeNo_y       = j % nodePerCellPerDir;
            int currentGlobalCell_y = cellNo_y + localCellNo_y;

            // perform copy to local memory
            if (currentGlobalCell_z < 0 || currentGlobalCell_z > maxSrcCellNo_z ||
                currentGlobalCell_x < 0 || currentGlobalCell_x > maxSrcCellNo_x ||
                currentGlobalCell_y < 0 || currentGlobalCell_y > maxSrcCellNo_y )
            {
               local_x[(localCellNo_z * localCellNo * localCellNo +
                        localCellNo_x * localCellNo +
                        localCellNo_y) * nodePerCell +
                        localNodeNo_z * nodePerCellPerDir * nodePerCellPerDir +
                        localNodeNo_x * nodePerCellPerDir +
                        localNodeNo_y] = 0.0;
            } else
            {
               local_x[(localCellNo_z * localCellNo * localCellNo +
                        localCellNo_x * localCellNo +
                        localCellNo_y) * nodePerCell +
                        localNodeNo_z * nodePerCellPerDir * nodePerCellPerDir +
                        localNodeNo_x * nodePerCellPerDir +
                        localNodeNo_y]
               =
               x_src[(currentGlobalCell_z  * maxSrcCellNo_x * maxSrcCellNo_y +
                      currentGlobalCell_x  * maxSrcCellNo_y +
                      currentGlobalCell_y) * nodePerCell +
                      localNodeNo_z * nodePerCellPerDir * nodePerCellPerDir +
                      localNodeNo_x * nodePerCellPerDir +
                      localNodeNo_y];
            }
         }
      }
   }

   barrier(CLK_LOCAL_MEM_FENCE);

   //* * * * * * * * * * * * * * Store local stencil * * * * * * * * * * * * * *//
 
   T localRowOfRestMatrix[nodePerCell] = {0.0};

   int localId = localSlice * get_local_size(1) * get_local_size(0) +
                 localRow   * get_local_size(0) +
                 localCol;

   int distIndx = 0;

   for (int ck = 0; ck < intplCellLocalMem; ++ck)
   {
      for (int ci = 0; ci < intplCellLocalMem; ++ci)
      {
         for (int cj = 0; cj < intplCellLocalMem; ++cj)
         {
            for (int ni = 0; ni < nodePerCell; ++ni)
            {
               localRowOfRestMatrix[ni] 
               =
               prolStencil[(ck * intplCellLocalMem * intplCellLocalMem +
                            ci * intplCellLocalMem +
                            cj) * nodePerCell * nodePerCell + 
                            localId * nodePerCell + ni];
            }

            //* * * * * * * * * * * * * * Apply stencil * * * * * * * * * * * * * *//
          
            distIndx = (((cellNo_z - ghostCellPad) * 2 + ck + ghostCellPad) *
                        ((maxSrcCellNo_x - 2 * ghostCellPad) * 2 + 2 * ghostCellPad) *
                        ((maxSrcCellNo_y - 2 * ghostCellPad) * 2 + 2 * ghostCellPad) + 
                        ((cellNo_x - ghostCellPad) * 2 + ci + ghostCellPad) * 
                        ((maxSrcCellNo_y - 2 * ghostCellPad) * 2 + 2 * ghostCellPad) + 
                        ((cellNo_y - ghostCellPad) * 2 + cj + ghostCellPad)) * 
                                                           nodePerCell + localId;
           
            T sum = 0.0;

            for (int ni = 0; ni < nodePerCell; ++ni)
            {
               sum += localRowOfRestMatrix[ni] * local_x[ni];
            }

            y[distIndx] += sum;
         }
      }
   }
}
