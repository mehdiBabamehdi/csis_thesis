/*-----------------------------------------------------------------------*\
           ___________________________________________________
          /                                                  /|
         /                                                  / |
        |--------------------------------------------------|  |
        |    ______ __     __  ______    _____  __   __    |  |
        |   |   ___|  \   /  |/   ___|  |  _  \|  | |  |   |  |
        |   |  |_  |   \_/   |   /      | |_|  |  | |  |   |  |
        |   |    | |         |  |   ____|   __/|  | |  |   |  |
        |   |   _| |   ___   |  |  |_  _|  |   |  |_|  |   |  |
        |   |  |   |  |   |  |   \__/ / |  |   |       |   |  |
        |   |__|   |__|   |__|\______/  |__|    \_____/    |  |
        |                                                  |  |
        |      Full      Multigrid      On     GPGPU       | /
        |__________________________________________________|/

--------------------------------------------------------------------------

Author

    Mehdi Baba Mehdi
    mehdi.baba_mehdi@uni-wuppertal.de

Date  
    Wed 03.05.2020
\*-----------------------------------------------------------------------*/

#ifndef VCYCLE_HPP
#define VCYCLE_HPP

#define __CL_ENABLE_EXCEPTIONS

#ifdef LOCAL_CL
#include "CL/cl.hpp"
#else
#include <CL/cl.hpp>
#endif

//#define DEBUG

#include <iostream>
#include <cassert>
#include <fstream>
#include <vector>
#include <string>
#include <cstring>
#include <cmath>
#include <iomanip>
#include <chrono>

#include "CL_ERROR.hpp"
#include "defines.hpp"
#include "solver.hpp"
#include "multiGrid.hpp"
#include "residual.hpp"
#include "relaxation.hpp"
#include "prolongation.hpp"
#include "restriction.hpp"
#include "gnuplot.hpp"


template<class T>
class restriction;

template<class T>
class gnuplot;

template<class T>
class relaxation;

template<class T>
class residual;

template<class T>
class prolonation;

template<class T>
class boundaryCondition;

template<class T>
class solver;

template<class T>
class multiGrid;

template<class T>
class vCycle : public multiGrid<T>
{
  protected:
    int nu_1 = 1;        // number of pre-smotting sweep
    int nu_2 = 1;       // number of post-smotting sweep

    bool plotting = false;

    // variables used for time study
    bool timing = false;
    double* vcycTime = NULL;
    double prolTime = 0;
    double restTime = 0;
    double residTime = 0;
    double relaxTime = 0;   

    // define cycle of V multigrid
    int vCycleStep = 0;   // count number of V-Cycle to reduce error to specific tol
    int maxVCycleSteps = 2;  // max of V-Cycle the algorithm is allowed to sweep

        
  public:
    vCycle( T* _RHS,
            int _origCellNo_x,  int _origCellNo_y,  int _origCellNo_z,
           std::string _relaxationMethod, T _omega,
           cl_device_type _deviceType, int _deviceID,
           int _nu_1, int _nu_2, bool _plotting = false, int _maxVCycleSteps = 1);

    vCycle(std::ifstream _RHSFileName,
            int _origCellNo_x,  int _origCellNo_y,  int _origCellNo_z, 
           std::string _relaxationMethod, T _omega,
           cl_device_type _deviceType, int _deviceID, 
           int _nu_1, int _nu_2, bool _plotting = false, int _maxVCycleSteps = 1);
 
    vCycle( int _origCellNo_x,  int _origCellNo_y, int _origCellNo_z,
           std::string _relaxationMethod, T _omega,
           cl_device_type _deviceType, int _deviceID, 
           int _nu_1, int _nu_2, bool _plotting = false, int _maxVCycleSteps = 1);

    vCycle(int _nu_1, int _nu_2, bool _plotting = false, int _maxVCycleSteps = 1);
    
    vCycle( multiGrid<T>& _vcycMultiGrid, 
           int _nu_1, int _nu_2, bool _plotting = false, int _maxVCycleSteps = 1);

    ~vCycle();

    virtual void solve() override;

    void vCycleAlgorithm();

};

#endif
