/*-----------------------------------------------------------------------*\
           ___________________________________________________
          /                                                  /|
         /                                                  / |
        |--------------------------------------------------|  |
        |    ______ __     __  ______    _____  __   __    |  |
        |   |   ___|  \   /  |/   ___|  |  _  \|  | |  |   |  |
        |   |  |_  |   \_/   |   /      | |_)  |  | |  |   |  |
        |   |    | |         |  |   ____|   __/|  | |  |   |  |
        |   |   _| |   ___   |  |  |_  _|  |   |  |_|  |   |  |
        |   |  |   |  |   |  |   \__/ / |  |   |       |   |  |
        |   |__|   |__|   |__|\______/  |__|    \_____/    |  |
        |                                                  |  |
        |      Full      Multigrid      On     GPGPU       | /
        |__________________________________________________|/

--------------------------------------------------------------------------

Author

    Mehdi Baba Mehdi
    mehdi.baba_mehdi@uni-wuppertal.de

Date  
    Sat 11.04.2020
\*-----------------------------------------------------------------------*/

#ifndef RESIDUAL_HPP
#define RESIDUAL_HPP

#define __CL_ENABLE_EXCEPTIONS

#ifdef LOCAL_CL
#include "CL/cl.hpp"
#else
#include <CL/cl.hpp>
#endif

#include <iostream>
#include <iomanip>
#include "CL_ERROR.hpp"
#include "defines.hpp"
#include <chrono>
#include <cmath>

template<class T>
class residual
{
  protected:
    cl::Kernel       kernel;

    long double timeOfOperation = 0.0;

    const int faceNo = 6;

    const int cellInLocalMem = 4;
   
  public:
     residual(cl::Program& _program);

    ~residual();

     const long double getTime() const;

     void createKernel(cl::Program& program);

     void calculateResidual(cl::CommandQueue& queue, 
                          const cl::Buffer& xBuffer, const cl::Buffer& RHSBuffer,
                          cl::Buffer& residualBuffer, const int& polynomialOrder, 
                          const cl::Buffer& volumeIntegralMatrixBuffer,
                          const std::vector<cl::Buffer> M11Buffer,
                          const std::vector<cl::Buffer> M12Buffer,
                          const std::vector<cl::Buffer> M21Buffer,
                          const std::vector<cl::Buffer> M22Buffer,
                          const cl::NDRange& globalRange, 
                          const cl::NDRange& localRange,
                          const cl::size_t<3> maxCellNo, 
                          cl::Event& event);
};


#endif

