
/*-----------------------------------------------------------------------*\
  	      __________________________________________________
	     /                                                 /|
	    /_________________________________________________/ |
	   |                                                 |  |
	   |     __  _______     _______    _____  __   __   |  |
	   |    |  |/   ____|   /   ____|  |  _  \|  | |  |  |  |
	   |    |  |   /       |   /       | |_)  |  | |  |  |  |
	   |  __|  |  |   _____|  |   _____|   __/|  | |  |  |  |
	   | /  _  |  |  |_   _|  |  |_   _|  |   |  |_|  |  |  |
	   ||  (_| |   \__/  / |   \__/  / |  |   |       |  |  |
	   | \_____|\_______/   \_______/  |__|    \_____/   |  |
	   |                                                 |  |
           |    discontinuous Galerkin    On      GPGPU      | /
	   |_________________________________________________|/

--------------------------------------------------------------------------

Author

    Mehdi Baba Mehdi
    mehdi.baba_mehdi@uni-wuppertal.de

Date  
    Fri 10.04.2020
\*-----------------------------------------------------------------------*/

#ifndef RESTRICTION_HPP
#define RESTRICTION_HPP

#define __CL_ENABLE_EXCEPTIONS

#ifdef LOCAL_CL
#include "CL/cl.hpp"
#else
#include <CL/cl.hpp>
#endif

#include <iostream>
#include <iomanip>
#include <cmath>
#include "CL_ERROR.hpp"
#include "defines.hpp"
#include <chrono>

template<class T>
class dGProlStencilCreator;


template<class T>
class restriction
{
  protected:
    int nodePerCellPerDir;

    int stencilWidth = 2;         // number of cell (in each dir)
    int restrictionStencilSize;   // number of node in stencil and local mem

    int localSizeDir;    // number of node in each dir of stencil and local mem

    T* stencil     = NULL;
    cl::Kernel       restrictionKernel;
    cl::Kernel       additionKernel;
    cl::Buffer       restrictionStencilBuffer;

    long double timeOfOperation = 0.0;

    dGProlStencilCreator<T>* dGStencil;
  public:
     restriction(cl::Program& _program, cl::CommandQueue& _queue,
                 cl::Context& _context, const int& _nodePerCellPerDir);

    ~restriction();

     const long double getTime() const;

     void setStencil(cl::Context& context, cl::CommandQueue& queue, 
                     const int nodePerCellPerDir);

     void createKernel(cl::Program& program);

     void doRestriction(cl::CommandQueue& queue, 
                    cl::Buffer& inputBuffer, cl::Buffer& outputBuffer,
                    const cl::NDRange& srcGlobalRange, const cl::NDRange& srcLocalRange,
                    const cl::size_t<3> maxDistCellNo,
                    cl::Event& event);
};


#endif














