
/*-----------------------------------------------------------------------*\
           ___________________________________________________
          /                                                  /|
         /                                                  / |
        |--------------------------------------------------|  |
        |    ______ __     __  ______    _____  __   __    |  |
        |   |   ___|  \   /  |/   ___|  |  _  \|  | |  |   |  |
        |   |  |_  |   \_/   |   /      | |_)  |  | |  |   |  |
        |   |    | |         |  |   ____|   __/|  | |  |   |  |
        |   |   _| |   ___   |  |  |_  _|  |   |  |_|  |   |  |
        |   |  |   |  |   |  |   \__/ / |  |   |       |   |  |
        |   |__|   |__|   |__|\______/  |__|    \_____/    |  |
        |                                                  |  |
        |      Full      Multigrid      On     GPGPU       | /
        |__________________________________________________|/

--------------------------------------------------------------------------

Author

    Mehdi Baba Mehdi
    mehdi.baba_mehdi@uni-wuppertal.de

Date  
    Mon 13.04.2020
\*-----------------------------------------------------------------------*/


#ifndef BOUNDARYCONDITION_HPP
#define BOUNDARYCONDITION_HPP

#ifdef LOCAL_CL
#include "CL/cl.hpp"
#else
#include <CL/cl.hpp>
#endif

#include <string>
#include <fstream>
#include <iostream>
#include <map>
#include <iterator>

#include "solver.hpp"
#include "defines.hpp"
#include "bcFunctionDict.hpp"

template<class T>
class solver;

template<class T>
struct boundaryHandler
{
    T (*boundaryFunction)( T, T, T);
};


template<class T>
class boundaryCondition
{
  private:
    std::string   bcSetupFileName = "/home/mehdi/csis_thesis/dG-FEM/bcDict";
    std::map<std::string, std::string> bCLeft;
    std::map<std::string, std::string> bCRight;
    std::map<std::string, std::string> bCUp;
    std::map<std::string, std::string> bCDown;
    std::map<std::string, std::string> bCBack;
    std::map<std::string, std::string> bCFront;

  public:
    boundaryCondition(solver<T>& mySolver);

    ~boundaryCondition();

    void readBCFromFile();

    void setBoundaryCondition(solver<T>& mySolver);

    void applyBC(boundaryHandler<T> bcHandl, solver<T>& mySolver, int i0, int in, int j0, int jn, int k0, int kn);
};

#endif
