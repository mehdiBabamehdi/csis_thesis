

/*-----------------------------------------------------------------------*\
           ___________________________________________________
          /                                                  /|
         /                                                  / |
        |--------------------------------------------------|  |
        |    ______ __     __  ______    _____  __   __    |  |
        |   |   ___|  \   /  |/   ___|  |  _  \|  | |  |   |  |
        |   |  |_  |   \_/   |   /      | |_)  |  | |  |   |  |
        |   |    | |         |  |   ____|   __/|  | |  |   |  |
        |   |   _| |   ___   |  |  |_  _|  |   |  |_|  |   |  |
        |   |  |   |  |   |  |   \__/ / |  |   |       |   |  |
        |   |__|   |__|   |__|\______/  |__|    \_____/    |  |
        |                                                  |  |
        |      Full      Multigrid      On     GPGPU       | /
        |__________________________________________________|/

--------------------------------------------------------------------------

Author

    Mehdi Baba Mehdi
    mehdi.baba_mehdi@uni-wuppertal.de

Date  
    Thu 01.4.2020
\*-----------------------------------------------------------------------*/



#ifndef BCFUNCTIONDICT_HPP
#define BCFUNCTIONDICT_HPP
#include <cmath>

#ifdef LOCAL_CL
#include "CL/cl.hpp"
#else
#include <CL/cl.hpp>
#endif

template<class T>
T left(T, T, T);

template<class T>
T right(T, T, T);

template<class T>
T up(T, T, T);

template<class T>
T down(T, T, T);

template<class T>
T back(T, T, T);

template<class T>
T front(T, T, T);

#endif
