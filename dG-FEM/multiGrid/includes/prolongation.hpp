
/*-----------------------------------------------------------------------*\
   	      __________________________________________________
	     /                                                 /|
	    /_________________________________________________/ |
	   |                                                 |  |
	   |     __  _______     _______    _____  __   __   |  |
	   |    |  |/   ____|   /   ____|  |  _  \|  | |  |  |  |
	   |    |  |   /       |   /       | |_)  |  | |  |  |  |
	   |  __|  |  |   _____|  |   _____|   __/|  | |  |  |  |
	   | /  _  |  |  |_   _|  |  |_   _|  |   |  |_|  |  |  |
	   ||  (_| |   \__/  / |   \__/  / |  |   |       |  |  |
	   | \_____|\_______/   \_______/  |__|    \_____/   |  |
	   |                                                 |  |
           |    discontinuous Galerkin    On      GPGPU      | /
	   |_________________________________________________|/

--------------------------------------------------------------------------

Author

    Mehdi Baba Mehdi
    mehdi.baba_mehdi@uni-wuppertal.de

Date  
    Fri 10.04.2020
\*-----------------------------------------------------------------------*/

#ifndef PROLONGATION_HPP
#define PROLONGATION_HPP

#define __CL_ENABLE_EXCEPTIONS

#ifdef LOCAL_CL
#include "CL/cl.hpp"
#else
#include <CL/cl.hpp>
#endif

#include <iostream>
#include <iomanip>
#include "CL_ERROR.hpp"
#include "defines.hpp"
#include <chrono>
#include <cmath>


template<class T>
class dGProlStencilCreator;


template<class T>
class prolongation
{
  protected:
    int stencilCellWidth = 2;   // stencil number of cell (in each dir)
    int localCellNo = 1;        // local number of cell (in each dir)
    int prolStencilSize;        // size of prolongation stencil (number of nodes)
 
    int nodePerCellPerDir;
    int localSizeDir;           // size of stencil in dir

    cl::Kernel       prolKernel;
    cl::Buffer       prolStencilBuffer;

    long double timeOfOperation = 0.0;

    dGProlStencilCreator<T>* dGStencil;

  public:
     prolongation(cl::Program& _program, cl::CommandQueue& _queue,
                  const cl::Context& _context, const int& _nodePerCellPerDir);

    ~prolongation();

     const long double getTime() const;

     void setStencil(const cl::Context& context, cl::CommandQueue& queue);

     void createKernel(cl::Program& program);

     void linearInterpolation(cl::CommandQueue& queue, 
          cl::Buffer& inputBuffer, cl::Buffer& outputBuffer,
          const cl::NDRange& srcGlobalRange, const cl::NDRange& srcLocalRange, 
          const cl::size_t<3> maxSrcCellNo, 
          cl::Event& event);

          

};

#endif
