/*-----------------------------------------------------------------------*\
           ___________________________________________________
          /                                                  /|
         /                                                  / |
        |--------------------------------------------------|  |
        |    ______ __     __  ______    _____  __   __    |  |
        |   |   ___|  \   /  |/   ___|  |  _  \|  | |  |   |  |
        |   |  |_  |   \_/   |   /      | |_|  |  | |  |   |  |
        |   |    | |         |  |   ____|   __/|  | |  |   |  |
        |   |   _| |   ___   |  |  |_  _|  |   |  |_|  |   |  |
        |   |  |   |  |   |  |   \__/ / |  |   |       |   |  |
        |   |__|   |__|   |__|\______/  |__|    \_____/    |  |
        |                                                  |  |
        |      Full      Multigrid      On     GPGPU       | /
        |__________________________________________________|/

--------------------------------------------------------------------------

Author

    Mehdi Baba Mehdi
    mehdi.baba_mehdi@uni-wuppertal.de

Date  
    Wed 01.04.2020
\*-----------------------------------------------------------------------*/

#ifndef FULLMULTIGRID_HPP
#define FULLMULTIGRID_HPP

#define __CL_ENABLE_EXCEPTIONS

#ifdef LOCAL_CL
#include "CL/cl.hpp"
#else
#include <CL/cl.hpp>
#endif

#include <iostream>
#include <cassert>
#include <fstream>
#include <vector>
#include <string>
#include <cstring>
#include <cmath>
#include <iomanip>
#include <chrono>

#include "CL_ERROR.hpp"
#include "defines.hpp"
#include "solver.hpp"
#include "multiGrid.hpp"
#include "residual.hpp"
#include "relaxation.hpp"
#include "prolongation.hpp"
#include "restriction.hpp"
#include "gnuplot.hpp"


template<class T>
class restriction;

template<class T>
class gnuplot;

template<class T>
class relaxation;

template<class T>
class residual;

template<class T>
class prolonation;

template<class T>
class boundaryCondition;

template<class T>
class solver;

template<class T>
class multiGrid;

template<class T>
class fullMultiGrid : public multiGrid<T>
{
  protected:
    int nu_1 = 1;        // number of pre-smotting sweep
    int nu_2 = 1;        // number of post-smotting sweep

    std::string interpolationMethod = "linear";
  
    bool plotting = false;

    bool timing = false;
    double* fmgTime;
    double prolTime = 0;
    double restTime = 0;
    double residTime = 0;
    double relaxTime = 0;
       
  public:
    fullMultiGrid( T* _RHS,
            int _origCellNo_x,  int _origCellNo_y,  int _origCellNo_z,
           std::string _relaxationMethod, T _omega,
           cl_device_type _deviceType, int _deviceID,
           int _nu_1, int _nu_2, bool _plotting);

    fullMultiGrid(std::ifstream _RHSFileName,
            int _origCellNo_x,  int _origCellNo_y,  int _origCellNo_z, 
           std::string _relaxationMethod, T _omega,
           cl_device_type _deviceType, int _deviceID, 
           int _nu_1, int _nu_2, bool _plotting);
 
    fullMultiGrid( int _origCellNo_x,  int _origCellNo_y, int _origCellNo_z,
           std::string _relaxationMethod, T _omega,
           cl_device_type _deviceType, int _deviceID, 
           int _nu_1, int _nu_2, bool _plotting);

    fullMultiGrid(int _nu_1, int _nu_2, bool _plotting);
    
    fullMultiGrid(multiGrid<T>& _fmgMultiGrid, 
                  int _nu_1, int _nu_2, bool _plotting);

    ~fullMultiGrid();

    virtual void solve() override;

};


#endif
