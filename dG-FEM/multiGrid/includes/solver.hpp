
/*-----------------------------------------------------------------------*\
 	      __________________________________________________
	     /                                                 /|
	    /_________________________________________________/ |
	   |                                                 |  |
	   |     __  _______     _______    _____  __   __   |  |
	   |    |  |/   ____|   /   ____|  |  _  \|  | |  |  |  |
	   |    |  |   /       |   /       | |_)  |  | |  |  |  |
	   |  __|  |  |   _____|  |   _____|   __/|  | |  |  |  |
	   | /  _  |  |  |_   _|  |  |_   _|  |   |  |_|  |  |  |
	   ||  (_| |   \__/  / |   \__/  / |  |   |       |  |  |
	   | \_____|\_______/   \_______/  |__|    \_____/   |  |
	   |                                                 |  |
           |    discontinuous Galerkin    On      GPGPU      | /
	   |_________________________________________________|/

--------------------------------------------------------------------------

Author

    Mehdi Baba Mehdi
    mehdi.baba_mehdi@uni-wuppertal.de

Date  
    Wed 01.04.2020
\*---------------------------------------------------------------------------*/

#ifndef SOLVER_HPP
#define SOLVER_HPP

#define __CL_ENABLE_EXCEPTIONS

#ifdef LOCAL_CL
#include "CL/cl.hpp"
#else
#include <CL/cl.hpp>
#endif

#include <iostream>
#include <fstream>
#include <chrono>
#include <ctime>
#include <cmath>
#include "defines.hpp"
#include "rhsFunctionDict.hpp"
#include "boundaryCondition.hpp"
#include "gnuplot.hpp"

#include "../../dG/includes/dGLGLLocalMatrix.hpp"

template <class T>
class solver;


template <class T>
class boundaryCondition;


template <class T>
class dGLGLLocalMatrix;


template <class T>
std::ofstream& operator<<(std::ofstream& ofs, const solver<T>& result);

template <class T>
std::iostream& operator<<(std::iostream& os, const solver<T>& result);


template <class T>
class solver
{
  protected:
    T* RHS = NULL;  // b in Ax = b
    T* x   = NULL;  // x in Ax = b

    T initialValue        = 0.0;    
    T spatialLength       = 1.0;
    T origSpatialStepSize = 0.0;
    
    // Number of spatial steps in X, Y and Z directions
    int origCellNo_x  = 0;
    int origCellNo_y  = 0;
    int origCellNo_z  = 0;

    int totalNodeNo_x = 0;
    int totalNodeNo_y = 0;
    int totalNodeNo_z = 0;

    int polynomialOrder = 0;

    int nodePerCellPerDir = 0;

    int nodePerCell = 0;

    double tol = 1.0e-6;
    
    boundaryCondition<T>* bcObject;
    dGLGLLocalMatrix<T>*  dgLocalMatrix;
    
    friend class boundaryCondition<T>;

  public:
    solver();

    solver(T* _RHS,
           int _origCellNo_x, int _origCellNo_y, int _origCellNo_z,
           int _polynomialOrder, std::string _method, double _penalty);

    solver(std::ifstream& _RHSFileName,
           int _origCellNo_x, int _origCellNo_y, int _origCellNo_z,
           int _polynomialOrder, std::string _method, double _penalty);

    solver(int _origCellNo_x, int _origCellNo_y, int _origCellNo_z,
           int _polynomialOrder, std::string _method, double _penalty);

    solver(solver<T>& _solver,
           int _polynomialOrder, std::string _method, double _penalty);

    virtual ~solver();

    int getOrigCellNo_y() const;

    int getOrigCellNo_x() const;

    int getOrigCellNo_z() const;

    int getPolynomialOrder() const;

    int getNodePerCell() const;

    T getOrigSpatialStepSize() const;

    void printLabel();

    void addGhostCell();

    void readDataFromFile(std::ifstream& RHSFileName);

    virtual void solve();

    T u(T x, T y, T z);

    void calcX_exact(T* x_exact);

    void calcError(T* x_exact, T* x_app, T* error);

    double norm_2(const T* vec);

    double norm_L2(const T* vec, const int& domainSize, const T& spatialStepSize);

    T norm_inf(const T* vec);

    void setInitialValues(T initValue);

    void setRightHandSide();

    void setBoundaryCondition();

    friend std::ofstream& operator<< <>(std::ofstream& ofs, const solver<T>& result);

    friend std::iostream& operator<< <>(std::iostream& os, const solver<T>& result);
};


#endif
