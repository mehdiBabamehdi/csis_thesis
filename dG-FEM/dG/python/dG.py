

import sympy as sp
import numpy as np

class dG_local_matrix():
    
    def __init__(self, _pol_degree, _interval, _sigma, _penalty):
        self.x = sp.Symbol('x')
        self.y = sp.Symbol('y')
        self.z = sp.Symbol('z')
        self.u = sp.Symbol('u')
        self.a = sp.Symbol('a')
        self.f = sp.Symbol('f')


        # div(a grad(u)) = f
        self.u = self.x * (1 - self.x) * self.y * (1 - self.y) * self.z * (1 - self.z)
        self.a = self.u + 1
        
        # right hand side
        self.f = (self.y - sp.Pow(self.y,2)) * (self.y - sp.Pow(self.y,2)) * \
                 (self.z - sp.Pow(self.z,2)) * (self.z - sp.Pow(self.z,2)) * \
                 (2.0 * (self.x - sp.Pow(self.x,2)) - (1.0 - 2.0 * self.x) * (1.0 - 2.0 * self.x)) + \
                 (self.x - sp.Pow(self.x,2)) * (self.x - sp.Pow(self.x,2)) * \
                 (self.z - sp.Pow(self.z,2)) * (self.z - sp.Pow(self.z,2)) * \
                 (2.0 * (self.y - sp.Pow(self.y,2)) - (1.0 - 2.0 * self.y) * (1.0 - 2.0 * self.y)) + \
                 (self.x - sp.Pow(self.x,2)) * (self.x - sp.Pow(self.x,2)) * \
                 (self.y - sp.Pow(self.y,2)) * (self.y - sp.Pow(self.y,2)) * \
                 (2.0 * (self.z - sp.Pow(self.z,2)) - (1.0 - 2.0 * self.z)* (1.0 - 2.0 * self.z)) + \
                  2.0 * (self.y - sp.Pow(self.y,2)) * (self.z - sp.Pow(self.z,2)) + \
                  2.0 * (self.x - sp.Pow(self.x,2)) * (self.z - sp.Pow(self.z,2)) + \
                  2.0 * (self.x - sp.Pow(self.x,2)) * (self.y - sp.Pow(self.y,2))
        
        self.pol_degree = int(_pol_degree)
        self.interval   = int(_interval)
        self.penalty    = float(_penalty)
        self.sigma      = float(_sigma)
    
        self.phi = []
        self.create_basis_function()

    def create_basis_function(self):
        '''
         The function creates basis function as tensor products of function
            phi(t) = t^k
         where k is the order of polynomial
         '''
        node_number = self.pol_degree + 1

        # generate 2D and 3D list
        for k in range(node_number):
           first = []
           for i in range(node_number):
              second = []
              for j in range(node_number):
                 second.append(self.x)
              first.append(second)
           self.phi.append(first)
        
        # generate basis according to tensor products of phi(t) = t^k
        for k in range(node_number):
           for i in range(node_number):
              for j in range(node_number):
                 self.phi[i][j][k] = sp.Pow(self.x,i) * sp.Pow(self.y,j) * sp.Pow(self.z,k)


    def create_volume_integral(self):
        '''
          The function creates volume integral of dG-FEM descritisation
        '''
        # spatial step size
        h = 1.0 / self.interval

        # number of nodes in each direction
        node_number = self.pol_degree + 1

        A = []

        # matrix transfers reference element to actual element
        B = h * np.array([[1,1,1],[1,1,1],[1,1,1]])
        B_inv = np.linalg.pinv(B.transpose())
        B_norm = 6.0 * pow(h,3)

                
        for i in range(pow(node_number,3)):
          A1 = []
          for j in range(pow(node_number,3)):
            A1.append(0)
          A.append(A1)

        
        # produce local matrix (dG-FEM stencil) for constant Poisson equation
        for k in range(node_number):
           for i in range(node_number):
              for j in range(node_number):
                 grad_phi_1 = np.array([sp.diff(self.phi[i][j][k] , self.x), sp.diff(self.phi[i][j][k], self.y),\
                                       sp.diff(self.phi[i][j][k], self.z)]).transpose()
                 for l in range(node_number):
                    for m in range(node_number):
                       for n in range(node_number):
                          grad_phi_2 = np.array([sp.diff(self.phi[l][m][n], self.x), sp.diff(self.phi[l][m][n], self.y), \
                                                sp.diff(self.phi[l][m][n], self.z)]).transpose()
                          
                          A[k*pow(node_number,2)+i*node_number+j][l*pow(node_number,2)+m*node_number+n] = \
                                  B_norm * sp.integrate(np.matmul(B_inv, grad_phi_1).dot(np.matmul(B_inv, grad_phi_2)), \
                                                                         (self.x, 0, 1), \
                                                                         (self.y, 0, 1), \
                                                                         (self.z, 0, 1))
         


    def create_face_integral(self, normal):
        '''
          This function create face integral for given outward normal vector of the face,
          this combination of M_1_1 + M_2_2 + M_1_2 + M_2_1 which corresponds to the 
          M_1_1 (M_2_2) interactions of the local basis of the neighboring element Ee 1 (resp., Ee 2 ) 
          with itself and the term M_1_2 e (resp., m_2_1) corresponds to the interactions 
          of the local basis of the neighboring element Ee 1  (resp., Ee 2) with the element
          Ee 2 (resp., Ee 1 )
        '''
        # spatial step size
        h = 1.0 / self.interval

        # number of nodes in each direction
        node_number = self.pol_degree + 1

        M_1_1 = []
        M_1_2 = []
        M_2_1 = []
        M_2_2 = []

        # matrix transfers reference element to actual element
        B = h * np.array([[1,1,1],[1,1,1],[1,1,1]])
        B_inv = np.linalg.pinv(B.transpose())
        B_norm = 6.0 * pow(h,3)

                
        for i in range(pow(node_number,3)):
          M1 = []
          for j in range(pow(node_number,3)):
            M1.append(0)
          M_1_1.append(M1)
          M_1_2.append(M1)
          M_2_1.append(M1)
          M_2_2.append(M1)
        
        stencil = np.array([])
        
        # produce local matrix (dG-FEM stencil) for constant Poisson equation
        for k in range(node_number):
           for i in range(node_number):
              for j in range(node_number):
                 grad_phi_1 = np.array([sp.diff(self.phi[i][j][k] , self.x), sp.diff(self.phi[i][j][k], self.y),\
                                       sp.diff(self.phi[i][j][k], self.z)]).transpose()
                 for l in range(node_number):
                    for m in range(node_number):
                       for n in range(node_number):
                          grad_phi_2 = np.array([sp.diff(self.phi[l][m][n], self.x), sp.diff(self.phi[l][m][n], self.y), \
                                                sp.diff(self.phi[l][m][n], self.z)]).transpose()
                          
 
                          M_1_1[k*pow(node_number,2)+i*node_number+j][l*pow(node_number,2)+m*node_number+n] = \
                             M_1_1[k*pow(node_number,2)+i*node_number+j][l*pow(node_number,2)+m*node_number+n] \
                             - 0.5 * sp.integrate(np.matmul(B_inv, grad_phi_1).dot(normal) * self.phi[l][m][n], (self.y,0,1),(self.z,0,1)) \
                             + self.sigma / 2.0 * sp.integrate(np.matmul(B_inv, grad_phi_1).dot(normal) * self.phi[l][m][n], (self.y,0,1),(self.z,0,1)) \
                             + self.penalty * sp.integrate(self.phi[i][j][k] * self.phi[l][m][n], (self.x,0,1), (self.y,0,1),(self.z,0,1))

                          ''' the change of normal vector corresponds to contribution of face integral of adjacent cell
                              in B(u,v), integral over faces, has term of basis function of space of adjacent cell (jump or mean values) '''

                          normal = -1 * normal;
                          M_1_2[k*pow(node_number,2)+i*node_number+j][l*pow(node_number,2)+m*node_number+n] = \
                             M_1_2[k*pow(node_number,2)+i*node_number+j][l*pow(node_number,2)+m*node_number+n] \
                             - 0.5 * sp.integrate(np.matmul(B_inv, grad_phi_2).dot(normal) * self.phi[i][j][k], (self.y,0,1),(self.z,0,1)) \
                             - self.sigma / 2.0 * sp.integrate(np.matmul(B_inv, grad_phi_1).dot(normal) * self.phi[l][m][n], (self.y,0,1),(self.z,0,1)) \
                             - self.penalty * sp.integrate(self.phi[i][j][k] * self.phi[l][m][n], (self.x,0,1), (self.y,0,1),(self.z,0,1)) 
                
                          normal = -1 * normal;
                          M_2_1[k*pow(node_number,2)+i*node_number+j][l*pow(node_number,2)+m*node_number+n] = \
                             M_2_1[k*pow(node_number,2)+i*node_number+j][l*pow(node_number,2)+m*node_number+n] \
                             - 0.5 * sp.integrate(np.matmul(B_inv, grad_phi_1).dot(normal) * self.phi[l][m][n], (self.y,0,1),(self.z,0,1)) \
                             + self.sigma / 2.0 * sp.integrate(np.matmul(B_inv, grad_phi_1).dot(normal) * self.phi[l][m][n], (self.y,0,1),(self.z,0,1)) \
                             - self.penalty * sp.integrate(self.phi[i][j][k] *  self.phi[l][m][n], (self.x,0,1), (self.y,0,1),(self.z,0,1))
                
                          M_2_2[k*pow(node_number,2)+i*node_number+j][l*pow(node_number,2)+m*node_number+n] = \
                             M_2_2[k*pow(node_number,2)+i*node_number+j][l*pow(node_number,2)+m*node_number+n] \
                             + 0.5 * sp.integrate(np.matmul(B_inv, grad_phi_2).dot(normal) * self.phi[l][m][n], (self.y,0,1),(self.z,0,1)) \
                             - self.sigma / 2.0 * sp.integrate(np.matmul(B_inv, grad_phi_2).dot(normal) * self.phi[l][m][n], (self.y,0,1),(self.z,0,1)) \
                             + self.penalty * sp.integrate(self.phi[i][j][k] * self.phi[l][m][n], (self.x,0,1), (self.y,0,1),(self.z,0,1))
                          
                          stencil = np.append(stencil, M_1_1[k*pow(node_number,2)+i*node_number+j][l*pow(node_number,2)+m*node_number+n] +
                                                       M_1_2[k*pow(node_number,2)+i*node_number+j][l*pow(node_number,2)+m*node_number+n] +
                                                       M_2_1[k*pow(node_number,2)+i*node_number+j][l*pow(node_number,2)+m*node_number+n] +
                                                       M_2_2[k*pow(node_number,2)+i*node_number+j][l*pow(node_number,2)+m*node_number+n])

   
        return stencil

    def create_face_M11(self, normal):
         '''
          This function create face integral for given outward normal vector of the face,
          M_1_1 which is interactions of the local basis of the neighboring element Ee 1 with itself.
          this used when the face is boundary of the domain
        '''
        # spatial step size
        h = 1.0 / self.interval

        # number of nodes in each direction
        node_number = self.pol_degree + 1

        M_1_1 = []

        # matrix transfers reference element to actual element
        B = h * np.array([[1,1,1],[1,1,1],[1,1,1]])
        B_inv = np.linalg.pinv(B.transpose())
        B_norm = 6.0 * pow(h,3)

                
        for i in range(pow(node_number,3)):
          M1 = []
          for j in range(pow(node_number,3)):
            M1.append(0)
          M_1_1.append(M1)
        
        stencil = np.array([])
        
        # produce local matrix (dG-FEM stencil) for constant Poisson equation
        for k in range(node_number):
           for i in range(node_number):
              for j in range(node_number):
                 grad_phi_1 = np.array([sp.diff(self.phi[i][j][k] , self.x), sp.diff(self.phi[i][j][k], self.y),\
                                       sp.diff(self.phi[i][j][k], self.z)]).transpose()
                 for l in range(node_number):
                    for m in range(node_number):
                       for n in range(node_number):
                          grad_phi_2 = np.array([sp.diff(self.phi[l][m][n], self.x), sp.diff(self.phi[l][m][n], self.y), \
                                                sp.diff(self.phi[l][m][n], self.z)]).transpose()
                          
 
                          M_1_1[k*pow(node_number,2)+i*node_number+j][l*pow(node_number,2)+m*node_number+n] = \
                             M_1_1[k*pow(node_number,2)+i*node_number+j][l*pow(node_number,2)+m*node_number+n] \
                             - 0.5 * sp.integrate(np.matmul(B_inv, grad_phi_1).dot(normal) * self.phi[l][m][n], (self.y,0,1),(self.z,0,1)) \
                             + self.sigma / 2.0 * sp.integrate(np.matmul(B_inv, grad_phi_1).dot(normal) * self.phi[l][m][n], (self.y,0,1),(self.z,0,1)) \
                             + self.penalty * sp.integrate(self.phi[i][j][k] * self.phi[l][m][n], (self.x,0,1), (self.y,0,1),(self.z,0,1))
                          
                          stencil = np.append(stencil, M_1_1[k*pow(node_number,2)+i*node_number+j][l*pow(node_number,2)+m*node_number+n])

   
        return stencil


    def create_right_hand_side(self):
        '''
         The function creates right hand side of the problem Au=f by using
         dG-FEM descritisation method. BC is supposed to be Dirichlet BC
         '''
        right_hand_side = np.array([])
        h = 1.0 / self.interval
        node_number = self.pol_degree + 1
        for k in range(self.interval):
           z_1 = k * h
           z_2 = (k + 1) * h
           for i in range(self.interval):
              x_1 = i * h
              x_2 = (i + 1) * h
              for j in range(self.interval):
                 y_1 = j * h
                 y_2 = (j + 1) * h
                 for l in range(node_number):
                    for m in range(node_number):
                       for n in range(node_number):
                          right_hand_side = np.append(right_hand_side, sp.integrate(self.f * self.phi[l][m][n],
                                                                                   (self.x, x_1, x_2),    
                                                                                   (self.y, y_1, y_2),    
                                                                                   (self.z, z_1, z_2)))    
    
        return right_hand_side
