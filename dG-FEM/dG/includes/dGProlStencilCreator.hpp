

/*-----------------------------------------------------------------------*\
	      __________________________________________________
	     /                                                 /|
	    /_________________________________________________/ |
	   |                                                 |  |
	   |     __  _______     _______    _____  __   __   |  |
	   |    |  |/   ____|   /   ____|  |  _  \|  | |  |  |  |
	   |    |  |   /       |   /       | |_)  |  | |  |  |  |
	   |  __|  |  |   _____|  |   _____|   __/|  | |  |  |  |
	   | /  _  |  |  |_   _|  |  |_   _|  |   |  |_|  |  |  |
	   ||  (_| |   \__/  / |   \__/  / |  |   |       |  |  |
	   | \_____|\_______/   \_______/  |__|    \_____/   |  |
	   |                                                 |  |
           |    discontinuous Galerkin    On      GPGPU      | /
	   |_________________________________________________|/


--------------------------------------------------------------------------

Author

    Mehdi Baba Mehdi
    mehdi.baba_mehdi@uni-wuppertal.de

Date  
    Wed 28.09.2020
\*---------------------------------------------------------------------------*/

#ifndef DGPROLSTENCILCREATOR_HPP
#define DGPROLSTENCILCREATOR_HPP

#include <iostream>
#include <cmath>
#include <string>

template <class T>
class dGProlStencilCreator;

template <class T>
std::ostream& operator<<(std::ostream& os, const dGProlStencilCreator<T>& stencil);

template <class T>
class dGProlStencilCreator
{
   protected:
    int stencilCol;

    int stencilRow;
    
    int blockNo = 2;

    T* prolStencil1d = NULL;
    T* restStencil1d = NULL;
    T* target = NULL;

   public:

    T* stencil3D = NULL;

    dGProlStencilCreator(const int _nodePerCellPerDir);
    
    ~dGProlStencilCreator();

    void kroneckerProduct3D(const T* blockC, const T* blockA, 
                            const T* blockB, T* resultMatrix);

    void copyBlock(const int blockIdx, T* blockMatrix);

    void copyResult(const int offset_k, const int offset_i, const int offset_j, 
                    const T* tmpMatrix);

    void kroneckerProduct(std::string stencil);
    
    friend std::ostream& operator<< <>(std::ostream& os, const dGProlStencilCreator<T>& stencil);
};
#endif


