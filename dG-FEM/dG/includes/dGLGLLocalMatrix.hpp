

/*-----------------------------------------------------------------------*\
  	      __________________________________________________
	     /                                                 /|
	    /_________________________________________________/ |
	   |                                                 |  |
	   |     __  _______     _______    _____  __   __   |  |
	   |    |  |/   ____|   /   ____|  |  _  \|  | |  |  |  |
	   |    |  |   /       |   /       | |_)  |  | |  |  |  |
	   |  __|  |  |   _____|  |   _____|   __/|  | |  |  |  |
	   | /  _  |  |  |_   _|  |  |_   _|  |   |  |_|  |  |  |
	   ||  (_| |   \__/  / |   \__/  / |  |   |       |  |  |
	   | \_____|\_______/   \_______/  |__|    \_____/   |  |
	   |                                                 |  |
           |    discontinuous Galerkin    On      GPGPU      | /
	   |_________________________________________________|/

--------------------------------------------------------------------------

Author

    Mehdi Baba Mehdi
    mehdi.baba_mehdi@uni-wuppertal.de

Date  
    Sat 12.09.2020
\*---------------------------------------------------------------------------*/

#ifndef DGLGLLOCALMATRIX_HPP
#define DGLGLLOCALMATRIX_HPP

#include <vector>
#include <cmath>
#include "/home/mehdi/library/boost/boost/math/special_functions/legendre.hpp"

using namespace boost;

template <class T>
class dGLGLLocalMatrix
{
   private:
     const double quadPoint[4] = {-1.0, -sqrt(5.0)/5.0, sqrt(5.0)/5.0, 1.0};
     const double weight[4]    = {1.0/6.0, 5.0/6.0, 5.0/6.0, 1.0/6.0};

     // outward normal vector
     double e_1[3] = {0.0,  0.0, -1.0};
     double e_2[3] = {-1.0, 0.0,  0.0};
     double e_3[3] = {0.0, -1.0,  0.0};
     double e_4[3] = {0.0,  1.0,  0.0};
     double e_5[3] = {1.0,  0.0,  0.0};
     double e_6[3] = {0.0,  0.0,  1.0};

     std::vector<double*> normal;

     std::string method;
     double sigma;
     double penalty;
     int polynomialOrder;
     int nodeNumber;

   public:
     dGLGLLocalMatrix(int _polynomialOrder, std::string _method, double _penalty);
     
     ~dGLGLLocalMatrix();

     std::string getMethod() const;

     double getSigma() const;

     double getPenalty() const;

     int getPolynomialOrder() const;

     double f(const double x, const double y, const double z);

     void createB(double* B_inv, const double h);

     double* MxV(const double* B_inv, const double* vector);

     double dotProduct(const double* vector_1, const double* vector_2);
     
     double* gradBasis(const int x_i,  const int y_i,  const int z_i, 
                       const int po_l, const int po_m, const int po_n); 

     void createVolumeIntegral(T* volumeLocalMatrix, const double h);
     
     void createFaceIntegral(T* M11, T* M12, T* M21, T* M22, 
                             const double h, const int faceNo);

     void createRightHandSide(const int dim, const double h, T* RHS);

};

#endif

