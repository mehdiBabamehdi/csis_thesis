
/*-----------------------------------------------------------------------*\
           ___________________________________________________
          /                                                  /|
         /                                                  / |
        |--------------------------------------------------|  |
        |    ______ __     __  ______    _____  __   __    |  |
        |   |   ___|  \   /  |/   ___|  |  _  \|  | |  |   |  |
        |   |  |_  |   \_/   |   /      | |_)  |  | |  |   |  |
        |   |    | |         |  |   ____|   __/|  | |  |   |  |
        |   |   _| |   ___   |  |  |_  _|  |   |  |_|  |   |  |
        |   |  |   |  |   |  |   \__/ / |  |   |       |   |  |
        |   |__|   |__|   |__|\______/  |__|    \_____/    |  |
        |                                                  |  |
        |      Full      Multigrid      On     GPGPU       | /
        |__________________________________________________|/

--------------------------------------------------------------------------

Author

    Mehdi Baba Mehdi
    mehdi.baba_mehdi@uni-wuppertal.de

Date  
    Wed 28.08.2020
\*---------------------------------------------------------------------------*/



#include "../includes/dGLocalMatrix.hpp"


// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

template <class T>
dGLocalMatrix<T>::dGLocalMatrix(int _polynomialOrder, double _sigma, double _penalty)
:
polynomialOrder{_polynomialOrder},
sigma{_sigma},
penalty{_penalty}
{}


// * * * * * * * * * * * * * * * * Destructor  * * * * * * * * * * * * * * * //

template <class T>
dGLocalMatrix<T>::~dGLocalMatrix()
{}


// * * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * //

template <class T>
void dGLocalMatrix<T>::createVolumeIntegral(const int dim, T* stencil)
{
    py::scoped_interpreter python;

    py::module np = py::module::import("numpy");

    py::object dgClass = py::module::import(moduleName).attr(className);
    
    py::object classObj = dgClass(polynomialOrder, dim, sigma, penalty); 
    
    py::object create_local_matrix = classObj.attr(methodVolumeIntegral);
    
    py::array_t<double> stencilPy = create_local_matrix();
    
    auto r = stencilPy.unchecked<1>();

    for (int i = 0; i < pow(polynomialOrder,6); ++i)
    {
       stencil[i] = r[i];
    }       
}

template <class T>
void dGLocalMatrix<T>::createFaceIntegral(const int dim, T* stencil)
{
    py::scoped_interpreter python;

    py::module np = py::module::import("numpy");

    py::object dgClass = py::module::import(moduleName).attr(className);
    
    py::object classObj = dgClass(polynomialOrder, dim, sigma, penalty); 
    
    py::object create_local_matrix = classObj.attr(methodFaceIntegral);
    
    py::array_t<double> stencilPy = create_local_matrix();
    
    auto r = stencilPy.unchecked<1>();

    for (int i = 0; i < pow(polynomialOrder,6); ++i)
    {
       stencil[i] = r[i];
    }       
}


template <class T>
void dGLocalMatrix<T>::createFaceM11Integral(const int dim, T* stencil)
{
    py::scoped_interpreter python;

    py::module np = py::module::import("numpy");

    py::object dgClass = py::module::import(moduleName).attr(className);
    
    py::object classObj = dgClass(polynomialOrder, dim, sigma, penalty); 
    
    py::object create_local_matrix = classObj.attr(methodFaceM11);
    
    py::array_t<double> stencilPy = create_local_matrix();
    
    auto r = stencilPy.unchecked<1>();

    for (int i = 0; i < pow(polynomialOrder,6); ++i)
    {
       stencil[i] = r[i];
    }       
}
template <class T>
void dGLocalMatrix<T>::createRightHandSide(const int dim, T* stencil)
{
    py::scoped_interpreter python;

    py::module np = py::module::import("numpy");

    py::object dgClass = py::module::import(moduleName).attr(className);
    
    py::object classObj = dgClass(polynomialOrder, dim, sigma, penalty); 
    
    py::object create_local_matrix = classObj.attr(methodRHS);
    
    py::array_t<double> stencilPy = create_local_matrix();
    
    auto r = stencilPy.unchecked<1>();

    for (int i = 0; i < pow(polynomialOrder,6); ++i)
    {
       stencil[i] = r[i];
    }       
}

