
/*-----------------------------------------------------------------------*\
           ___________________________________________________
          /                                                  /|
         /                                                  / |
        |--------------------------------------------------|  |
        |    ______ __     __  ______    _____  __   __    |  |
        |   |   ___|  \   /  |/   ___|  |  _  \|  | |  |   |  |
        |   |  |_  |   \_/   |   /      | |_)  |  | |  |   |  |
        |   |    | |         |  |   ____|   __/|  | |  |   |  |
        |   |   _| |   ___   |  |  |_  _|  |   |  |_|  |   |  |
        |   |  |   |  |   |  |   \__/ / |  |   |       |   |  |
        |   |__|   |__|   |__|\______/  |__|    \_____/    |  |
        |                                                  |  |
        |      Full      Multigrid      On     GPGPU       | /
        |__________________________________________________|/

--------------------------------------------------------------------------

Author

    Mehdi Baba Mehdi
    mehdi.baba_mehdi@uni-wuppertal.de

Date  
    Sat 12.09.2020
\*---------------------------------------------------------------------------*/



#include "../includes/dGLGLLocalMatrix.hpp"


// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

template <class T>
dGLGLLocalMatrix<T>::dGLGLLocalMatrix(int _polynomialOrder, std::string _method, 
                                      double _penalty)
        :
        polynomialOrder{_polynomialOrder},
        method{_method},
        penalty{_penalty}
{
  if (method == "SIP")
  {
     sigma = -1.0;
  } else if (method == "NIP")
  {
     sigma = 1.0;
  } else 
  {
     std::cerr << "dG class, dG method is not correct!" << std::endl;
     exit(1);
  }

  nodeNumber = polynomialOrder + 1;
  
  normal.push_back(e_1);
  
  normal.push_back(e_2);

  normal.push_back(e_3);

  normal.push_back(e_4);

  normal.push_back(e_5);

  normal.push_back(e_6);
}


// * * * * * * * * * * * * * * * * Destructor  * * * * * * * * * * * * * * * //

template <class T>
dGLGLLocalMatrix<T>::~dGLGLLocalMatrix()
{}


// * * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * //

template <class T>
inline std::string dGLGLLocalMatrix<T>::getMethod() const
{
   return method;
}


template <class T>
inline double dGLGLLocalMatrix<T>::getSigma() const
{
   return sigma;
}



template <class T>
inline double dGLGLLocalMatrix<T>::getPenalty() const
{
   return penalty;
}



template <class T>
inline int dGLGLLocalMatrix<T>::getPolynomialOrder() const
{
   return polynomialOrder;
}



template <class T>
double dGLGLLocalMatrix<T>::f(const double x, const double y, const double z)
{

  // u = (x^2-x^4)(y^4-y^2)(z^2-z^4
  return (2.0 * ((1.0 - 6.0 * std::pow(x,2)) * 
                 (std::pow(y,2) - std::pow(y,4)) * 
                 (std::pow(z,2) - std::pow(z,4)) +
         (1.0 - 6.0 * std::pow(y,2)) * 
                 (std::pow(x,2) - std::pow(x,4)) * 
                 (std::pow(z,2) - std::pow(z,4)) +
         (1.0 - 6.0 * std::pow(z,2)) * 
                 (std::pow(y,2) - std::pow(y,4)) * 
                 (std::pow(x,2) - std::pow(x,4))));
}


template <class T>
void dGLGLLocalMatrix<T>::createB(double* B_inv, const double h)
{
   B_inv[0] = 2.0 / h;
   B_inv[4] = 2.0 / h;
   B_inv[8] = 2.0 / h;
}


template <class T>
double* dGLGLLocalMatrix<T>::MxV(const double* B_inv, const double* vector)
{
  double* result = new double[3](); 
  
  for (int i = 0; i < 3; ++i)
  {
    for (int j = 0; j < 3; ++j)
    {
      result[i] += B_inv[i * 3 + j] * vector[j];
    }
  }

  return result;
}


template <class T>
double dGLGLLocalMatrix<T>::dotProduct(const double* vector_1, 
                                       const double* vector_2)
{
  double result = 0.0;

  for (int i = 0; i < 3; ++i)
  {
    result += vector_1[i] * vector_2[i];
  }

  return result;
}


template <class T>
double* dGLGLLocalMatrix<T>::gradBasis(const int i, const int j, const int k, 
                                       const int po_l, const int po_m, const int po_n)
{
   double* gradVec = new double[3]();

   gradVec[0] = boost::math::legendre_p_prime<double>(po_l, quadPoint[i]) * 
                boost::math::legendre_p<double>(po_m, quadPoint[j]) * 
                boost::math::legendre_p<double>(po_n, quadPoint[k]);
   
   gradVec[1] = boost::math::legendre_p_prime<double>(po_m, quadPoint[j]) * 
                boost::math::legendre_p<double>(po_l, quadPoint[i]) * 
                boost::math::legendre_p<double>(po_n, quadPoint[k]);
   
   gradVec[2] = boost::math::legendre_p_prime<double>(po_n, quadPoint[k]) * 
                boost::math::legendre_p<double>(po_l, quadPoint[i]) * 
                boost::math::legendre_p<double>(po_m, quadPoint[j]);

   return gradVec;
}


template <class T>
void dGLGLLocalMatrix<T>::createVolumeIntegral(T* volumeLocalMatrix, const double h)
{
    double* vec_1 = new double[3]();
    double* vec_2 = new double[3]();
    double* vec_3 = new double[3]();
    double* vec_4 = new double[3]();
    double result;
    int position;

    double* B_inv = new double[9]();

    createB(B_inv, h);

    double B_norm = std::pow(h / 2.0, 3);

    // loop over quadrature point
    for (int i = 0; i < nodeNumber; ++i)
    {
        for (int j = 0; j < nodeNumber; ++j)
        {
           for (int k = 0; k < nodeNumber; ++k)
           {
              // loop over basis function of test function
              for (int l = 0; l < nodeNumber; ++l)
              {
                 for (int m = 0; m < nodeNumber; ++m)
                 {
                    for (int n = 0; n < nodeNumber; ++n)
                    {
                       // loop over basis function generates U
                       for (int o = 0; o < nodeNumber; ++o)
                       {
                          for (int p = 0; p < nodeNumber; ++p)
                          {
                             for (int q = 0; q < nodeNumber; ++q)
                             {
                                position = (l * std::pow(nodeNumber,2) + m * nodeNumber + n) 
                                              * std::pow(nodeNumber,3) + 
                                            o * std::pow(nodeNumber,2) + p * nodeNumber + q;

                                vec_3 = gradBasis(i,j,k,l,m,n);
                                vec_4 = gradBasis(i,j,k,o,p,q);
                                 
                                vec_1 = MxV(B_inv, vec_3);
                                vec_2 = MxV(B_inv, vec_4);
                                result = dotProduct(vec_1, vec_2);

                                volumeLocalMatrix[position] += B_norm * weight[i] * 
                                                               weight[j] * weight[k] * result;
                             }
                          }
                       }
                    }
                 }
              }
           }
        }
    }
}



template <class T>
void dGLGLLocalMatrix<T>::createFaceIntegral(T* M11, T* M12, T* M21, T* M22, 
                            const double h, const int faceNo)
{
    double* vec1 = new double[3]();
    double* vec2 = new double[3]();
    double* vec3 = new double[3]();
    double* vec4 = new double[3]();
    double result1;
    double result2;

    int localPosition;

    double* B_inv = new double[9]();

    createB(B_inv, h);

    double face_surface = h * h;

    // loop over quadrature point
    for (int i = 0; i < nodeNumber; ++i)
    {
        for (int j = 0; j < nodeNumber; ++j)
        {
           for (int k = 0; k < nodeNumber; ++k)
           {
              // loop over basis function of test function (omega_h)
              for (int l = 0; l < nodeNumber; ++l)
              {
                 for (int m = 0; m < nodeNumber; ++m)
                 {
                    for (int n = 0; n < nodeNumber; ++n)
                    {
                       // loop over basis function of U
                       for (int o = 0; o < nodeNumber; ++o)
                       {
                          for (int p = 0; p < nodeNumber; ++p)
                          {
                             for (int q = 0; q < nodeNumber; ++q)
                             {
 
                                localPosition = (l * std::pow(nodeNumber,2) + m * nodeNumber + n) *
                                                std::pow(nodeNumber,3) + o * std::pow(nodeNumber,2) + 
                                                p * nodeNumber + q;
                                // Omega 
                                vec1 = gradBasis(i,j,k,l,m,n);
                                vec2 = MxV(B_inv, vec1);
                                result1 = dotProduct(vec2, normal[faceNo]);
                                // U
                                vec3 = gradBasis(i,j,k,o,p,q);
                                vec4 = MxV(B_inv, vec1);
                                result2 = dotProduct(vec4, normal[faceNo]);
                                
                                M11[localPosition] += - 0.5 * face_surface * weight[i] * weight[j] * weight[k] *
                                                              boost::math::legendre_p<double>(l,quadPoint[i]) * 
                                                              boost::math::legendre_p<double>(m,quadPoint[j]) * 
                                                              boost::math::legendre_p<double>(n,quadPoint[k]) * result2
                                                      + 0.5 * sigma * face_surface * weight[i] * weight[j] * weight[k] *
                                                              boost::math::legendre_p<double>(o,quadPoint[i]) * 
                                                              boost::math::legendre_p<double>(p,quadPoint[j]) * 
                                                              boost::math::legendre_p<double>(q,quadPoint[k]) * result1
                                                      + penalty / h * face_surface * weight[i] * weight[j] * weight[k] *
                                                              boost::math::legendre_p<double>(l,quadPoint[i]) * 
                                                              boost::math::legendre_p<double>(m,quadPoint[j]) * 
                                                              boost::math::legendre_p<double>(n,quadPoint[k]) *
                                                              boost::math::legendre_p<double>(o,quadPoint[i]) * 
                                                              boost::math::legendre_p<double>(p,quadPoint[j]) * 
                                                              boost::math::legendre_p<double>(q,quadPoint[k]);

                                // Omega
                                vec1 = gradBasis(i,j,k,l,m,n);
                                vec2 = MxV(B_inv, vec1);
                                result1 = dotProduct(vec2, normal[faceNo]);
                                // U
                                vec3 = gradBasis(i,j,k,o,p,q);
                                vec4 = MxV(B_inv, vec3);
                                result2 = dotProduct(vec2, normal[faceNo]);
                                M12[localPosition] += -  0.5 * face_surface * weight[i] * weight[j] * weight[k] *
                                                               boost::math::legendre_p<double>(l,quadPoint[i]) * 
                                                               boost::math::legendre_p<double>(m,quadPoint[j]) * 
                                                               boost::math::legendre_p<double>(n,quadPoint[k]) * result2
                                                       - 0.5 * sigma * face_surface * weight[i] * weight[j] * weight[k] *
                                                               boost::math::legendre_p<double>(o,quadPoint[i]) * 
                                                               boost::math::legendre_p<double>(p,quadPoint[j]) * 
                                                               boost::math::legendre_p<double>(q,quadPoint[k]) * result1
                                                       - penalty / h * face_surface * weight[i] * weight[j] * weight[k] *
                                                               boost::math::legendre_p<double>(l,quadPoint[i]) * 
                                                               boost::math::legendre_p<double>(m,quadPoint[j]) * 
                                                               boost::math::legendre_p<double>(n,quadPoint[k]) *
                                                               boost::math::legendre_p<double>(o,quadPoint[i]) * 
                                                               boost::math::legendre_p<double>(p,quadPoint[j]) * 
                                                               boost::math::legendre_p<double>(q,quadPoint[k]);
                    
                                // minus in calculation of result is due to revesing normal vector
                                // Omega
                                vec1 = gradBasis(i,j,k,l,m,n);
                                vec2 = MxV(B_inv, vec1);
                                result1 = - dotProduct(vec2, normal[faceNo]);
                                // U
                                vec3 = gradBasis(i,j,k,o,p,q);
                                vec4 = MxV(B_inv, vec3);
                                result2 = - dotProduct(vec2, normal[faceNo]);
                                M21[localPosition] +=   0.5 * face_surface * weight[i] * weight[j] * weight[k] *
                                                              boost::math::legendre_p<double>(l,quadPoint[i]) *
                                                              boost::math::legendre_p<double>(m,quadPoint[j]) *
                                                              boost::math::legendre_p<double>(n,quadPoint[k]) * result2
                                                      + 0.5 * sigma * face_surface * weight[i] * weight[j] * weight[k] *
                                                              boost::math::legendre_p<double>(o,quadPoint[i]) *
                                                              boost::math::legendre_p<double>(p,quadPoint[j]) *
                                                              boost::math::legendre_p<double>(q,quadPoint[k]) * result1
                                                      - penalty / h * face_surface * weight[i] * weight[j] * weight[k] *
                                                              boost::math::legendre_p<double>(l,quadPoint[i]) *
                                                              boost::math::legendre_p<double>(m,quadPoint[j]) *
                                                              boost::math::legendre_p<double>(n,quadPoint[k]) *
                                                              boost::math::legendre_p<double>(o,quadPoint[i]) *
                                                              boost::math::legendre_p<double>(p,quadPoint[j]) *
                                                              boost::math::legendre_p<double>(q,quadPoint[k]);

                                // Omega
                                vec1 = gradBasis(i,j,k,l,m,n);
                                vec2 = MxV(B_inv, vec1);
                                result1 = - dotProduct(vec2, normal[faceNo]);
                                // U
                                vec3 = gradBasis(i,j,k,o,p,q);
                                vec4 = MxV(B_inv, vec3);
                                result2 = - dotProduct(vec2, normal[faceNo]);
                                M22[localPosition] +=    0.5 * face_surface * weight[i] * weight[j] * weight[k] *
                                                              boost::math::legendre_p<double>(l,quadPoint[i]) *
                                                              boost::math::legendre_p<double>(m,quadPoint[j]) *
                                                              boost::math::legendre_p<double>(n,quadPoint[k]) * result2
                                                      - 0.5 * sigma * face_surface * weight[i] * weight[j] * weight[k] *
                                                              boost::math::legendre_p<double>(o,quadPoint[i]) *
                                                              boost::math::legendre_p<double>(p,quadPoint[j]) *
                                                              boost::math::legendre_p<double>(q,quadPoint[k]) * result1
                                                      + penalty / h * face_surface * weight[i] * weight[j] * weight[k] *
                                                              boost::math::legendre_p<double>(l,quadPoint[i]) *
                                                              boost::math::legendre_p<double>(m,quadPoint[j]) *
                                                              boost::math::legendre_p<double>(n,quadPoint[k]) *
                                                              boost::math::legendre_p<double>(o,quadPoint[i]) *
                                                              boost::math::legendre_p<double>(p,quadPoint[j]) *
                                                              boost::math::legendre_p<double>(q,quadPoint[k]);
                             }
                          }
                       }
                    }
                 }
              }
           }
        }
    }
}


/*
 * NOTE: vector of right hand side generated in a way that loop over 
 *       cells and in each cells loop over quadrature point. 
 *       i.e. i = cell number + quadrature point
 */
template <class T>
void dGLGLLocalMatrix<T>::createRightHandSide(const int dim, const double h, T* RHS)
{
    std::cout << std::setfill(' ') << std::setw(40);
    std::cout << std::left << "==> Creation of Right Hand Size";
    
    double fValue;
    double x_p, y_p, z_p;
    int globalIndex;

    const int nodePerCell = nodeNumber * nodeNumber * nodeNumber;

    double B_norm = std::pow(h / 2.0, 3);

    // loop over cells
    // dim subtracted by 2 to only fill original domain
    for (int k = 0; k < dim - 2; ++k)
    {
        for (int i = 0; i < dim - 2; ++i)
        {
           for (int j = 0; j < dim - 2; ++j)
           {
              // loop over quadrature Point
              for (int l = 0; l < nodeNumber; ++l)
              {
                 for (int m = 0; m < nodeNumber; ++m)
                 {
                    for (int n = 0; n < nodeNumber; ++n)
                    {
                       // loop over basis function of test function
                       for (int o = 0; o < nodeNumber; ++o)
                       {
                          for (int p = 0; p < nodeNumber; ++p)
                          {
                             for (int q = 0; q < nodeNumber; ++q)
                             {
                                 // nodes on faces of E1 & E2 have same coordinates
                                 // coordinate on refrence cell should be transfer
                                 // to global coordinates
                                 // here we do not consider ghost cell in index since
                                 // k, i, j start from zero
                                 z_p = k * h + (quadPoint[l] - quadPoint[0]) * h / 2.0;
                                 y_p = j * h + (quadPoint[m] - quadPoint[0]) * h / 2.0;
                                 x_p = i * h + (quadPoint[n] - quadPoint[0]) * h / 2.0;

                                 fValue = f(x_p, y_p, z_p);
                                 
                                 // +1 is for leaving ghost cell zero
                                 globalIndex = ((k + 1) * dim * dim +
                                                (i + 1) * dim + (j + 1)) * nodePerCell  +
                                                 o * nodeNumber * nodeNumber +
                                                 p * nodeNumber + q;

                                 RHS[globalIndex] += B_norm * fValue *
                                                     weight[l] * boost::math::legendre_p<double>(o, quadPoint[l]) *
                                                     weight[m] * boost::math::legendre_p<double>(p, quadPoint[m]) *
                                                     weight[n] * boost::math::legendre_p<double>(q, quadPoint[n]);
                           }
                        }
                     }
                  }
               }
            }
         }
      }
   }

   std::cout << std::right << "  -> Done! " << std::endl;
}
 
