/*-----------------------------------------------------------------------*\
 	      __________________________________________________
	     /                                                 /|
	    /_________________________________________________/ |
	   |                                                 |  |
	   |     __  _______     _______    _____  __   __   |  |
	   |    |  |/   ____|   /   ____|  |  _  \|  | |  |  |  |
	   |    |  |   /       |   /       | |_)  |  | |  |  |  |
	   |  __|  |  |   _____|  |   _____|   __/|  | |  |  |  |
	   | /  _  |  |  |_   _|  |  |_   _|  |   |  |_|  |  |  |
	   ||  (_| |   \__/  / |   \__/  / |  |   |       |  |  |
	   | \_____|\_______/   \_______/  |__|    \_____/   |  |
	   |                                                 |  |
           |    discontinuous Galerkin    On      GPGPU      | /
	   |_________________________________________________|/

--------------------------------------------------------------------------

Author

    Mehdi Baba Mehdi
    mehdi.baba_mehdi@uni-wuppertal.de

Date  
    Wed 28.09.2020

NOTE: The stencil took from 
  Hemker, P. W. and Hoffmann, W. and van Raalte, M. H,
   TWO-LEVEL FOURIER ANALYSIS OF A MULTIGRID APPROACH 
   FOR DISCONTINUOUS GALERKIN DISCRETIZATION, 
   SIAM Journal on Scientific Computing, 2004
\*---------------------------------------------------------------------------*/

#include "../includes/dGProlStencilCreator.hpp"


// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

template <class T>
dGProlStencilCreator<T>::dGProlStencilCreator(const int _nodePerCellPerDir) 
                     :
                     stencilRow{_nodePerCellPerDir}
{
   stencilCol = _nodePerCellPerDir * blockNo;

   prolStencil1d   = new T[blockNo * stencilCol * stencilRow]();
   restStencil1d   = new T[blockNo * stencilCol * stencilRow]();

   int blockSize3D = std::pow(stencilRow, 3) * std::pow(stencilRow, 3);

   stencil3D   = new T[(int)(std::pow(blockNo, 3)) * blockSize3D]();

   prolStencil1d[0]  =   1.0;
   prolStencil1d[4]  =   1.0 / 2.0;
   prolStencil1d[5]  =   1.0 / 8.0;
   prolStencil1d[6]  =   1.0 / 8.0;
   prolStencil1d[7]  =   1.0 / 2.0;
   prolStencil1d[9]  =   3.0 / 8.0;
   prolStencil1d[10] = - 1.0 / 8.0;
   prolStencil1d[14] =   1.0 / 4.0;
   prolStencil1d[17] =   1.0 / 4.0;
   prolStencil1d[21] = - 1.0 / 8.0;
   prolStencil1d[22] =   3.0 / 8.0;
   prolStencil1d[24] =   1.0 / 2.0;
   prolStencil1d[25] =   1.0 / 8.0;
   prolStencil1d[26] =   1.0 / 8.0;
   prolStencil1d[27] =   1.0 / 2.0;
   prolStencil1d[31] =   1.0 ;

   restStencil1d[1]  = 1.0;
   restStencil1d[9]  = 3.0;
   restStencil1d[14] = 1.0;
   restStencil1d[17] = 1.0;
   restStencil1d[22] = 3.0;
   restStencil1d[31] = 1.0;
}

// * * * * * * * * * * * * * * * * Destructor  * * * * * * * * * * * * * * * /

template <class T>
dGProlStencilCreator<T>::~dGProlStencilCreator()
{
}


// * * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * //

template <class T>
void dGProlStencilCreator<T>::kroneckerProduct3D(const T* blockC, const T* blockA, 
                                                 const T* blockB, T* resultMatrix)
{
    T* tmpMatrix = new T[(int)(std::pow(stencilRow, 4))]();

    for (int ari = 0; ari < stencilRow; ++ari)
    {
       for (int aci = 0; aci < stencilRow; ++aci)
       {
           for (int bri = 0; bri < stencilRow; ++bri)
           {
              for (int bci = 0; bci < stencilRow; ++bci)
              {
                tmpMatrix[(ari * stencilRow + bri) * stencilRow * stencilRow + 
                           aci * stencilRow + bci] = blockA[ari * stencilRow + aci] * 
                                            blockB[bri * stencilRow + bci];
              }
           }
       }
    }
 
    
    for (int cri = 0; cri < stencilRow; ++cri)
    {
       for (int cci = 0; cci < stencilRow; ++cci)
       {
          for (int tri = 0; tri < stencilRow * stencilRow; ++tri)
          {
             for (int tci = 0; tci < stencilRow * stencilRow; ++tci)
             {
                resultMatrix[(cri * stencilRow * stencilRow + tri) * 
                             (int)(std::pow(stencilRow,3)) + 
                              cci * stencilRow * stencilRow + tci] = 
                              blockC[cri * stencilRow + cci] * 
                              tmpMatrix[tri * stencilRow * stencilRow + tci];

             }
          }
       }
    } 
}

  
// copy block of matrix prolStencil1d
template <class T>
void dGProlStencilCreator<T>::copyBlock(const int blockIdx, T* blockMatrix)
{
    int offset_col = blockIdx * stencilRow;

    for (int ri = 0; ri < stencilRow; ++ri)
    {
       for (int ci = 0; ci < stencilRow; ++ci)
       {
           blockMatrix[ri * stencilRow + ci] = 
                          target[ri * stencilRow + ci + offset_col];
       }
    }
}


// copy block of matrix produced by Kronecker product of block of matrix
// A, B, C into correct place on resultMatrix
template <class T>
void dGProlStencilCreator<T>::copyResult(const int offset_k, const int offset_i, 
                                         const int offset_j, const T* tmpMatrix)
{
   int blockSize = stencilRow * stencilRow * stencilRow;

   for (int ri = 0; ri < blockSize; ++ri)
   {
      for (int bi = 0; bi < blockSize; ++bi)
      {
          stencil3D[offset_k + offset_i + offset_j + ri * blockSize + bi] = 
                                                  tmpMatrix[ri * blockSize + bi];
      }
   }   
}

// select block of matrix of A, B, C, do Kronecker product of
// CAB and copy the result block of matrix into result matrix
template <class T>
void dGProlStencilCreator<T>::kroneckerProduct(std::string stencil)
{
    if (stencil == "prolongation")
    {
       target = prolStencil1d;
    } else if (stencil == "restriction")
    {
       target = restStencil1d;
    } else
    {
       std::cout << "dG class, stencil not specified" << std::endl;
       exit(1);
    }

    int blockSize3D = std::pow(stencilRow * stencilRow * stencilRow, 2);
    int offset_i, offset_j, offset_k;
  
    T* aBlock = new T[stencilRow * stencilRow]();
    T* bBlock = new T[stencilRow * stencilRow]();
    T* cBlock = new T[stencilRow * stencilRow]();


    T* tmpMatrix    = new T[blockSize3D]();
    stencil3D = new T[blockSize3D * blockNo * blockNo * blockNo]();

    // C * A * B
    // loop over block of matrix C
    for (int cbi = 0; cbi < blockNo; ++cbi)
    {
       offset_k = cbi * blockSize3D * blockNo * blockNo;
       copyBlock(cbi , cBlock);

       // loop over block of matrix A
       for (int abi = 0; abi < blockNo; ++abi)
       {
           offset_i = abi * blockSize3D * blockNo;
           copyBlock(abi , aBlock);

           // loop over block of matrix B
           for (int bbi = 0; bbi < blockNo; ++bbi)
           {
              offset_j = bbi * std::pow(stencilRow, 3);
              copyBlock(abi, bBlock);

              kroneckerProduct3D(cBlock, aBlock, bBlock, tmpMatrix);

              copyResult(offset_k, offset_i, offset_j, tmpMatrix); 
           }
       }
    }
}

template <class T>
std::ostream& operator<< (std::ostream& os, const dGProlStencilCreator<T>& stencil)
{
   int row3D = std::pow(stencil.stencilRow, 3);

   int idx;
   
   for (int k = 0; k < stencil.blockNo; ++k)
   {
      for (int i = 0; i < stencil.blockNo; ++i)
      {
         for (int j = 0; j < stencil.blockNo; ++j)
         {
            for (int bi = 0; bi < row3D; ++bi)
            {
               for (int bj = 0; bj < row3D; ++bj)
               {
                 idx = (k * stencil.blockNo * stencil.blockNo + i * stencil.blockNo + j) * row3D * row3D +
                        bi * row3D + bj;
                  os << stencil.stencil3D[idx] << "   ";
               }
               os << std::endl;
            }
               os << std::endl << std::endl;
         }
      }
   }

   return os;
}
