
/*-----------------------------------------------------------------------*\
           ___________________________________________________
          /                                                  /|
         /                                                  / |
        |--------------------------------------------------|  |
        |    ______ __     __  ______    _____  __   __    |  |
        |   |   ___|  \   /  |/   ___|  |  _  \|  | |  |   |  |
        |   |  |_  |   \_/   |   /      | |_)  |  | |  |   |  |
        |   |    | |         |  |   ____|   __/|  | |  |   |  |
        |   |   _| |   ___   |  |  |_  _|  |   |  |_|  |   |  |
        |   |  |   |  |   |  |   \__/ / |  |   |       |   |  |
        |   |__|   |__|   |__|\______/  |__|    \_____/    |  |
        |                                                  |  |
        |      Full      Multigrid      On     GPGPU       | /
        |__________________________________________________|/

--------------------------------------------------------------------------

Author

    Mehdi Baba Mehdi
    mehdi.baba_mehdi@uni-wuppertal.de

Date  
    Wed 05.04.2020
\*-----------------------------------------------------------------------*/
/*
 * NOTE: T is type defined as preprocesor in "multigrid class" when openCL
 *       program is build. It automatically gets the type of template.
 *
 --------------------------------------------------------------------------*/




/*
 * The kernel used to implement direct solver
 */
__kernel void directSolver(__global T* x, int deviceHeight, 
                     int deviceWidth, int deviceDepth,
                     __constant T* stencil, int stencilWidth,
                     __global T* b,
                     __global T* y,
                     T h, int subt,
                     __local T* local_x, int localHeight, 
                     int localWidth, int localDepth)

{
   // padding for stencil
   int stencilRadius = (stencilWidth / 2);
   int padding      = stencilRadius * 2;
  
   // size of work group in output
   int groupStartCol   = get_group_id(0) * get_local_size(0);
   int groupStartRow   = get_group_id(1) * get_local_size(1);
   int groupStartSlice = get_group_id(2) * get_local_size(2);

   // local ID for work-items
   int localCol   = get_local_id(0);
   int localRow   = get_local_id(1);
   int localSlice = get_local_id(2);

   // global ID of each work-item
   int globalCol   = groupStartCol   + localCol;
   int globalRow   = groupStartRow   + localRow;
   int globalSlice = groupStartSlice + localSlice;

  
   int position = (globalSlice + stencilRadius) * deviceWidth * deviceHeight + 
                  (globalRow   + stencilRadius) * deviceWidth + 
                  (globalCol   + stencilRadius);
   /*_____________________ perform convolution_____________________*/
   
   if ((globalRow   < (deviceHeight - padding)) && 
       (globalCol   < (deviceWidth - subt - padding))  &&
       (globalSlice < (deviceDepth - padding)))
   {          
      *(y + position) = b[position] * pow(h,2) / diagCoeff;
   }
}



/*
 * The kernel used to implement Jacobi iterative scheme
 */
__kernel void jacobi(__global T* x, int deviceHeight, 
                     int deviceWidth, int deviceDepth,
                     __constant T* stencil, int stencilWidth,
                     __global T* b,
                     __global T* y,
                     T h, int subt,
                     __local T* local_x, int localHeight, 
                     int localWidth, int localDepth)

{
   // padding for stencil
   int stencilRadius = (stencilWidth / 2);
   int padding      = stencilRadius * 2;
  
   // size of work group in output
   int groupStartCol   = get_group_id(0) * get_local_size(0);
   int groupStartRow   = get_group_id(1) * get_local_size(1);
   int groupStartSlice = get_group_id(2) * get_local_size(2);

   // local ID for work-items
   int localCol   = get_local_id(0);
   int localRow   = get_local_id(1);
   int localSlice = get_local_id(2);

   // global ID of each work-item
   int globalCol   = groupStartCol   + localCol;
   int globalRow   = groupStartRow   + localRow;
   int globalSlice = groupStartSlice + localSlice;

   /*__________________Copy data to local memory___________________*/
   for (int k = localSlice; k < localDepth; k += get_local_size(2))
   {
      int currentSlice = groupStartSlice + k;
      // step down rows
      for (int i = localRow; i < localHeight; i += get_local_size(1))
      {
         int currentRow = groupStartRow + i;

         // step across columns
         for (int j = localCol; j < localWidth; j += get_local_size(0))
         {
            int currentCol = groupStartCol + j;

            // perform copy to local memory
            if (currentRow < deviceHeight && currentCol < deviceWidth && 
                currentSlice < deviceDepth)
            {
              *(local_x + k * localWidth * localHeight + i * localWidth + j) = 
             *(x + currentSlice * deviceWidth * deviceHeight + 
                   currentRow * deviceWidth + currentCol);
            }
         }
      }
   }

   barrier(CLK_LOCAL_MEM_FENCE);

   int position = (globalSlice + stencilRadius) * deviceWidth * deviceHeight + 
                  (globalRow   + stencilRadius) * deviceWidth + 
                  (globalCol   + stencilRadius);
   /*_____________________ perform convolution_____________________*/
   
   if ((globalRow   < (deviceHeight - padding)) && 
       (globalCol   < (deviceWidth - subt - padding))  &&
       (globalSlice < (deviceDepth - padding)))
   {     
     // Calculate right hand-side
     
     T sum = b[position] * pow(h,2) / diagCoeff * omega;

     int stencilIdx = 0;
     int offset;
     
     for (int k = localSlice; k < localSlice + stencilWidth; ++k)
     {
       for (int i = localRow; i < localRow + stencilWidth; ++i)
       {
         offset = k * localWidth * localHeight + i * localWidth + localCol;

         sum += local_x[offset++] * stencil[stencilIdx++];
         sum += local_x[offset++] * stencil[stencilIdx++];
         sum += local_x[offset++] * stencil[stencilIdx++];
       }
     }
     
     *(y + position) = sum;
   }
}


/*
 * The kernel used to implement red Guass-Seidel iterative scheme
 */
__kernel void GS_red(__global T* x, int deviceHeight, 
                     int deviceWidth, int deviceDepth,
                     __constant T* stencil, int stencilWidth,
                     __global T* b,
                     __global T* y,
                     T h, int subt,
                     __local T* local_x, int localHeight, 
                     int localWidth, int localDepth)

{
   // padding for stencil
   int stencilRadius = (stencilWidth / 2);
   int padding      = stencilRadius * 2;
  
   // size of work group in output
   int groupStartCol   = get_group_id(0) * get_local_size(0);
   int groupStartRow   = get_group_id(1) * get_local_size(1);
   int groupStartSlice = get_group_id(2) * get_local_size(2);

   // local ID for work-items
   int localCol   = get_local_id(0);
   int localRow   = get_local_id(1);
   int localSlice = get_local_id(2);

   // global ID of each work-item
   int globalCol   = groupStartCol   + localCol;
   int globalRow   = groupStartRow   + localRow;
   int globalSlice = groupStartSlice + localSlice;

   /*__________________Copy data to local memory___________________*/
   for (int k = localSlice; k < localDepth; k += get_local_size(2))
   {
      int currentSlice = groupStartSlice + k;
      // step down rows
      for (int i = localRow; i < localHeight; i += get_local_size(1))
      {
         int currentRow = groupStartRow + i;

         // step across columns
         for (int j = localCol; j < localWidth; j += get_local_size(0))
         {
            int currentCol = groupStartCol + j;

            // perform copy to local memory
            if (currentRow < deviceHeight && currentCol < deviceWidth && 
                currentSlice < deviceDepth)
            {
              *(local_x + k * localWidth * localHeight + i * localWidth + j) = 
             *(x + currentSlice * deviceWidth * deviceHeight + 
                   currentRow * deviceWidth + currentCol);
            }
         }
      }
   }

   barrier(CLK_LOCAL_MEM_FENCE);
   int position = (globalSlice + stencilRadius) * deviceWidth * deviceHeight + 
                  (globalRow   + stencilRadius) * deviceWidth + 
                  (globalCol   + stencilRadius);

   /*_____________________perform convolution_____________________*/
   
   if ((globalRow   < (deviceHeight - padding)) && 
       (globalCol   < (deviceWidth - subt - padding))  &&
       (globalSlice < (deviceDepth - padding))  &&
       (((globalSlice + stencilRadius) + (globalRow   + stencilRadius) + 
         (globalCol   + stencilRadius))%2 == 1)
      )
   {     
     // Calculate right hand-side
     
     T sum = b[position] * pow(h,2) / diagCoeff * omega;

     //printf("i = %d, j = %d, k = %d, sum = %d\n",globalRow   + stencilRadius, globalCol   + stencilRadius,  globalSlice + stencilRadius, ((globalSlice + stencilRadius) + (globalRow   + stencilRadius) + (globalCol   + stencilRadius)));
     int stencilIdx = 0;
     int offset;
     for (int k = localSlice; k < localSlice + stencilWidth; ++k)
     {
       for (int i = localRow; i < localRow + stencilWidth; ++i)
       {
         offset = k * localWidth * localHeight + i * localWidth + localCol;

         sum += local_x[offset++] * stencil[stencilIdx++];
         sum += local_x[offset++] * stencil[stencilIdx++];
         sum += local_x[offset++] * stencil[stencilIdx++];
       }
     }
     
     *(y + position) = sum;
   }
}


 /*
 * The kernel used to implement black Guass-Seidel iterative scheme
 */
__kernel void GS_black(__global T* x, int deviceHeight, 
                       int deviceWidth, int deviceDepth,
                       __constant T* stencil, int stencilWidth,
                       __global T* b,
                       __global T* y,
                       T h, int subt,
                       __local T* local_x, int localHeight, 
                       int localWidth, int localDepth)

{
   // padding for stencil
   int stencilRadius = (stencilWidth / 2);
   int padding      = stencilRadius * 2;
  
   // size of work group in output
   int groupStartCol   = get_group_id(0) * get_local_size(0);
   int groupStartRow   = get_group_id(1) * get_local_size(1);
   int groupStartSlice = get_group_id(2) * get_local_size(2);

   // local ID for work-items
   int localCol   = get_local_id(0);
   int localRow   = get_local_id(1);
   int localSlice = get_local_id(2);

   // global ID of each work-item
   int globalCol   = groupStartCol   + localCol;
   int globalRow   = groupStartRow   + localRow;
   int globalSlice = groupStartSlice + localSlice;

   /*__________________Copy data to local memory___________________*/
   for (int k = localSlice; k < localDepth; k += get_local_size(2))
   {
      int currentSlice = groupStartSlice + k;
      // step down rows
      for (int i = localRow; i < localHeight; i += get_local_size(1))
      {
         int currentRow = groupStartRow + i;

         // step across columns
         for (int j = localCol; j < localWidth; j += get_local_size(0))
         {
            int currentCol = groupStartCol + j;

            // perform copy to local memory
            if (currentRow < deviceHeight && currentCol < deviceWidth && 
                currentSlice < deviceDepth)
            {
              *(local_x + k * localWidth * localHeight + i * localWidth + j) = 
             *(x + currentSlice * deviceWidth * deviceHeight + 
                   currentRow * deviceWidth + currentCol);
            }
         }
      }
   }

   barrier(CLK_LOCAL_MEM_FENCE);
   int position = (globalSlice + stencilRadius) * deviceWidth * deviceHeight + 
                  (globalRow   + stencilRadius) * deviceWidth + 
                  (globalCol   + stencilRadius);
   /*_____________________ perform convolution_____________________*/
   if ((globalRow   < (deviceHeight - padding)) && 
       (globalCol   < (deviceWidth - subt - padding))  &&
       (globalSlice < (deviceDepth - padding)) &&
       (((globalSlice + stencilRadius) + (globalRow   + stencilRadius) + 
         (globalCol   + stencilRadius))%2 == 0)
      )
   {     
     // Calculate right hand-side
     
     T sum = b[position] * pow(h,2) / diagCoeff * omega;

     int stencilIdx = 0;
     int offset;
     
     for (int k = localSlice; k < localSlice + stencilWidth; ++k)
     {
       for (int i = localRow; i < localRow + stencilWidth; ++i)
       {
         offset = k * localWidth * localHeight + i * localWidth + localCol;

         sum += local_x[offset++] * stencil[stencilIdx++];
         sum += local_x[offset++] * stencil[stencilIdx++];
         sum += local_x[offset++] * stencil[stencilIdx++];
       }
     }
     
     *(y + position) = sum;
   }
}


/*
 * The kernel calculates the residual of r = b - Ax
 */
__kernel void residual(__global T* x, int deviceHeight, 
                       int deviceWidth, int deviceDepth,
		       __constant T* A, int stencilWidth,
                       __global T* b,
		       __global T* residual,
                       T h, int subt,
                       __local T* local_x, int localWidth, 
                       int localHeight, int localDepth)
{
   // padding for stencil
   int stencilRadius = (stencilWidth / 2);
   int padding = stencilRadius * 2;

   // size of work group in output
   int groupStartCol   = get_group_id(0) * get_local_size(0);
   int groupStartRow   = get_group_id(1) * get_local_size(1);
   int groupStartSlice = get_group_id(2) * get_local_size(2);

   // local ID for work-items
   int localCol   = get_local_id(0);
   int localRow   = get_local_id(1);
   int localSlice = get_local_id(2);

   // global ID of each work-item
   int globalCol   = groupStartCol   + localCol;
   int globalRow   = groupStartRow   + localRow;
   int globalSlice = groupStartSlice + localSlice;

   /*__________________Copy data to local memory___________________*/

   for (int k = localSlice; k < localDepth; k += get_local_size(2))
   {
      int currentSlice = groupStartSlice + k;
      // step down rows
      for (int i = localRow; i < localHeight; i += get_local_size(1))
      {
         int currentRow = groupStartRow + i;

         // step across columns
         for (int j = localCol; j < localWidth; j += get_local_size(0))
         {
           int currentCol = groupStartCol + j;

            // perform copy to local memory
            if (currentRow < deviceHeight && currentCol < deviceWidth && 
                currentSlice < deviceDepth)
            {
               *(local_x + k * localWidth * localHeight + i * localWidth + j) = 
               *(x + currentSlice * deviceWidth * deviceHeight + 
                     currentRow * deviceWidth + currentCol);
            }
         } 
      }  
   }

   barrier(CLK_LOCAL_MEM_FENCE);

   int position = (globalSlice + stencilRadius) * deviceWidth * deviceHeight + 
                  (globalRow   + stencilRadius) * deviceWidth + 
                  (globalCol   + stencilRadius);
   /*_____________________ perform convolution_____________________*/
   
   if ((globalRow   < (deviceHeight - padding)) && 
       (globalCol   < (deviceWidth  - subt - padding))  &&
       (globalSlice < (deviceDepth  - padding)))
   {
     // Calculate right hand-side
     T sum =  b[position];

     int stencilIdx = 0;
     int offset;

     for (int k = localSlice; k < localSlice + stencilWidth; ++k)
     {
       for (int i = localRow; i < localRow + stencilWidth; ++i)
       {
          offset = k * localWidth * localHeight + i * localWidth + localCol;

          sum -= local_x[offset++] * A[stencilIdx++];
          sum -= local_x[offset++] * A[stencilIdx++];
          sum -= local_x[offset++] * A[stencilIdx++];
       }
     }
     *(residual + position) = sum;
   }
}



/*
 * The kernel implements restriction of multigrid (going down to coarse grid)
 */
__kernel void restriction(__global T* x, int srcDeviceHeight, 
                          int srcDeviceWidth, int srcDeviceDepth,
                          __constant T* A, int stencilWidth,
                          __global T* y, int distDeviceWidth, int subt,
                          __local T* local_x, int localWidth, 
                          int localHeight, int localDepth)
{ 
   // padding for stencil
   int stencilRadius = (stencilWidth / 2);
   int padding = stencilRadius * 2;

   // size of work group in output
   int groupStartCol   = get_group_id(0) * get_local_size(0);
   int groupStartRow   = get_group_id(1) * get_local_size(1);
   int groupStartSlice = get_group_id(2) * get_local_size(2);

   // local ID for work-items
   int localCol   = get_local_id(0);
   int localRow   = get_local_id(1);
   int localSlice = get_local_id(2);

   // global ID of each work-item
   int globalCol   = groupStartCol   + localCol;
   int globalRow   = groupStartRow   + localRow;
   int globalSlice = groupStartSlice + localSlice;

   /*__________________Copy data to local memory___________________*/
   
   for (int k = localSlice; k < localDepth; k += get_local_size(2))
   {
      int currentSlice = groupStartSlice + k;
      // step down rows
      for (int i = localRow; i < localHeight; i += get_local_size(1))
      {
        int currentRow = groupStartRow + i;

        // step across columns
        for (int j = localCol; j < localWidth; j += get_local_size(0))
        {
           int currentCol = groupStartCol + j;

           // perform copy to local memory
           if (currentRow < srcDeviceHeight && currentCol < srcDeviceWidth && 
              currentSlice < srcDeviceDepth)
           {
            *(local_x + k * localWidth * localHeight + i * localWidth + j) = 
            *(x + currentSlice * srcDeviceWidth * srcDeviceHeight + 
                  currentRow * srcDeviceWidth + currentCol);
           }
        } 
     }  
   }
   
   barrier(CLK_LOCAL_MEM_FENCE);

   /*_____________________ perform convolution_____________________*/
   // perform convolution. The odd rows and columns are eliminated for y
   
   if ((globalRow   < (srcDeviceHeight - padding)) && 
       (globalCol   < (srcDeviceWidth - subt - padding))  && 
       (globalSlice < (srcDeviceDepth - padding))  && 
      ((globalRow   + stencilRadius)%2 == 0)       &&
       ((globalCol  + stencilRadius)%2 == 0)       && 
       ((globalSlice + stencilRadius)%2 == 0))
   {
     // Calculate right hand-side
     T sum = 0;

     int stencilIdx = 0;
     int offset;
     
     for (int k = localSlice; k < localSlice + stencilWidth; ++k)
     {
       for (int i = localRow; i < localRow + stencilWidth; ++i)
       {
         offset = k * localWidth * localHeight + i * localWidth + localCol;

         sum += local_x[offset++] * A[stencilIdx++];
         sum += local_x[offset++] * A[stencilIdx++];
         sum += local_x[offset++] * A[stencilIdx++];
       }
     }
     *(y +  (globalSlice + stencilRadius) * (distDeviceWidth) * (srcDeviceHeight + 1) / 4
         +  (globalRow   + stencilRadius) * (distDeviceWidth) / 2
         +  (globalCol   + stencilRadius) / 2) = sum;  
   }
}


/* /////////////////////////////////////////////////////////////////////////
//                                                                        //
//                           LINEAR INTERPOLATION                         //
//                                                                        //
//////////////////////////////////////////////////////////////////////////*/

/*
 * The kernel copies the data from coarse grid (x) to corresponding node in fine grid 
 * before performing prolongation (y)
 */
__kernel void  copyInLinearInterpolation (__global T* x, int srcDeviceHeight, 
                                          int srcDeviceWidth, int srcDeviceDepth,
                                          int stencilWidth, 
                                          __global T* y, int distDeviceWidth)
{
   int stencilRadius = (stencilWidth / 2);
   int padding = stencilRadius * 2;

   // size of work group in output
   int groupStartCol   = get_group_id(0) * get_local_size(0);
   int groupStartRow   = get_group_id(1) * get_local_size(1);
   int groupStartSlice = get_group_id(2) * get_local_size(2);

   // local ID for work-items
   int localCol   = get_local_id(0);
   int localRow   = get_local_id(1);
   int localSlice = get_local_id(2);

   // global ID of each work-item
   int globalCol   = groupStartCol   + localCol;
   int globalRow   = groupStartRow   + localRow;
   int globalSlice = groupStartSlice + localSlice;
   
   if ((globalRow   < (srcDeviceHeight - padding))  && 
       (globalCol   < (srcDeviceWidth - padding))     && 
       (globalSlice < (srcDeviceDepth - padding)))
   {
 
       *(y + ((globalSlice + stencilRadius) * (distDeviceWidth) * 
                                              (2 * srcDeviceHeight - 1)  * 2
           +  (globalRow   + stencilRadius) * (distDeviceWidth) * 2 + 
              (globalCol   + stencilRadius) * 2)) = 
       *(x +  (globalSlice + stencilRadius) * srcDeviceWidth * srcDeviceHeight + 
              (globalRow   + stencilRadius) * srcDeviceWidth + 
               globalCol   + stencilRadius);
   }
}

/*
 * The kernel implements linear interpolation of multigrid (going up to fine grid)
 * before performing prolongation, copy kernel should be done to copy 
 */
__kernel void linearInterpolation (__global T* x, int deviceHeight, 
                                   int deviceWidth, int deviceDepth,
                                   __constant T* A, int stencilWidth,
                                   __global T* y, int subt,
                                   __local T* local_x, int localWidth, 
                                   int localHeight, int localDepth)
{ 
   // padding for stencil
   int stencilRadius = (stencilWidth / 2);
   int padding = stencilRadius * 2;

   // size of work group in output
   int groupStartCol   = get_group_id(0) * get_local_size(0);
   int groupStartRow   = get_group_id(1) * get_local_size(1);
   int groupStartSlice = get_group_id(2) * get_local_size(2);

   // local ID for work-items
   int localCol   = get_local_id(0);
   int localRow   = get_local_id(1);
   int localSlice = get_local_id(2);

   // global ID of each work-item
   int globalCol   = groupStartCol   + localCol;
   int globalRow   = groupStartRow   + localRow;
   int globalSlice = groupStartSlice + localSlice; 

   /*__________________Copy data to local memory___________________*/
   
   for (int k = localSlice; k < localDepth; k += get_local_size(2))
   {
      int currentSlice = groupStartSlice + k;
      // step down rows
      for (int i = localRow; i < localHeight; i += get_local_size(1))
      {
        int currentRow = groupStartRow + i;

        // step across columns
        for (int j = localCol; j < localWidth; j += get_local_size(0))
        {
           int currentCol = groupStartCol + j;

           // perform copy to local memory
           if (currentRow < deviceHeight && currentCol < deviceWidth && 
               currentSlice < deviceDepth)
           {
            *(local_x + k * localWidth * localHeight + i * localWidth + j) = 
            *(x + currentSlice * deviceWidth * deviceHeight + 
                  currentRow * deviceWidth + currentCol);
           }
         } 
      }  
   }
 
    barrier(CLK_LOCAL_MEM_FENCE);

   /*_____________________ perform convolution_____________________*/
   
   if ((globalRow   < (deviceHeight - padding))  && 
       (globalCol   < (deviceWidth - subt - padding))   && 
       (globalSlice < (deviceDepth - padding)))
   {
     // Calculate right hand-side
     T sum = 0.0;

     int stencilIdx = 0;
     int offset;

     for (int k = localSlice; k < localSlice + stencilWidth; ++k)
     {
       for (int i = localRow; i < localRow + stencilWidth; ++i)
       {
         offset = k * localWidth * localHeight + i * localWidth + localCol;

         sum += local_x[offset++] * A[stencilIdx++];
         sum += local_x[offset++] * A[stencilIdx++];
         sum += local_x[offset++] * A[stencilIdx++];
       }
     }
     *(y + (globalSlice + stencilRadius) * deviceWidth * deviceHeight + 
           (globalRow   + stencilRadius) * deviceWidth + 
           (globalCol   + stencilRadius)) += sum;
   }
}


/*
 * The kernel corrects boundary values results from copying data from 2h -> h
 * by kernel copy and then vector x used for implementing prolongation
 */

__kernel void  correctionBoundary (__global T* x, 
                                   int deviceHeight, int deviceWidth, int deviceDepth, 
                                   int subt)
{
   int stencilWidth = 3;
   int stencilRadius = (stencilWidth / 2);
   int padding = stencilRadius * 2;

   // size of work group in output
   int groupStartCol   = get_group_id(0) * get_local_size(0);
   int groupStartRow   = get_group_id(1) * get_local_size(1);
   int groupStartSlice = get_group_id(2) * get_local_size(2);

   // local ID for work-items
   int localCol   = get_local_id(0);
   int localRow   = get_local_id(1);
   int localSlice = get_local_id(2);

   // global ID of each work-item
   int globalCol   = groupStartCol   + localCol;
   int globalRow   = groupStartRow   + localRow;
   int globalSlice = groupStartSlice + localSlice;

   if (globalCol   == (deviceWidth - subt -  padding))
   {
       *(x + ((globalSlice + stencilRadius) * deviceWidth * deviceHeight
           +  (globalRow   + stencilRadius) * deviceWidth
           +  (globalCol   + stencilRadius) )) = 0.0;
   }
}


/* /////////////////////////////////////////////////////////////////////////
//                                                                        //
//                            CUBIC INTERPOLATION                         //
//                                                                        //
//////////////////////////////////////////////////////////////////////////*/

/*
 * The kernel implements linear interpolation of multigrid (going up to fine grid)
 * before performing prolongation, copy kernel should be done to copy 
 */
__kernel void cubicInterpolation (__global T* x, int deviceHeight, 
                                  int deviceWidth, int deviceDepth,
                                  __constant T* A, int cubicStencilWidth,
                                  __global T* y, int subt,
                                  __local T* local_x, int localWidth, 
                                  int localHeight, int localDepth)
{ 
   // padding for stencil
   int cubicStencilRadius = (cubicStencilWidth / 2);
   int cubicPadding = cubicStencilRadius * 2;

   int linearStencilWidth = 3;
   int linearStencilRadius = (linearStencilWidth / 2);
   int linearPadding = linearStencilRadius * 2;
  
   // difference between cubic stencil with linear stencil width
   int diffCubicLinearWidth = cubicStencilWidth - linearStencilWidth;
  
   // size of work group in output
   int groupStartCol   = get_group_id(0) * get_local_size(0);
   int groupStartRow   = get_group_id(1) * get_local_size(1);
   int groupStartSlice = get_group_id(2) * get_local_size(2);

   // local ID for work-items
   int localCol   = get_local_id(0);
   int localRow   = get_local_id(1);
   int localSlice = get_local_id(2);

   // global ID of each work-item
   int globalCol   = groupStartCol   + localCol;
   int globalRow   = groupStartRow   + localRow;
   int globalSlice = groupStartSlice + localSlice; 

   /*__________________Copy data to local memory___________________*/
   // step across slices
   for (int k = localSlice; k < localDepth; k += get_local_size(2))
   {
      int currentSlice = groupStartSlice + k;
      // step down rows
      for (int i = localRow; i < localHeight; i += get_local_size(1))
      {
         int currentRow = groupStartRow + i;

         // step across columns
         for (int j = localCol; j < localWidth; j += get_local_size(0))
         {
             int currentCol = groupStartCol + j;

             // perform copy to local memory
             if (currentRow   < (deviceHeight + diffCubicLinearWidth)  && 
                 currentCol   < (deviceWidth  + diffCubicLinearWidth)  && 
                 currentSlice < (deviceDepth  + diffCubicLinearWidth))
             {
                // +diffcubicLinearWidth in each dim of x referes to ghost cells around 
                // original buffer (on the boundaries) to impelement cubic interpolation 
                *(local_x + k * localWidth * localHeight + i * localWidth + j) = 
                *(x + currentSlice * (deviceWidth  + diffCubicLinearWidth) * 
                                     (deviceHeight + diffCubicLinearWidth) + 
                      currentRow   * (deviceWidth  + diffCubicLinearWidth) + 
                      currentCol);
             }
         }  
      }
   }

   barrier(CLK_LOCAL_MEM_FENCE);

   /*_____________________ perform convolution_____________________*/
   
   if ((globalRow   < (deviceHeight - linearPadding))         && 
       (globalCol   < (deviceWidth - subt - linearPadding))   && 
       (globalSlice < (deviceDepth - linearPadding)))
   {
      if (((globalRow + linearStencilRadius) % 2 != 0 ||
           (globalCol + linearStencilRadius) % 2 != 0 ||
           (globalSlice + linearStencilRadius) % 2 != 0) &&
           !((globalRow + linearStencilRadius) % 2 == 0 &&
                 (globalCol + linearStencilRadius) % 2 == 0 &&
                 (globalSlice + linearStencilRadius) % 2 == 0))

      {
         // Calculate right hand-side
         T sum = 0.0;

         int stencilIdx = 0;
         int offset;

         for (int k = localSlice; k < localSlice + cubicStencilWidth; ++k)
         {
            for (int i = localRow; i < localRow + cubicStencilWidth; ++i)
            {
               offset = k * localWidth * localHeight + i * localWidth + localCol;

               sum += local_x[offset++] * A[stencilIdx++];
               sum += local_x[offset++] * A[stencilIdx++];
               sum += local_x[offset++] * A[stencilIdx++];
               sum += local_x[offset++] * A[stencilIdx++];
               sum += local_x[offset++] * A[stencilIdx++];
               sum += local_x[offset++] * A[stencilIdx++];
               sum += local_x[offset++] * A[stencilIdx++];
           }
         }

         *(y + (globalSlice + linearStencilRadius) * deviceWidth * deviceHeight + 
               (globalRow   + linearStencilRadius) * deviceWidth + 
               (globalCol   + linearStencilRadius)) += sum;
      } else if ((globalRow + linearStencilRadius) % 2 == 0 &&
                 (globalCol + linearStencilRadius) % 2 == 0 &&
                 (globalSlice + linearStencilRadius) % 2 == 0)
      {
           *(y + (globalSlice + linearStencilRadius) * deviceWidth * deviceHeight + 
                 (globalRow   + linearStencilRadius) * deviceWidth + 
                 (globalCol   + linearStencilRadius)) += 
                  local_x[(localSlice + cubicStencilRadius) * localWidth * localHeight + 
                  (localRow + cubicStencilRadius) * localWidth + 
                   localCol + cubicStencilRadius];
      }
   }
}



/*
 * The kernel copies the data from coarse grid (x) to corresponding node in fine grid 
 * with  ghost cells around before performing cubic interpolation (y)
 */
__kernel void  copyInCubicInterpolation (__global T* x, int srcDeviceHeight, 
                                         int srcDeviceWidth, int srcDeviceDepth,
                                         int stencilWidth, 
                                         __global T* y, int distDeviceWidth)
{
   int stencilRadius = (stencilWidth / 2);
   int padding = stencilRadius * 2;

   // size of work group in output
   int groupStartCol   = get_group_id(0) * get_local_size(0);
   int groupStartRow   = get_group_id(1) * get_local_size(1);
   int groupStartSlice = get_group_id(2) * get_local_size(2);

   // local ID for work-items
   int localCol   = get_local_id(0);
   int localRow   = get_local_id(1);
   int localSlice = get_local_id(2);

   // global ID of each work-item
   int globalCol   = groupStartCol   + localCol;
   int globalRow   = groupStartRow   + localRow;
   int globalSlice = groupStartSlice + localSlice;
   
   if ((globalRow   < (srcDeviceHeight - padding))  && 
       (globalCol   < (srcDeviceWidth - padding))     && 
       (globalSlice < (srcDeviceDepth - padding)))
   {
 
       *(y + ((globalSlice + stencilRadius + 2) * (distDeviceWidth) * 
                                              (2 * srcDeviceHeight - 1)  * 2
           +  (globalRow   + stencilRadius + 2) * (distDeviceWidth) * 2 + 
              (globalCol   + stencilRadius + 2) * 2)) = 
       *(x +  (globalSlice + stencilRadius) * srcDeviceWidth * srcDeviceHeight + 
              (globalRow   + stencilRadius) * srcDeviceWidth + 
               globalCol   + stencilRadius);
   }
}



/*
 * The kernel corrects boundary values results from copying data from 2h -> h
 * by kernel copy and then vector x used for implementing prolongation
 */

__kernel void  setGhostCellBC (__global T* x, 
                               int deviceHeight, int deviceWidth, int deviceDepth,
                               int distSubt)
{
   // size of work group in output
   int groupStartCol   = get_group_id(0) * get_local_size(0);
   int groupStartRow   = get_group_id(1) * get_local_size(1);
   int groupStartSlice = get_group_id(2) * get_local_size(2);

   // local ID for work-items
   int localCol   = get_local_id(0);
   int localRow   = get_local_id(1);
   int localSlice = get_local_id(2);

   // global ID of each work-item
   int globalCol   = groupStartCol   + localCol;
   int globalRow   = groupStartRow   + localRow;
   int globalSlice = groupStartSlice + localSlice;

   // we only loop in actual domain not extended area
   if ((globalCol == 0) &&
       ((globalRow   > 1 && globalRow   < deviceHeight - 2) && 
        (globalSlice > 1 && globalSlice < deviceDepth  - 2)))
   {  
       *(x + globalSlice * deviceWidth * deviceHeight
           + globalRow   * deviceWidth + globalCol) =  
       *(x + globalSlice * deviceWidth * deviceHeight + globalRow * deviceWidth 
           + globalCol + 6 )  
       - 3.0 * *(x + globalSlice * deviceWidth * deviceHeight + globalRow * deviceWidth 
                 + globalCol + 4)
       + 3.0 * *(x + globalSlice * deviceWidth * deviceHeight + globalRow * deviceWidth 
                 + globalCol + 2);

   } else if ((globalCol == deviceWidth - distSubt - 1) && 
              ((globalRow   > 1 && globalRow   < deviceHeight - 2) && 
               (globalSlice > 1 && globalSlice < deviceDepth  - 2)))
   {
       *(x + globalSlice * deviceWidth * deviceHeight
           + globalRow * deviceWidth + globalCol) = 
       3.0 * *(x + globalSlice * deviceWidth * deviceHeight + globalRow * deviceWidth 
                 + globalCol - 2)
       - 3.0 * *(x + globalSlice * deviceWidth * deviceHeight + globalRow * deviceWidth 
                 + globalCol - 4)
       + *(x + globalSlice * deviceWidth * deviceHeight + globalRow * deviceWidth 
           + globalCol - 6);

   } else if ((globalRow == 0) &&
              ((globalCol   > 1 && globalCol   < deviceWidth - distSubt - 2) && 
               (globalSlice > 1 && globalSlice < deviceDepth - 2)))
   {
       *(x + globalSlice  * deviceWidth * deviceHeight
           + globalRow    * deviceWidth + globalCol) =  
       *(x + globalSlice * deviceWidth * deviceHeight + (globalRow + 6) * deviceWidth 
           + globalCol )
       - 3.0 * *(x + globalSlice * deviceWidth * deviceHeight + 
             (globalRow + 4) * deviceWidth + globalCol) 
       + 3.0 * *(x + globalSlice * deviceWidth * deviceHeight + 
             (globalRow + 2) * deviceWidth + globalCol);

   } else if ((globalRow == deviceHeight - 1) && 
              ((globalCol   > 1 && globalCol   < deviceWidth - distSubt - 2) && 
               (globalSlice > 1 && globalSlice < deviceDepth - 2)))
   {
       *(x + globalSlice * deviceWidth * deviceHeight
           + globalRow   * deviceWidth + globalCol) =  
       3.0 * *(x + globalSlice * deviceWidth * deviceHeight + 
             (globalRow - 2) * deviceWidth + globalCol) 
       - 3.0 * *(x + globalSlice * deviceWidth * deviceHeight + 
             (globalRow - 4) * deviceWidth + globalCol)
       + *(x + globalSlice * deviceWidth * deviceHeight + 
             (globalRow - 6) * deviceWidth + globalCol);

   } else if ((globalSlice == 0) &&
              ((globalCol > 1 && globalCol < deviceWidth - distSubt - 2) &&
               (globalRow > 1 && globalRow < deviceHeight - 2)))
   {
       *(x + globalSlice * deviceWidth * deviceHeight
           +  globalRow * deviceWidth  + globalCol) =  
       *(x + (globalSlice + 6) * deviceWidth * deviceHeight + globalRow * deviceWidth 
           + globalCol)
       - 3.0 * *(x + (globalSlice + 4) * deviceWidth * deviceHeight + 
             globalRow * deviceWidth + globalCol)
       + 3.0 * *(x + (globalSlice + 2) * deviceWidth * deviceHeight + 
             globalRow * deviceWidth + globalCol);

   } else if ((globalSlice == deviceDepth - 1) &&
              ((globalCol > 1 && globalCol < deviceWidth - distSubt - 2) &&
               (globalRow > 1 && globalRow < deviceHeight - 2)))
   {
       *(x + globalSlice * deviceWidth * deviceHeight
           + globalRow  * deviceWidth  + globalCol) =  
       3.0 * *(x + (globalSlice - 2) * deviceWidth * deviceHeight + 
             globalRow * deviceWidth + globalCol) 
       - 3.0 * *(x + (globalSlice - 4) * deviceWidth * deviceHeight + 
             globalRow * deviceWidth + globalCol)
       + *(x + (globalSlice - 6) * deviceWidth * deviceHeight + 
             globalRow * deviceWidth + globalCol);
   }
}
