
/*-----------------------------------------------------------------------*\
           ___________________________________________________
          /                                                  /|
         /                                                  / |
        |--------------------------------------------------|  |
        |    ______ __     __  ______    _____  __   __    |  |
        |   |   ___|  \   /  |/   ___|  |  _  \|  | |  |   |  |
        |   |  |_  |   \_/   |   /      | |_)  |  | |  |   |  |
        |   |    | |         |  |   ____|   __/|  | |  |   |  |
        |   |   _| |   ___   |  |  |_  _|  |   |  |_|  |   |  |
        |   |  |   |  |   |  |   \__/ / |  |   |       |   |  |
        |   |__|   |__|   |__|\______/  |__|    \_____/    |  |
        |                                                  |  |
        |      Full      Multigrid      On     GPGPU       | /
        |__________________________________________________|/

--------------------------------------------------------------------------

Author

    Mehdi Baba Mehdi
    mehdi.baba_mehdi@uni-wuppertal.de

Date  
    Fri 10.04.2020
\*-----------------------------------------------------------------------*/

#ifndef PROLONGATION_HPP
#define PROLONGATION_HPP

#define __CL_ENABLE_EXCEPTIONS

#ifdef LOCAL_CL
#include "CL/cl.hpp"
#else
#include <CL/cl.hpp>
#endif

#include <iostream>
#include <iomanip>
#include "CL_ERROR.hpp"
#include "defines.hpp"
#include <chrono>
#include <cmath>

template<class T>
class prolongation
{
  protected:
    int linearStencilWidth = 3;
    int linearStencilSize = pow(linearStencilWidth,3);

    // width of cubic stencil is 5 in 2h grid, but 7 in h grid, since the strategy 
    // in implementing prolongation is first copying data from 2h to h grid and then 
    // applying interpolation stencil
    int cubicStencilWidth = 7;
    int cubicStencilSize = pow(cubicStencilWidth,3);

    cl::Kernel       copyLinearInterKernel;
    cl::Kernel       linearKernel;
    cl::Kernel       cubicKernel;
    cl::Kernel       correctionBoundaryKernel;
    cl::Kernel       copyCubicInterKernel;
    cl::Kernel       setGhostCellBCKernel;
    cl::Buffer       linearStencilBuffer;
    cl::Buffer       cubicStencilBuffer;

    long double timeOfOperation = 0.0;

  public:
     prolongation(cl::Program& _program, cl::CommandQueue& _queue,
                  const cl::Context& _context);

    ~prolongation();

     const long double getTime() const;

     double bernsteinWeight(double t);

     double discreteWeight(double t);

     void cubicInterStencilGen(T* stencil);

     int roundUp(unsigned int value, unsigned int multiple);

     void setStencil(const cl::Context& context, cl::CommandQueue& queue);

     void createKernel(cl::Program& program);

     void linearInterpolation(cl::CommandQueue& queue, 
          cl::Buffer& inputBuffer, cl::Buffer& outputBuffer,
          cl::Buffer& intermediateBuffer,
          const cl::NDRange& srcGlobalRange, const cl::NDRange& srcLocalRange, 
          const cl::NDRange& distGlobalRange, const cl::NDRange& distLocalRange, 
          const int& srcDeviceHeight, const int& srcDeviceWidth, 
          const int& srcDeviceDepth, 
          const int& distDeviceHeight, const int& distDeviceWidth, 
          const int& distDeviceDepth, const std::vector<int> distLocalSize, 
          const int& distSubt, cl::Event& event);

   void cubicInterpolation(cl::Context& context, cl::CommandQueue& queue,  
          cl::Buffer& inputBuffer, cl::Buffer& outputBuffer,
          cl::Buffer& intermediateBuffer,
          const cl::NDRange& srcGlobalRange, const cl::NDRange& srcLocalRange, 
          const cl::NDRange& distGlobalRange, const cl::NDRange& distLocalRange, 
          const int& srcDeviceHeight, const int& srcDeviceWidth, 
          const int& srcDeviceDepth, 
          const int& distDeviceHeight, const int& distDeviceWidth, 
          const int& distDeviceDepth, const std::vector<int> distLocalSize, 
          const int& distSubt, cl::Event& event);

};

//#include "../src/prolongation.cc"

#endif
