/*-----------------------------------------------------------------------*\
           ___________________________________________________
          /                                                  /|
         /                                                  / |
        |--------------------------------------------------|  |
        |    ______ __     __  ______    _____  __   __    |  |
        |   |   ___|  \   /  |/   ___|  |  _  \|  | |  |   |  |
        |   |  |_  |   \_/   |   /      | |_)  |  | |  |   |  |
        |   |    | |         |  |   ____|   __/|  | |  |   |  |
        |   |   _| |   ___   |  |  |_  _|  |   |  |_|  |   |  |
        |   |  |   |  |   |  |   \__/ / |  |   |       |   |  |
        |   |__|   |__|   |__|\______/  |__|    \_____/    |  |
        |                                                  |  |
        |      Full      Multigrid      On     GPGPU       | /
        |__________________________________________________|/

--------------------------------------------------------------------------

Author

    Mehdi Baba Mehdi
    mehdi.baba_mehdi@uni-wuppertal.de

Date  
    Sat 11.04.2020
\*-----------------------------------------------------------------------*/

#ifndef RELAXATION_HPP
#define RELAXATION_HPP

#define __CL_ENABLE_EXCEPTIONS

#ifdef LOCAL_CL
#include "CL/cl.hpp"
#else
#include <CL/cl.hpp>
#endif

#include <iostream>
#include <iomanip>
#include <cmath>
#include <string>
#include "CL_ERROR.hpp"
#include "defines.hpp"
#include <chrono>

template<class T>
class relaxation
{
  protected:
    int stencilDataWidth = 3;
    int relaxationStencilSize = pow(stencilDataWidth,3);
    T* stencil   = NULL;
    std::string relaxationMethod;
    T omega = 1.0;  

    long double timeOfOperation = 0.0;

    cl::Buffer       relaxationStencilBuffer;
    cl::Kernel       kernelJacobi;
    cl::Kernel       kernelGSRed;
    cl::Kernel       kernelGSBlack;
    cl::Kernel       kernelDirectSolver;

  public:
     relaxation(cl::Program& _programInput, cl::CommandQueue& _queue, 
                const cl::Context& _context, std::string relaxationMethod_, T omega);

    ~relaxation();

     const T getOmega() const;
    
     const long double getTime() const;
  
     void createKernel(cl::Program& program);

     void setStencil(const cl::Context& context, cl::CommandQueue& queue);

     void relaxing(cl::CommandQueue& queue, 
                   cl::Buffer& inoutBuffer, cl::Buffer& RHSBuffer, 
                   cl::Buffer& intermediateBUffer,
                   const cl::NDRange& globalRange, const cl::NDRange& localRange,
                   const int numberOfRelaxationSweep,
                   const int& deviceHeight, const int& deviceWidth,
                   const int& deviceDepth, const std::vector<int> localSize, 
                   cl::size_t<3> bufferOrigin, cl::size_t<3> hostOrigin, 
                   cl::size_t<3> region,
                   const T& spatialStepSize, const int& subt, 
                   cl::Event& event);


   void jacobi(cl::CommandQueue& queue, 
                   cl::Buffer& inoutBuffer, cl::Buffer& RHSBuffer, 
                   cl::Buffer& intermediateBUffer,
                   const cl::NDRange& globalRange, const cl::NDRange& localRange,
                   const int numberOfRelaxationSweep,
                   const int& deviceHeight, const int& deviceWidth,
                   const int& deviceDepth, const std::vector<int> localSize,
                   cl::size_t<3> bufferOrigin, cl::size_t<3> hostOrigin, 
                   cl::size_t<3> region,
                   const T& spatialStepSize, const int& subt, 
                   cl::Event& event);

   void Guass_Seidel(cl::CommandQueue& queue, 
                   cl::Buffer& inoutBuffer, cl::Buffer& RHSBuffer, 
                   cl::Buffer& intermediateBUffer,
                   const cl::NDRange& globalRange, const cl::NDRange& localRange,
                   const int numberOfRelaxationSweep,
                   const int& deviceHeight, const int& deviceWidth,
                   const int& deviceDepth, const std::vector<int> localSize,
                   cl::size_t<3> bufferOrigin, cl::size_t<3> hostOrigin, 
                   cl::size_t<3> region,
                   const T& spatialStepSize, const int& subt, 
                   cl::Event& event);

    void directSolver(cl::CommandQueue& queue,
                         cl::Buffer& inoutBuffer, cl::Buffer& RHSBuffer,
                         cl::Buffer& intermediateBuffer,
                         const cl::NDRange& globalRange, const cl::NDRange& localRange,
                         const int& deviceHeight, const int& deviceWidth,
                         const int& deviceDepth,const std::vector<int> localSize, 
                         cl::size_t<3> bufferOrigin, cl::size_t<3> hostOrigin, 
                         cl::size_t<3> region, 
                         const T& spatialStepSize, const int& subt, 
                         cl::Event& event);


};

//#include "../src/relaxation.cc"

#endif
