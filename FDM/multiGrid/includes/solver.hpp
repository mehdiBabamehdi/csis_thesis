
/*-----------------------------------------------------------------------*\
           ___________________________________________________
          /                                                  /|
         /                                                  / |
        |--------------------------------------------------|  |
        |    ______ __     __  ______    _____  __   __    |  |
        |   |   ___|  \   /  |/   ___|  |  _  \|  | |  |   |  |
        |   |  |_  |   \_/   |   /      | |_)  |  | |  |   |  |
        |   |    | |         |  |   ____|   __/|  | |  |   |  |
        |   |   _| |   ___   |  |  |_  _|  |   |  |_|  |   |  |
        |   |  |   |  |   |  |   \__/ / |  |   |       |   |  |
        |   |__|   |__|   |__|\______/  |__|    \_____/    |  |
        |                                                  |  |
        |      Full      Multigrid      On     GPGPU       | /
        |__________________________________________________|/

--------------------------------------------------------------------------

Author

    Mehdi Baba Mehdi
    mehdi.baba_mehdi@uni-wuppertal.de

Date  
    Wed 01.04.2020
\*---------------------------------------------------------------------------*/

#ifndef SOLVER_HPP
#define SOLVER_HPP

#define __CL_ENABLE_EXCEPTIONS

#ifdef LOCAL_CL
#include "CL/cl.hpp"
#else
#include <CL/cl.hpp>
#endif

#include <iostream>
#include <fstream>
#include <chrono>
#include <ctime>
#include <cmath>
#include "defines.hpp"
#include "rhsFunctionDict.hpp"
#include "boundaryCondition.hpp"
#include "gnuplot.hpp"

template <class T>
class solver;

template <class T>
class boundaryCondition;


template <class T>
std::ofstream& operator<<(std::ofstream& ofs, const solver<T>& result);

template <class T>
std::iostream& operator<<(std::iostream& os, const solver<T>& result);

template <class T>
class solver
{
  protected:
    T* RHS = NULL;  // b in Ax = b
    T* x = NULL;    // x in Ax = b
    std::ifstream RHSFileName;
    T initialValue = 0.0;    
    T spatialLength = 1.0;
    T origSpatialStepSize = 0.0;
    
    // Number of spatial steps in X, Y and Z directions
    int origWidth = 0;
    int origHeight = 0;
    int origDepth = 0;
   
    double tol = 1.0e-6;
    
    friend class boundaryCondition<T>;
    boundaryCondition<T>* bcObject;
    
  public:
    solver();

    solver(const T* _RHS,
           const int _origHeight, const int _origWidth, const int _origDepth);

    solver(std::ifstream _RHSFileName,
           const int _origHeight, const int _origWidth, const int _origDepth);

    solver(const int _origHeight, const int _origWidth, const int _origDepth);

    solver(const solver<T>& _solver);

    virtual ~solver();

    int getOrigWidth() const;

    int getOrigHeight() const;

    int getOrigDepth() const;

    T getOrigSpatialStepSize() const;

    void printLabel();

    void readDataFromFile();

    virtual void solve();

    T u(T x, T y, T z);

    void calcX_exact(T* x_exact);

    void calcError(T* x_exact, T* x_app, T* error);

    double norm_2(const T* vec);

    double norm_L2(const T* vec, const int& domainSize, const T& spatialStepSize);

    T norm_inf(const T* vec);

    void setInitialValues(T initValue);

    void setRightHandSide();

    void setBoundaryCondition();

    friend std::ofstream& operator<< <>(std::ofstream& ofs, const solver<T>& result);

    friend std::iostream& operator<< <>(std::iostream& os, const solver<T>& result);
};


#endif
