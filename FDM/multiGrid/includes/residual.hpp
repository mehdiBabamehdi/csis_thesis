/*-----------------------------------------------------------------------*\
           ___________________________________________________
          /                                                  /|
         /                                                  / |
        |--------------------------------------------------|  |
        |    ______ __     __  ______    _____  __   __    |  |
        |   |   ___|  \   /  |/   ___|  |  _  \|  | |  |   |  |
        |   |  |_  |   \_/   |   /      | |_)  |  | |  |   |  |
        |   |    | |         |  |   ____|   __/|  | |  |   |  |
        |   |   _| |   ___   |  |  |_  _|  |   |  |_|  |   |  |
        |   |  |   |  |   |  |   \__/ / |  |   |       |   |  |
        |   |__|   |__|   |__|\______/  |__|    \_____/    |  |
        |                                                  |  |
        |      Full      Multigrid      On     GPGPU       | /
        |__________________________________________________|/

--------------------------------------------------------------------------

Author

    Mehdi Baba Mehdi
    mehdi.baba_mehdi@uni-wuppertal.de

Date  
    Sat 11.04.2020
\*-----------------------------------------------------------------------*/

#ifndef RESIDUAL_HPP
#define RESIDUAL_HPP

#define __CL_ENABLE_EXCEPTIONS

#ifdef LOCAL_CL
#include "CL/cl.hpp"
#else
#include <CL/cl.hpp>
#endif

#include <iostream>
#include <iomanip>
#include "CL_ERROR.hpp"
#include "defines.hpp"
#include <chrono>

template<class T>
class residual
{
  protected:
    int stencilDataSize = 27;
    int stencilDataWidth = 3;
    cl::Kernel       kernel;

    long double timeOfOperation = 0.0;

  public:
     residual(cl::Program& _program);

    ~residual();

     const long double getTime() const;

     void createKernel(cl::Program& program);

     void calculateResidual(cl::CommandQueue& queue, 
                          const cl::Buffer& xBuffer, const cl::Buffer& RHSBuffer,
                          cl::Buffer& residualBuffer, const cl::Buffer& matrixBuffer, 
                          const cl::NDRange& globalRange, const cl::NDRange& localRange,
                          const int& deviceHeight, const int& deviceWidth,
                          const int& deviceDepth, const std::vector<int> localSize,
                          const T& spatialStepSize, const int& subt, cl::Event& event);
};


#endif

