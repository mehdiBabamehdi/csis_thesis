/*-----------------------------------------------------------------------*\
           ___________________________________________________
          /                                                  /|
         /                                                  / |
        |--------------------------------------------------|  |
        |    ______ __     __  ______    _____  __   __    |  |
        |   |   ___|  \   /  |/   ___|  |  _  \|  | |  |   |  |
        |   |  |_  |   \_/   |   /      | |_|  |  | |  |   |  |
        |   |    | |         |  |   ____|   __/|  | |  |   |  |
        |   |   _| |   ___   |  |  |_  _|  |   |  |_|  |   |  |
        |   |  |   |  |   |  |   \__/ / |  |   |       |   |  |
        |   |__|   |__|   |__|\______/  |__|    \_____/    |  |
        |                                                  |  |
        |      Full      Multigrid      On     GPGPU       | /
        |__________________________________________________|/

--------------------------------------------------------------------------

Author

    Mehdi Baba Mehdi
    mehdi.baba_mehdi@uni-wuppertal.de

Date  
    Wed 03.05.2020
\*-----------------------------------------------------------------------*/

#ifndef MULTIGRID_HPP
#define MULTIGRID_HPP

#define __CL_ENABLE_EXCEPTIONS

#ifdef LOCAL_CL
#include "CL/cl.hpp"
#else
#include <CL/cl.hpp>
#endif

//#define DEBUG

#include <iostream>
#include <cassert>
#include <fstream>
#include <vector>
#include <string>
#include <cstring>
#include <cmath>
#include <iomanip>
#include <chrono>
#include <cstdlib>
#include <cxxabi.h>
#include <typeinfo>

#include "CL_ERROR.hpp"
#include "defines.hpp"
#include "solver.hpp"
#include "residual.hpp"
#include "relaxation.hpp"
#include "prolongation.hpp"
#include "restriction.hpp"
#include "gnuplot.hpp"


template<class T>
class restriction;

template<class T>
class gnuplot;

template<class T>
class relaxation;

template<class T>
class residual;

template<class T>
class prolonation;

template<class T>
class boundaryCondition;

template<class T>
class solver;

template<class T>
class multiGrid : public solver<T>
{
  protected:
    int deviceID = 0;
    int multiGridLevels = 0;
    T* residual_h = NULL;

    std::string kernelFileName = "./kernels/multigrid_nConst.cl";
    cl_device_type deviceType = CL_DEVICE_TYPE_GPU;

    std::string relaxationMethod = "GS";
    T omega = 1.15;
 
    std::vector<cl::Platform> platforms;
    cl::Platform myPlatform;
    std::vector<cl::Device> devices;

    cl::Device device;
    cl::CommandQueue queue;
    cl::Context context;
    cl::Program program;

    std::vector<int> domainWidth;
    std::vector<int> domainHeight;
    std::vector<int> domainDepth;
    std::vector<T> spatialStepSize;

    cl::Buffer xBuffer;
    cl::Buffer relaxedXBuffer;
    cl::Buffer bBuffer;
    
    std::vector<cl::Buffer> matrixBuffer;            // 7-point stencil
    std::vector<cl::Buffer> residualBuffer;          // r = b - Ax 
    std::vector<cl::Buffer> interResidualBuffer;     // r_2h = I_h^2h(r_h) 
    std::vector<cl::Buffer> intermediateBuffer;      // use for intermediate steps
    std::vector<cl::Buffer> errorBuffer;             // e = |x* - x|
    
    cl::Event event;
    
    std::vector<int> deviceWidth;
    std::vector<int> deviceHeight;
    std::vector<int> deviceDepth;
    std::vector<int> deviceDataSize;

    std::vector<int> subt;         // deviceWidth - domainWidth
    
    cl::size_t<3> bufferOrigin;
    cl::size_t<3> hostOrigin;
    std::vector<cl::size_t<3>> region;

    std::vector<cl::NDRange> globalRange;
    std::vector<cl::NDRange> restProlGlobalRange;
    std::vector<cl::NDRange> localRange;
    std::vector<std::vector<int>> deviceLocalSize;
 
    residual<T>* mgResidual;
    relaxation<T>* mgRelaxation;
    restriction<T>* mgRestriction;
    prolongation<T>* mgProlongation;
    gnuplot<T>* mgGnuplot;
    
  public:
    multiGrid(const T* _RHS,
           const int _origHeight, const int _origWidth, const int _origDepth,
           std::string _relaxationMethod, T _omega,   
           cl_device_type _deviceType, int _deviceID);

    multiGrid(std::ifstream _RHSFileName,
           const int _origHeight, const int _origWidth, const int _origDepth, 
           std::string _relaxationMethod, T _omega,   
           cl_device_type _deviceType, int _deviceID);
 
    multiGrid(const int _origHeight, const int _origWidth,const int _origDepth,
           std::string _relaxationMethod, T _omega,   
           cl_device_type _deviceType, int _deviceID);

    multiGrid(std::string _relaxationMethod, T _omega, 
              cl_device_type _deviceType, int _deviceID);
    
    multiGrid(const solver<T>& _mgSolver, 
           std::string _relaxationMethod, T _omega,   
           cl_device_type _deviceType, int _deviceID);

    ~multiGrid();

    void setDevice();

    void displayDeviceInfo();    

    int roundUp (unsigned int value, unsigned int multiple);    

    void setNumberOfMGLevels();


    void buildProgram(T omega);

    void setDomainSize();

    void createBuffers();

    void setNDRange();

    void writeDataToDevice();

    void createMatrixA(T* stencil, const int level);

    virtual void solve() override;

    void copyBuffer(cl::Buffer& srcBuffer, cl::Buffer& distBuffer, int level);

    void readDataFromDevice();

    double printData(cl::Buffer& buffer, int stepNumber, bool print, bool norm);

    void printSettingData(std::string multigridAlg);

    void printResult(long double exeTime, double normResidualRatio, 
                     double normErrorRation, std::string multigridAlg, 
                     std::string relaxationMethod, T omega, 
                     int nu_1, int nu_2, int numberOfSteps);
};


#endif
