

/*-----------------------------------------------------------------------*\
           ___________________________________________________
          /                                                  /|
         /                                                  / |
        |--------------------------------------------------|  |
        |    ______ __     __  ______    _____  __   __    |  |
        |   |   ___|  \   /  |/   ___|  |  _  \|  | |  |   |  |
        |   |  |_  |   \_/   |   /      | |_)  |  | |  |   |  |
        |   |    | |         |  |   ____|   __/|  | |  |   |  |
        |   |   _| |   ___   |  |  |_  _|  |   |  |_|  |   |  |
        |   |  |   |  |   |  |   \__/ / |  |   |       |   |  |
        |   |__|   |__|   |__|\______/  |__|    \_____/    |  |
        |                                                  |  |
        |      Full      Multigrid      On     GPGPU       | /
        |__________________________________________________|/

--------------------------------------------------------------------------

Author

    Mehdi Baba Mehdi
    mehdi.baba_mehdi@uni-wuppertal.de

Date  
    Wed 15.04.2020
\*-----------------------------------------------------------------------*/

#ifndef DEFINES_HPP
#define DEFINES_HPP

/* If local OPENCL library is used, activate this */
#define LOCAL_CL
 
#define WGX 8       // local work group size in x direction
#define WGY 8       // local work group size in y direction
#define WGZ 8       // local work group size in z direction

#define stencilWidth 3
#define stencilRadius (stencilWidth / 2)
#define padding  (stencilRadius * 2)

#define stencilSize (stencilWidth * stencilWidth * stencilWidth) 

#endif
