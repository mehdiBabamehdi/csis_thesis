/*-----------------------------------------------------------------------*\
           ___________________________________________________
          /                                                  /|
         /                                                  / |
        |--------------------------------------------------|  |
        |    ______ __     __  ______    _____  __   __    |  |
        |   |   ___|  \   /  |/   ___|  |  _  \|  | |  |   |  |
        |   |  |_  |   \_/   |   /      | |_)  |  | |  |   |  |
        |   |    | |         |  |   ____|   __/|  | |  |   |  |
        |   |   _| |   ___   |  |  |_  _|  |   |  |_|  |   |  |
        |   |  |   |  |   |  |   \__/ / |  |   |       |   |  |
        |   |__|   |__|   |__|\______/  |__|    \_____/    |  |
        |                                                  |  |
        |      Full      Multigrid      On     GPGPU       | /
        |__________________________________________________|/

--------------------------------------------------------------------------

Author

    Mehdi Baba Mehdi
    mehdi.baba_mehdi@uni-wuppertal.de

Date  
    Wed 03.05.2020
\*-----------------------------------------------------------------------*/


#include "../includes/multiGrid.hpp"

template <class T>
multiGrid<T>::multiGrid(const T* _RHS,
           const int _origHeight, const int _origWidth, const int _origDepth,
           std::string _relaxationMethod, T _omega, 
           cl_device_type _deviceType, int _deviceID)
           :
           solver<T>(_RHS, _origHeight, _origWidth, _origDepth),
           deviceType{_deviceType},
           deviceID{_deviceID}
{
  setNumberOfMGLevels();
}


template <class T>
multiGrid<T>::multiGrid(std::ifstream _RHSFILENAME,
           const int _origHeight, const int _origWidth, const int _origDepth,
           std::string _relaxationMethod, T _omega, 
           cl_device_type _deviceType, int _deviceID)
           :
           solver<T>(_RHSFILENAME, _origHeight, _origWidth, _origDepth),
           relaxationMethod{_relaxationMethod},
           omega{_omega},
           deviceType{_deviceType},
           deviceID{_deviceID}
{
  setNumberOfMGLevels();
}


template <class T>
multiGrid<T>::multiGrid(const int _origHeight, const int _origWidth, 
           const int _origDepth,
           std::string _relaxationMethod, T _omega, 
           cl_device_type _deviceType, int _deviceID)
           :
           solver<T>(_origHeight, _origWidth, _origDepth),
           relaxationMethod{_relaxationMethod},
           omega{_omega},
           deviceType{_deviceType},
           deviceID{_deviceID}
{
  setNumberOfMGLevels();
}


template <class T>
multiGrid<T>::multiGrid(const solver<T>& _mgSolver,
           std::string _relaxationMethod, T _omega, 
           cl_device_type _deviceType, int _deviceID)
           :
           solver<T>(_mgSolver),
           relaxationMethod{_relaxationMethod},
           omega{_omega},
           deviceType{_deviceType},
           deviceID{_deviceID}
{
  setNumberOfMGLevels();
}


template <class T>
multiGrid<T>::multiGrid(std::string _relaxationMethod, T _omega, 
                        cl_device_type _deviceType, int _deviceID)
           :
           solver<T>(),
           relaxationMethod{_relaxationMethod},
           omega{_omega},
           deviceType{_deviceType},
           deviceID{_deviceID}
{
  setNumberOfMGLevels();
}


template <class T>
multiGrid<T>::~multiGrid()
{
}



template <class T>
void multiGrid<T>::setDevice()
{
  cl::Platform::get(&platforms);

  assert(platforms.size() > 0);

  myPlatform = cl::Platform(platforms[0]);

  myPlatform.getDevices(CL_DEVICE_TYPE_GPU, &devices);
  assert(devices.size() > 0);

  device = devices[deviceID];
  displayDeviceInfo();
  context = cl::Context(device);
}


template <class T>
void multiGrid<T>::displayDeviceInfo()
{
  std::cout << "\n__________________DEVICE INFO_____________________\n" << std::endl;
  std::cout << std::setfill(' ') << std::setw(25);
  std::cout << std::left << "Platform"; 
  std::cout << std::right << ":  " << myPlatform.getInfo<CL_PLATFORM_NAME>() << std::endl;

  std::cout << std::setfill(' ') << std::setw(25);
  std::cout << std::left << "Device"; 
  std::cout << std::right << ":  " << device.getInfo<CL_DEVICE_NAME>() << std::endl;

  std::vector<::size_t> maxWorkItems;
  maxWorkItems = device.getInfo<CL_DEVICE_MAX_WORK_ITEM_SIZES>();

  std::cout << std::setfill(' ') << std::setw(25);
  std::cout << std::left << "Max. Work Item Sizes";
  std::cout << std::right << ":  " << maxWorkItems[0] << "*" << maxWorkItems[1] 
            << "*" << maxWorkItems[2] << std::endl;

  std::cout << std::setfill(' ') << std::setw(25);
  std::cout << std::left << "Max. Work Group Size";
  std::cout << std::right <<  ":  " << device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>() 
            << std::endl;

  std::cout << std::setfill(' ') << std::setw(25);
  std::cout << std::left << "Global Memory Size";
  std::cout << std::right << ":  " << device.getInfo<CL_DEVICE_GLOBAL_MEM_SIZE>()
   / 1024/1024/1024 << " GB" << std::endl;

  std::cout << std::setfill(' ') << std::setw(25);
  std::cout << std::left << "Local Memory Size";
  std::cout << std::right << ":  " << device.getInfo<CL_DEVICE_LOCAL_MEM_SIZE>()
    / 1024 << " KB" << std::endl;

  std::cout << std::setfill(' ') << std::setw(25);
  std::cout << std::left << "Constant Memory Size";
  std::cout << std::right << ":  " << device.getInfo<CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE>()                                       / 1024 << " KB" << std::endl;

  std::cout << std::setfill(' ') << std::setw(25);
  std::cout << std::left << "Number of Compute Unit"; 
  std::cout << std::right << ":  " << device.getInfo<CL_DEVICE_MAX_COMPUTE_UNITS>() 
            << std::endl;

  std::cout << "__________________________________________________\n" << std::endl;
}


template <class T>
int multiGrid<T>::roundUp(unsigned int value, unsigned int multiple)
{
  int remainder = value % multiple;

  if(remainder != 0)
  {
    value += (multiple - remainder);
  }
  return value;
}


template <class T>
inline void multiGrid<T>::setNumberOfMGLevels()
{
  multiGridLevels = std::min(         log2((double)this->origWidth  - 1), 
                            std::min(log2((double)this->origHeight - 1), 
                                     log2((double)this->origDepth  - 1))) - 1;
}


template <class T>
void multiGrid<T>::buildProgram(T omega)
{
  setDevice();

  std::ifstream kernelFile(kernelFileName);

  std::string src(std::istreambuf_iterator<char>(kernelFile), 
                 (std::istreambuf_iterator<char>()));

  cl::Program::Sources sources;
  try
  { 
    sources =  cl::Program::Sources(1,std::make_pair(src.c_str(),src.length() + 1));
  }catch (const cl::Error& error)
  {
    std::cout << "  -> Multigrid Class, Problem in creating source  " << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }
  
  try
  {
    program = cl::Program(context, sources);
  }catch (const cl::Error& error)
  {
    std::cout << "  -> Multigrid Class, Problem in creating program  " << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }
  
  // create queue to which we will push commands for the device.
  try
  {
    queue = cl::CommandQueue(context, device, CL_QUEUE_PROFILING_ENABLE);
  }catch (const cl::Error& error)
  {
    std::cout << "  -> Multigrid Class, Problem in creating queue  " << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }


  // create preprocessor defines for the kernel
  std::string buildOptions;
  {
    // find type of template (T) and pass it as preprocessor to kernel
    int status;
    std::string tname = typeid(T).name();
    char *typeOfData = abi::__cxa_demangle(tname.c_str(), NULL, NULL, &status);
    char buf[256]; 
    sprintf(buf,"-cl-std=CL2.0 -w -I. -D T=%s -D omega=%f -D diagCoeff=%f", typeOfData, 
                omega, 6.0f);
    buildOptions += std::string(buf);
  }
 
  std::cout << std::setfill(' ') << std::setw(40);
  std::cout << std::left << "==> Building program";
  try
  {
    program.build(buildOptions.c_str());
    std::cout << std::right << "  -> Done! " << std::endl;
  }catch(const cl::Error& error)
  {
    std::cout << "  -> Multigrid Class, Problem in building program " << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    std::cout << "  -> " << program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(device) 
              << std::endl;
    exit(0);
  }
}


template <class T>
void multiGrid<T>::setDomainSize()
{
  bufferOrigin[0] = 0;
  bufferOrigin[1] = 0;
  bufferOrigin[2] = 0;

  hostOrigin[0] = 0;
  hostOrigin[1] = 0;
  hostOrigin[2] = 0;
  
  cl::size_t<3> tmpRegion;
  for (int i = 0; i <= multiGridLevels; i++)
  {
     spatialStepSize.push_back(this->origSpatialStepSize * pow(2,i));
     
     domainWidth.push_back((this->origWidth   - 1) / pow(2,i) + 1);
     domainHeight.push_back((this->origHeight - 1) / pow(2,i) + 1);
     domainDepth.push_back((this->origDepth   - 1) / pow(2,i) + 1);

     deviceWidth.push_back(roundUp(domainWidth[i] , WGX));
     deviceHeight.push_back(domainHeight[i]);
     deviceDepth.push_back(domainDepth[i]);
    
     deviceDataSize.push_back(deviceWidth[i] * deviceHeight[i] * 
                              deviceDepth[i] * sizeof(T));

     subt.push_back(deviceWidth[i] - domainWidth[i]);
     
     tmpRegion[0] = (size_t)(deviceWidth[i] * sizeof(T));
     tmpRegion[1] = (size_t)(domainHeight[i]);
     tmpRegion[2] = (size_t)(domainDepth[i]);
     region.push_back(tmpRegion);
  }
}


template <class T>
void multiGrid<T>::createBuffers()
{  
  std::cout << std::setfill(' ') << std::setw(40);
  std::cout << std::left << "==> Creation of Buffers";
  int stencilDataSize   = stencilSize * sizeof(T);
  xBuffer               = cl::Buffer(context, CL_MEM_READ_WRITE,  deviceDataSize[0]);
  bBuffer               = cl::Buffer(context, CL_MEM_READ_WRITE,  deviceDataSize[0]);
  
  for (int i = 0; i <= multiGridLevels; i++)
  {
     matrixBuffer.push_back(cl::Buffer(context, CL_MEM_READ_ONLY, stencilDataSize));

     residualBuffer.push_back(cl::Buffer(context, CL_MEM_READ_WRITE, deviceDataSize[i]));

     interResidualBuffer.push_back(cl::Buffer(context, CL_MEM_READ_WRITE, 
                                   deviceDataSize[i]));
    
     intermediateBuffer.push_back(cl::Buffer(context, CL_MEM_READ_WRITE, 
                                   deviceDataSize[i]));
     errorBuffer.push_back(cl::Buffer(context, CL_MEM_READ_WRITE, deviceDataSize[i]));
  }
  std::cout << std::right << "  -> Done! " << std::endl;
}


template <class T>
void multiGrid<T>::setNDRange()
{ 
  size_t maxGlobalWorkSize[3];
  cl::NDRange globalWorkSize; 
  std::vector<int> localSize;
  
  
  std::cout << std::setfill(' ') << std::setw(40);
  std::cout << std::left << "==> Setting Ranges!";
  for (int i = 0; i <= multiGridLevels; i++)
  {
     if (domainWidth[i] >= WGX && domainHeight[i] >= WGY && domainDepth[i] >= WGZ)
     {
        maxGlobalWorkSize[0] = (size_t)roundUp(domainWidth[i]  - padding, WGX);
        maxGlobalWorkSize[1] = (size_t)roundUp(domainHeight[i] - padding, WGY);
        maxGlobalWorkSize[2] = (size_t)roundUp(domainDepth[i]  - padding, WGZ);
        globalWorkSize = cl::NDRange(maxGlobalWorkSize[0], maxGlobalWorkSize[1],  
                                  maxGlobalWorkSize[2]);
        globalRange.push_back(globalWorkSize);
        restProlGlobalRange.push_back(globalWorkSize);
        localRange.push_back(cl::NDRange((size_t) WGX, (size_t) WGY, (size_t) WGZ));

        int localWidth  = WGX + padding;
        int localHeight = WGY + padding;
        int localDepth  = WGZ + padding;
        localSize.push_back(localHeight);
        localSize.push_back(localWidth);
        localSize.push_back(localDepth);
        deviceLocalSize.push_back(localSize);
     } else if (domainWidth[i] < WGX || domainHeight[i] < WGX || domainDepth[i] < WGX)
     {
        maxGlobalWorkSize[0] = (size_t)roundUp(domainWidth[i]  - padding, WGX);
        maxGlobalWorkSize[1] = (size_t)roundUp(domainHeight[i] - padding, WGY);
        maxGlobalWorkSize[2] = (size_t)roundUp(domainDepth[i]  - padding, WGZ);
        globalWorkSize = cl::NDRange(maxGlobalWorkSize[0], maxGlobalWorkSize[1],  
                                  maxGlobalWorkSize[2]);
        restProlGlobalRange.push_back(globalWorkSize);

        maxGlobalWorkSize[0] = (size_t)roundUp(domainWidth[i]  - padding, 1);
        maxGlobalWorkSize[1] = (size_t)roundUp(domainHeight[i] - padding, 1);
        maxGlobalWorkSize[2] = (size_t)roundUp(domainDepth[i]  - padding, 1);
        globalWorkSize = cl::NDRange(maxGlobalWorkSize[0], maxGlobalWorkSize[1],  
                                  maxGlobalWorkSize[2]);
        globalRange.push_back(globalWorkSize);
        localRange.push_back(cl::NDRange((size_t) 1, (size_t) 1, (size_t) 1));
        int localWidth  = 1 + padding;
        int localHeight = 1 + padding;
        int localDepth  = 1 + padding;
        localSize.push_back(localHeight);
        localSize.push_back(localWidth);
        localSize.push_back(localDepth);
        deviceLocalSize.push_back(localSize);
     }
     localSize.clear();
  }
  std::cout << std::right << "  -> Done! " << std::endl;
}


template <class T>
void multiGrid<T>::writeDataToDevice()
{
  std::cout << std::setfill(' ') << std::setw(40);
  std::cout << std::left << "==> Writing Data to device";
  if (this->x == NULL || this->RHS == NULL)
  {
    std::cout << "  -> Multigrid Class, X or RHS is NULL"
              << std::endl;
    exit(0);
  }

  cl::size_t<3> dataRegion;
  dataRegion[0] = (size_t)((this->origWidth) * sizeof(T));
  dataRegion[1] = (size_t)(this->origHeight);
  dataRegion[2] = (size_t)(this->origDepth);

  try
  {
    queue.enqueueWriteBufferRect(xBuffer, CL_TRUE, bufferOrigin, hostOrigin, dataRegion,
            deviceWidth[0] * sizeof(T), 0, domainWidth[0] * sizeof(T), 0, this->x);
    queue.enqueueWriteBufferRect(bBuffer, CL_TRUE, bufferOrigin, hostOrigin, dataRegion,
           deviceWidth[0] * sizeof(T), 0, domainWidth[0] * sizeof(T), 0, this->RHS);

    T* stencil = new T[stencilSize];
    int stencilDataSize = stencilSize * sizeof(T);
  
    for (int i = 0; i <= multiGridLevels; i++)
    { 
      createMatrixA(stencil,i);
      queue.enqueueWriteBuffer(matrixBuffer[i], CL_TRUE, 0, stencilDataSize, stencil);
    }
      
    delete[] stencil;
  }catch (cl::Error& error)
  {
    std::cout << "  -> Multigrid Class, Problem in writing data from Host to"
                   "  Device: " << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }
  std::cout << std::right << "  -> Done! " << std::endl;
}


template<class T>
void multiGrid<T>::createMatrixA(T* stencil, const int level)
{
  double coef = 1.0f / pow(spatialStepSize[level],2);
   
  stencil[0] = 0.0f;
  stencil[1] = 0.0f;
  stencil[2] = 0.0f;
  stencil[3] = 0.0f;
  stencil[4] = - 1.0 * coef;
  stencil[5] = 0.0f;
  stencil[6] = 0.0f;
  stencil[7] = 0.0f;
  stencil[8] = 0.0f;

  stencil[9] = 0.0f;
  stencil[10] = - 1.0f * coef;
  stencil[11] = 0.0f;
  stencil[12] = - 1.0f * coef;
  stencil[13] = 6.0 * coef;
  stencil[14] = - 1.0f * coef;
  stencil[15] = 0.0f;
  stencil[16] = - 1.0f * coef;
  stencil[17] = 0.0f;

  stencil[18] = 0.0f;
  stencil[19] = 0.0f;
  stencil[20] = 0.0f;
  stencil[21] = 0.0f;
  stencil[22] = - 1.0f * coef;
  stencil[23] = 0.0f;
  stencil[24] = 0.0f;
  stencil[25] = 0.0f;
  stencil[26] = 0.0f;
}


template<class T>
void multiGrid<T>::copyBuffer(cl::Buffer& srcBuffer, cl::Buffer& distBuffer, 
                                  int level)
{
  try
  {
    queue.enqueueCopyBufferRect(srcBuffer, distBuffer, bufferOrigin,
              hostOrigin, region[level], deviceWidth[level] * sizeof(T), 0,
              deviceWidth[level] * sizeof(T), 0, NULL, &event);
  } catch (const cl::Error& error)
  {
    std::cout << "  -> Multigrid Class, Problem in copying buffers" << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }
  
  try
  {
    queue.finish();
  }catch (const cl::Error& error)
  {
    std::cout << "  -> Multigrid Class, Problem in finishing copyBufferRect" 
              << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }

  
}


template<class T>
void multiGrid<T>::solve()
{}


template<class T>
void multiGrid<T>::readDataFromDevice()
{
  residual_h = new T[this->origHeight * this->origWidth * this->origDepth]();
   
  cl::size_t<3> buffer_origin;
  cl::size_t<3> host_origin;
  cl::size_t<3> dataRegion;

  std::cout << "==> Start reading data from device" << std::endl;
  // read result y and residual from the device 
  buffer_origin[0] = 0;
  buffer_origin[1] = 0;
  buffer_origin[2] = 0;

  host_origin[0] = 0;
  host_origin[1] = 0;
  host_origin[2] = 0;

  // region of x
  dataRegion[0] = (size_t)((this->origWidth) * sizeof(T));
  dataRegion[1] = (size_t)(this->origHeight);
  dataRegion[2] = (size_t)(this->origDepth);

  size_t buffer_row_pitch = deviceWidth[0] * deviceHeight[0] * sizeof(T);
  size_t host_row_pitch   = domainWidth[0] * domainHeight[0] * sizeof(T);
 
  try
  {
    queue.enqueueReadBufferRect(residualBuffer[0], CL_TRUE, buffer_origin, 
              host_origin, dataRegion, deviceWidth[0] * sizeof(T), buffer_row_pitch, 
              this->getOrigWidth() * sizeof(T), host_row_pitch, residual_h);
    queue.enqueueReadBufferRect(xBuffer, CL_TRUE, buffer_origin, 
              host_origin, dataRegion, deviceWidth[0] * sizeof(T), buffer_row_pitch, 
              this->getOrigWidth() * sizeof(T), host_row_pitch, this->x);
  } catch (cl::Error& error)
  {
    std::cout << "  -> Multigrid Class, Problem reading buffer in device: "  << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }

  try
  {
    queue.finish();
  }catch (const cl::Error& error)
  {
    std::cout << "  -> Multigrid Class, Problem in finishing reading buffer" 
              << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }



} 


template<class T>
double multiGrid<T>::printData(cl::Buffer& buffer, int stepNumber, bool print, bool norm)
{
  cl::size_t<3> buffer_origin;
  cl::size_t<3> host_origin;
  cl::size_t<3> dataRegion;

  // read result y and residual from the device 
  buffer_origin[0] = 0;
  buffer_origin[1] = 0;
  buffer_origin[2] = 0;

  host_origin[0] = 0;
  host_origin[1] = 0;
  host_origin[2] = 0;

  dataRegion[0] = (size_t)((domainWidth[stepNumber]) * sizeof(T));
  dataRegion[1] = (size_t)(domainHeight[stepNumber]);
  dataRegion[2] = (size_t)(domainDepth[stepNumber]);

  int buffer_row_pitch = deviceWidth[stepNumber] * deviceHeight[stepNumber] * sizeof(T);
  int host_row_pitch = domainWidth[stepNumber] * domainHeight[stepNumber] * sizeof(T);
 
  T* data = new T[domainWidth[stepNumber] * domainHeight[stepNumber] * 
                   domainDepth[stepNumber]]();
  try
  {
    queue.enqueueReadBufferRect(buffer, CL_TRUE, buffer_origin, 
              host_origin, dataRegion, deviceWidth[stepNumber] * sizeof(T), 
              buffer_row_pitch, domainWidth[stepNumber] * sizeof(T), host_row_pitch, 
              data);
  } catch (cl::Error& error)
  {
    std::cout << "  -> Problem reading buffer in device: "  << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }

  double  residualNorm = this->norm_L2(data, 
            domainWidth[stepNumber] * domainHeight[stepNumber] * domainDepth[stepNumber], 
            spatialStepSize[stepNumber]);

  if (norm) std::cout << "   ->||r||_L2 = "  << residualNorm << std::endl;

  #ifdef DEBUG
  if (stepNumber != 0 && plotting)
      mgGnuplot->plot(data, domainHeight[stepNumber], domainWidth[stepNumber], 
                        domainDepth[stepNumber], "error", stepNumber);
  #endif

  if(print)
  {
     std::cout << std::setprecision(4);
     for (int k = 0; k < domainDepth[stepNumber]; ++k)
     {
        for (int i = 0; i < domainHeight[stepNumber]; ++i)
        {
           for (int j = 0; j < domainWidth[stepNumber]; ++j)
           {
             int arrayNumber = k *  domainHeight[stepNumber] * domainWidth[stepNumber] +
                            i * domainWidth[stepNumber] + j;
             std::cout << *(data + arrayNumber) << "  ";
           }
           std::cout << "\n";
        }
        std::cout << "\n\n";
      }
   }
   return residualNorm;
}


template<class T>
inline void multiGrid<T>::printSettingData(std::string multigridAlg)
{
  std::cout << "\n_____________________Result______________________\n" << std::endl;

  std::cout << std::setfill(' ') << std::setw(25);
  std::cout << std::left << "Method";
  std::cout << std::right << ":  " << multigridAlg << std::endl;

  std::cout << std::setfill(' ') << std::setw(25);
  std::cout << std::left <<"Size of Domain (m)"; 
  std::cout << std::right << ":  " << this->spatialLength << " * " << 
                this->spatialLength << " * " << this->spatialLength << std::endl;

  std::cout << std::setfill(' ') << std::setw(25);
  std::cout << std::left <<"Spatial Steps" ;
  std::cout << std::right  << ":  " << this->origHeight-1 << " * " <<
               this->origWidth-1 << " * " << this->origDepth-1 << std::endl;

  std::cout << std::setfill(' ') << std::setw(25);
  std::cout << std::left <<"Local Range";
  std::cout << std::right << ":  " << WGX << " * " 
            << WGY << " * " << WGZ << std::endl;

  std::cout << std::setfill(' ') << std::setw(25);
  std::cout << std::left <<"Local Memory Size";
  std::cout << std::right  << ":  " << deviceLocalSize[0][0] << " * " 
            << deviceLocalSize[0][1] << " * " << deviceLocalSize[0][2] << std::endl;

}


template<class T>
void multiGrid<T>::printResult(long double exeTime, double normResidualRatio, 
                               double normErrorRatio, std::string multigridAlg,
                               std::string relaxationMethod, T omega,
                               int nu_1, int nu_2, int numberOfSteps)
{
  int domainSize = this->domainHeight[0] * this->domainWidth[0] * this->domainDepth[0];
  double WU = 0.0;  


  if (multigridAlg == "V-Cycle")
  {
     for (int i = 0; i < multiGridLevels; ++i)
     {
        WU += 1.0/pow(2,i);
     }
     WU *=domainSize * 13.0 * (nu_1 + nu_2 + 2) * numberOfSteps;
  } else if (multigridAlg == "Full Multigrid")
  {
     for (int i = 0; i < multiGridLevels; ++i)
     {
        WU += 1.0/pow(2,i);
     }
     WU *= 2.0 /(1.0 - pow(2,-3)) * domainSize * 13.0 * (nu_1 + nu_2 + 2) * numberOfSteps;
  }

  printSettingData(multigridAlg);

  std::cout << std::setfill(' ') << std::setw(25);
  std::cout << std::left <<"Multigrid Steps";
  std::cout << std::right << ":  " << numberOfSteps << std::endl;

  std::cout << std::setfill(' ') << std::setw(25);
  std::cout << std::left <<"Multigrid Level";
  std::cout << std::right << ":  " << multiGridLevels + 1 << std::endl;

  std::cout << std::setfill(' ') << std::setw(25);
  std::cout << std::left <<"Smotthing Scheme";
  std::cout << std::right << ":  " << relaxationMethod << std::endl;

  std::cout << std::setfill(' ') << std::setw(25);
  std::cout << std::left <<"Omega";
  std::cout << std::right << ":  " <<  omega << std::endl;

  std::cout << std::setfill(' ') << std::setw(25);
  std::cout << std::left <<"Pre-Smoothing Sweep";
  std::cout << std::right << ":  " << nu_1 << std::endl;

  std::cout << std::setfill(' ') << std::setw(25);
  std::cout << std::left <<"Post-Smoothing Sweep";
  std::cout << std::right << ":  " <<  nu_2 << std::endl;

  std::cout << std::setfill(' ') << std::setw(25);
  std::cout << std::left << "Ratio ||e||_L2"; 
  std::cout << std::right  << ":  " << pow(normErrorRatio, 1.0/numberOfSteps) 
               << std::endl;

  std::cout << std::setfill(' ') << std::setw(25);
  std::cout << std::left << "Ratio ||r||_L2"; 
  std::cout << std::right  << ":  " <<  pow(normResidualRatio, 1.0/numberOfSteps) 
               << std::endl;

  std::cout << std::setfill(' ') << std::setw(25);
  std::cout << std::left << "Exe. time";
  std::cout << std::right  << ":  " << exeTime << " u sec" << std::endl;

  std::cout << std::setfill(' ') << std::setw(25);
  std::cout << std::left << "Performance"; 
  std::cout << std::right  << ":  " << WU/exeTime/1.0e3 << " GFlops/sec" << std::endl;

  std::cout << "__________________________________________________\n" << std::endl;
}

