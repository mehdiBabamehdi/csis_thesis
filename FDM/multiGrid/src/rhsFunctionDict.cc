



/*-----------------------------------------------------------------------*\
           ___________________________________________________
          /                                                  /|
         /                                                  / |
        |--------------------------------------------------|  |
        |    ______ __     __  ______    _____  __   __    |  |
        |   |   ___|  \   /  |/   ___|  |  _  \|  | |  |   |  |
        |   |  |_  |   \_/   |   /      | |_)  |  | |  |   |  |
        |   |    | |         |  |   ____|   __/|  | |  |   |  |
        |   |   _| |   ___   |  |  |_  _|  |   |  |_|  |   |  |
        |   |  |   |  |   |  |   \__/ / |  |   |       |   |  |
        |   |__|   |__|   |__|\______/  |__|    \_____/    |  |
        |                                                  |  |
        |      Full      Multigrid      On     GPGPU       | /
        |__________________________________________________|/

--------------------------------------------------------------------------

Author

    Mehdi Baba Mehdi
    mehdi.baba_mehdi@uni-wuppertal.de

Date  
    Mon 13.04.2020
\*-----------------------------------------------------------------------*/



#include "../includes/rhsFunctionDict.hpp"

template <class T>
T sourceTerm(T xPos, T yPos, T zPos)
{

/*______________________________Constnat Coeff_____________________________*/
// u = (1+x)sin(x+y+z)
/*   return (3.0 * (1.0 + xPos) * sin(xPos + yPos + zPos) - 2.0 * cos(xPos + yPos + zPos));*/

// u = (x^2-x^4)(y^4-y^2)(z^2-z^4)

/*  return (2.0 * ((1.0 - 6.0 * pow(xPos,2)) * (pow(yPos,2) - pow(yPos,4)) * (pow(zPos,2) - pow(zPos,4)) + 
         (1.0 - 6.0 * pow(yPos,2)) * (pow(xPos,2) - pow(xPos,4)) * (pow(zPos,2) - pow(zPos,4)) +
         (1.0 - 6.0 * pow(zPos,2)) * (pow(yPos,2) - pow(yPos,4)) * (pow(xPos,2) - pow(xPos,4))));
*/




/*______________________________Constnat Coeff_____________________________*/
// u = (x-x^2)(y-y^2)(z-z^2)
// a = u+1
  return ((yPos - pow(yPos,2)) * (yPos - pow(yPos,2)) * 
          (zPos - pow(zPos,2)) * (zPos - pow(zPos,2)) *
          (2.0 * (xPos - pow(xPos,2)) - (1.0 - 2.0 * xPos) * (1.0 - 2.0 * xPos)) +
          (xPos - pow(xPos,2)) * (xPos - pow(xPos,2)) * 
          (zPos - pow(zPos,2)) * (zPos - pow(zPos,2)) *
          (2.0 * (yPos - pow(yPos,2)) - (1.0 - 2.0 * yPos) * (1.0 - 2.0 * yPos)) +
          (xPos - pow(xPos,2)) * (xPos - pow(xPos,2)) * 
          (yPos - pow(yPos,2)) * (yPos - pow(yPos,2)) *
          (2.0 * (zPos - pow(zPos,2)) - (1.0 - 2.0 * zPos) * (1.0 - 2.0 * zPos)) +
          2.0 * (yPos - pow(yPos,2)) * (zPos - pow(zPos,2)) +
          2.0 * (xPos - pow(xPos,2)) * (zPos - pow(zPos,2)) +
          2.0 * (xPos - pow(xPos,2)) * (yPos - pow(yPos,2)));


// u = (x-x^2)(y-y^2)(z-z^2)
// a = x+y+z+1
/*    return ((yPos - pow(yPos,2)) * (zPos - pow(zPos,2)) * (2.0 * (xPos + yPos + zPos + 1.0f) - (1.0f - 2.0 * xPos))
          + (xPos - pow(xPos,2)) * (zPos - pow(zPos,2)) * (2.0 * (xPos + yPos + zPos + 1.0f) - (1.0f - 2.0 * yPos))
          + (xPos - pow(xPos,2)) * (yPos - pow(yPos,2)) * (2.0 * (xPos + yPos + zPos + 1.0f) - (1.0f - 2.0 * zPos))) ;*/

}
