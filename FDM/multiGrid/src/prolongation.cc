/*-----------------------------------------------------------------------*\
           ___________________________________________________
          /                                                  /|
         /                                                  / |
        |--------------------------------------------------|  |
        |    ______ __     __  ______    _____  __   __    |  |
        |   |   ___|  \   /  |/   ___|  |  _  \|  | |  |   |  |
        |   |  |_  |   \_/   |   /      | |_)  |  | |  |   |  |
        |   |    | |         |  |   ____|   __/|  | |  |   |  |
        |   |   _| |   ___   |  |  |_  _|  |   |  |_|  |   |  |
        |   |  |   |  |   |  |   \__/ / |  |   |       |   |  |
        |   |__|   |__|   |__|\______/  |__|    \_____/    |  |
        |                                                  |  |
        |      Full      Multigrid      On     GPGPU       | /
        |__________________________________________________|/

--------------------------------------------------------------------------

Author

    Mehdi Baba Mehdi
    mehdi.baba_mehdi@uni-wuppertal.de

Date  
    Fri 10.04.2020
\*-----------------------------------------------------------------------*/

#include "../includes/prolongation.hpp"

/*--------------------Constructor-------------------------*/
template<class T>
prolongation<T>::prolongation(cl::Program& _program, cl::CommandQueue& _queue, 
                const cl::Context& _context)
               
{
  setStencil(_context, _queue);
  createKernel(_program);
}


/*--------------------Deconstructor-------------------------*/
template<class T>
prolongation<T>::~prolongation()
{
}


/*---------------------Member functions------------------------*/
template<class T>
inline const long double prolongation<T>::getTime() const
{
   return timeOfOperation;
}


template<class T>
double prolongation<T>::bernsteinWeight(double t)
{
  if (t == 0)
  {
    return 1;
  }else if (std::abs(t) > 0.0 && std::abs(t) < 1.0)
  {
    return (1.0 - 2 * pow(t,2) + std::abs(pow(t,3)));
  } else if (std::abs(t) >= 1.0 && std::abs(t) <= 2.0)
  {
    return (4.0 - std::abs(8.0 * t) + 5.0 * pow(t,2) - std::abs(pow(t,3)));
  } else
  {
    return 0.0;
  }
}


template<class T>
double prolongation<T>::discreteWeight(double t)
{
  if (t == 0)
  {
    return 1.0;
  }else if (std::abs(t) == 0.5)
  {
    return (9.0 / 16.0);
  } else if (std::abs(t) == 1.5)
  {
    return (- 1.0 / 16.0);
  } else
  {
    return 0.0;
  }
}
template<class T>
void prolongation<T>::cubicInterStencilGen(T* stencil)
{
  int cubicStencilRadius = (int) (cubicStencilWidth / 2);
  double pointWeight;

  double coeff[cubicStencilWidth] = {1.0, 8.0, 1.0, 1.0, 1.0, 8.0, 1.0};

  for (int k = 0; k < cubicStencilWidth; k++)
  {
    for (int i = 0; i < cubicStencilWidth; i++)
    {
      for(int j = 0; j < cubicStencilWidth; j++)
      {
        pointWeight =  discreteWeight(((double)k * coeff[k] - cubicStencilRadius)/2.0) *
                       discreteWeight(((double)i * coeff[i]- cubicStencilRadius)/2.0) *
                       discreteWeight(((double)j * coeff[j]- cubicStencilRadius)/2.0);
        stencil[k * cubicStencilWidth * cubicStencilWidth + i * cubicStencilWidth + j] 
              = pointWeight;
      }
    }
  }

  // center element
  stencil[cubicStencilRadius * cubicStencilWidth * cubicStencilWidth + 
          cubicStencilRadius * cubicStencilWidth + cubicStencilRadius] = 0.0;
}


template<class T>
int prolongation<T>::roundUp(unsigned int value, unsigned int multiple)
{
  int remainder = value % multiple;

  if(remainder != 0)
  {
    value += (multiple - remainder);
  }
  return value;
}


template<class T>
void prolongation<T>::setStencil(const cl::Context& context, cl::CommandQueue& queue)
{
  T* linearStencil = new T[linearStencilSize];
  T* cubicStencil = new T[cubicStencilSize];

  linearStencil[0]  = 1.0f / 8.0f;
  linearStencil[1]  = 1.0f / 4.0f;
  linearStencil[2]  = 1.0f / 8.0f;
  linearStencil[3]  = 1.0f / 4.0f;
  linearStencil[4]  = 1.0f / 2.0f;
  linearStencil[5]  = 1.0f / 4.0f;
  linearStencil[6]  = 1.0f / 8.0f;
  linearStencil[7]  = 1.0f / 4.0f;
  linearStencil[8]  = 1.0f / 8.0f;

  linearStencil[9]  = 1.0f / 4.0f;
  linearStencil[10] = 1.0f / 2.0f;
  linearStencil[11] = 1.0f / 4.0f;
  linearStencil[12] = 1.0f / 2.0f;
  linearStencil[13] = 1.0f;
  linearStencil[14] = 1.0f / 2.0f;
  linearStencil[15] = 1.0f / 4.0f;
  linearStencil[16] = 1.0f / 2.0f;
  linearStencil[17] = 1.0f / 4.0f;

  linearStencil[18] = 1.0f / 8.0f;
  linearStencil[19] = 1.0f / 4.0f;
  linearStencil[20] = 1.0f / 8.0f;
  linearStencil[21] = 1.0f / 4.0f;
  linearStencil[22] = 1.0f / 2.0f;
  linearStencil[23] = 1.0f / 4.0f;
  linearStencil[24] = 1.0f / 8.0f;
  linearStencil[25] = 1.0f / 4.0f;
  linearStencil[26] = 1.0f / 8.0f;
 
  cubicInterStencilGen(cubicStencil);

  try
  {  
    linearStencilBuffer = cl::Buffer(context, CL_MEM_READ_ONLY, 
                                   linearStencilSize * sizeof(T));
    queue.enqueueWriteBuffer(linearStencilBuffer,  CL_TRUE, 0, 
                             linearStencilSize * sizeof(T), linearStencil);
    cubicStencilBuffer = cl::Buffer(context, CL_MEM_READ_ONLY, 
                                   cubicStencilSize * sizeof(T));
    queue.enqueueWriteBuffer(cubicStencilBuffer,  CL_TRUE, 0, 
                             cubicStencilSize * sizeof(T), cubicStencil);

  }catch (const cl::Error& error)
  {
    std::cout << "  -> Prolongation class, Problem in buffer creation/writing "
                   "data to device " << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }
}


template<class T>
void prolongation<T>::createKernel(cl::Program& program)
{
  std::cout << std::setfill(' ') << std::setw(40);
  std::cout << std::left << "==> Prolongation class, Creating kernels";

  try
  {
    linearKernel                  = cl::Kernel(program, "linearInterpolation");
    cubicKernel                   = cl::Kernel(program, "cubicInterpolation");
    copyLinearInterKernel         = cl::Kernel(program, "copyInLinearInterpolation");
    copyCubicInterKernel          = cl::Kernel(program, "copyInCubicInterpolation");
    correctionBoundaryKernel      = cl::Kernel(program, "correctionBoundary");
    setGhostCellBCKernel          = cl::Kernel(program, "setGhostCellBC");
    std::cout << std::right << "  -> Done! " << std::endl;

  }catch (const cl::Error& error)
  {
    std::cout << "  -> Prolongation class, Problem in kernel  " << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }
}


template<class T>
void prolongation<T>::linearInterpolation(cl::CommandQueue& queue, 
          cl::Buffer& inputBuffer,  cl::Buffer& outputBuffer,
          cl::Buffer& intermediateBuffer, 
          const cl::NDRange& srcGlobalRange, const cl::NDRange& srcLocalRange,   
          const cl::NDRange& distGlobalRange, const cl::NDRange& distLocalRange,   
          const int& srcDeviceHeight, const int& srcDeviceWidth, 
          const int& srcDeviceDepth, 
          const int& distDeviceHeight, const int& distDeviceWidth, 
          const int& distDeviceDepth, const std::vector<int> distLocalSize, 
          const int& distSubt, cl::Event& event)
{
  auto t1 = std::chrono::high_resolution_clock::now();

  // This step is done to fill intermediate buffer with zero

  cl::Event copyEvent;
  cl::Event prolEvent;


  int distDeviceDataSize = distDeviceHeight * distDeviceWidth * 
                           distDeviceDepth * sizeof(T);
  T value = 0.0;
   try
  {
    queue.enqueueFillBuffer(intermediateBuffer, value, 0,
                            distDeviceDataSize, NULL, NULL);
  }catch (const cl::Error& error)
  {
    std::cout << "  -> Prolongation class, Problem in enqueue fill buffer" << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }

  try
  {
    queue.finish();
  }catch (const cl::Error& error)
  {
    std::cout << "  -> Prolongation class, Problem in finishing fill buffer" << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }

  int argCount = 0;
  try
  {
    copyLinearInterKernel.setArg(argCount++, inputBuffer);
    copyLinearInterKernel.setArg(argCount++, srcDeviceHeight);
    copyLinearInterKernel.setArg(argCount++, srcDeviceWidth);
    copyLinearInterKernel.setArg(argCount++, srcDeviceDepth);
    copyLinearInterKernel.setArg(argCount++, linearStencilWidth);
    copyLinearInterKernel.setArg(argCount++, intermediateBuffer);
    copyLinearInterKernel.setArg(argCount++, distDeviceWidth);
  }catch (const cl::Error& error)
  {
    std::cout << "  -> Prolongation class, Problem in setting the argument of "
                 "kernel copyLinearInterpolation" << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }

  try
  {
    queue.enqueueNDRangeKernel(copyLinearInterKernel, cl::NullRange, srcGlobalRange, 
                               srcLocalRange, NULL, &copyEvent);
  }catch (const cl::Error& error)
  {
    std::cout << "  -> Prolongation class, Problem in enqueue kernel "
                 "copyLinearInterpolation" << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }

  try
  {
    queue.finish();
  }catch (const cl::Error& error)
  {
    std::cout << "  -> Prolongation class, Problem in finishing kernel "
                 "copyLinearInterpolation" << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }

  copyEvent.wait();
  
  argCount = 0;
  try
  {
    correctionBoundaryKernel.setArg(argCount++, intermediateBuffer);
    correctionBoundaryKernel.setArg(argCount++, distDeviceHeight);
    correctionBoundaryKernel.setArg(argCount++, distDeviceWidth);
    correctionBoundaryKernel.setArg(argCount++, distDeviceDepth);
    correctionBoundaryKernel.setArg(argCount++, distSubt);
  }catch (const cl::Error& error)
  {
    std::cout << "  -> Prolongation class, Problem in setting the argument of "
                 "kernel correctionBoundary" << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }

  try
  {
    queue.enqueueNDRangeKernel(correctionBoundaryKernel, cl::NullRange, distGlobalRange,
                               distLocalRange, NULL, NULL);
  }catch (const cl::Error& error)
  {
    std::cout << "  -> Prolongation class, Problem in enqueue kernel correctionBoundary" 
              << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }

  try
  {
    queue.finish();
  }catch (const cl::Error& error)
  {
    std::cout << "  -> Prolongation class, Problem in finishing kernel correctionBoundary"
              << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }

  int localMemSize = distLocalSize[0] * distLocalSize[1] * distLocalSize[2] * sizeof(T); 

  argCount = 0;
  try
  {
    linearKernel.setArg(argCount++, intermediateBuffer);
    linearKernel.setArg(argCount++, distDeviceHeight);
    linearKernel.setArg(argCount++, distDeviceWidth);
    linearKernel.setArg(argCount++, distDeviceDepth);
    linearKernel.setArg(argCount++, linearStencilBuffer);
    linearKernel.setArg(argCount++, linearStencilWidth);
    linearKernel.setArg(argCount++, outputBuffer);
    linearKernel.setArg(argCount++, distSubt);
    linearKernel.setArg(argCount++, localMemSize, NULL);
    linearKernel.setArg(argCount++, distLocalSize[0]);
    linearKernel.setArg(argCount++, distLocalSize[1]);
    linearKernel.setArg(argCount++, distLocalSize[2]);
  } catch (const cl::Error& error)
  {
    std::cout << "  -> Prolongation class, Problem in setting the argument of kernel "
                 "linearInterpolation" << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }


  try
  {
    queue.enqueueNDRangeKernel(linearKernel, cl::NullRange, distGlobalRange, 
                               distLocalRange, NULL, &event);
  }catch (const cl::Error& error)
  {
    std::cout << "  -> Prolongation class, Problem in enqueue kernel linearInterpolation"
              <<  std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }

  try
  {
    queue.finish();
  }catch (const cl::Error& error)
  {
    std::cout << "  -> Prolongation class, Problem in finishing kernel "
                 "linearInterpolation" << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }
  
  auto t2 = std::chrono::high_resolution_clock::now();
  timeOfOperation +=  std::chrono::duration_cast<std::chrono::microseconds>(t2-t1).count() ;
}


/*__________________________________________________________________________________*\
 * @name cubicInterpolation
 * @info The memeber function impelements cubic interpolation on the given buffer
 *       (inputBuffer) and impelement cubic interpolation on it and add to output
 *       buffer 
\*__________________________________________________________________________________*/
template<class T>
void prolongation<T>::cubicInterpolation(cl::Context& context, cl::CommandQueue& queue, 
          cl::Buffer& inputBuffer,  cl::Buffer& outputBuffer,
          cl::Buffer& intermediateBuffer, 
          const cl::NDRange& srcGlobalRange, const cl::NDRange& srcLocalRange,   
          const cl::NDRange& distGlobalRange, const cl::NDRange& distLocalRange,   
          const int& srcDeviceHeight, const int& srcDeviceWidth, 
          const int& srcDeviceDepth, 
          const int& distDeviceHeight, const int& distDeviceWidth, 
          const int& distDeviceDepth, const std::vector<int> distLocalSize, 
          const int& distSubt, cl::Event& event)
{
  auto t1 = std::chrono::high_resolution_clock::now();

  // This step is done to fill intermediate buffer with zero
  cl::Event copyEvent;
  cl::Event prolEvent;

  int diffCubicLinearWidth = cubicStencilWidth - linearStencilWidth;

  // buffer for cubic interpolation to add ghost cell 
  cl::Buffer cubicIntermediateBuffer(context, CL_MEM_READ_WRITE,
                            (distDeviceHeight + diffCubicLinearWidth) * 
                            (distDeviceWidth + diffCubicLinearWidth)  * 
                            (distDeviceDepth + diffCubicLinearWidth)  * sizeof(T));

  // fill intermediateBuffer with zero
  T value = 0.0;
  try
  {
    int distDeviceDataSize = distDeviceHeight * distDeviceWidth * 
                             distDeviceDepth * sizeof(T);
    queue.enqueueFillBuffer(intermediateBuffer, value, 0,
                            distDeviceDataSize, NULL, NULL);

  }catch (const cl::Error& error)
  {
    std::cout << "  -> Prolongation class, Problem in enqueue fill buffer" << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }

  try
  {
    queue.finish();
  }catch (const cl::Error& error)
  {
    std::cout << "  -> Prolongation class, Problem in finishing fill buffer" << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }

  int argCount = 0;
  try
  {
    copyLinearInterKernel.setArg(argCount++, inputBuffer);
    copyLinearInterKernel.setArg(argCount++, srcDeviceHeight);
    copyLinearInterKernel.setArg(argCount++, srcDeviceWidth);
    copyLinearInterKernel.setArg(argCount++, srcDeviceDepth);
    copyLinearInterKernel.setArg(argCount++, linearStencilWidth);
    copyLinearInterKernel.setArg(argCount++, intermediateBuffer);
    copyLinearInterKernel.setArg(argCount++, distDeviceWidth);
  }catch (const cl::Error& error)
  {
    std::cout << "  -> Prolongation class, Problem in setting the argument of "
                 "kernel copyCubicInter" << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }

  try
  {
    queue.enqueueNDRangeKernel(copyLinearInterKernel, cl::NullRange, srcGlobalRange, 
                               srcLocalRange, NULL, NULL);
  }catch (const cl::Error& error)
  {
    std::cout << "  -> Prolongation class, Problem in enqueue kernel copyCubicInter" 
              << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }

  try
  {
    queue.finish();
  }catch (const cl::Error& error)
  {
    std::cout << "  -> Prolongation class, Problem in finishing kernel copyCubicInter" 
              << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }

  // copy E_2h into bigger buffer with ghost cells
  cl::size_t<3> srcOrigin;
  cl::size_t<3> distOrigin;
  cl::size_t<3> cubicRegion;

  distOrigin[0] = 2 * sizeof(T);
  distOrigin[1] = 2;
  distOrigin[2] = 2;

  srcOrigin[0] = 0;
  srcOrigin[1] = 0;
  srcOrigin[2] = 0;

  cubicRegion[0] = (size_t)(distDeviceWidth * sizeof(T));
  cubicRegion[1] = (size_t)(distDeviceHeight);
  cubicRegion[2] = (size_t)(distDeviceDepth);

  size_t src_row_pitch  = distDeviceWidth * distDeviceHeight * sizeof(T);
  size_t dist_row_pitch = (distDeviceWidth + diffCubicLinearWidth) * 
                          (distDeviceHeight + diffCubicLinearWidth) * sizeof(T);

  try
  {
    queue.enqueueCopyBufferRect(intermediateBuffer, cubicIntermediateBuffer, 
                                srcOrigin, distOrigin, cubicRegion, 
                                distDeviceWidth * sizeof(T), src_row_pitch,
                                (distDeviceWidth + diffCubicLinearWidth) * sizeof(T), 
                                dist_row_pitch, NULL, NULL);
  } catch (const cl::Error& error)
  {
    std::cout << "  -> Prolongation Class, Problem in copying buffers" << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }

  try
  {
    queue.finish();
  }catch (const cl::Error& error)
  {
    std::cout << "  -> Prolongation Class, Problem in finishing copyBufferRect"
              << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }


  // set boundary condition on ghost cell around original buffer
  argCount = 0;
  try
  {
    setGhostCellBCKernel.setArg(argCount++, cubicIntermediateBuffer);
    setGhostCellBCKernel.setArg(argCount++, distDeviceHeight + diffCubicLinearWidth);
    setGhostCellBCKernel.setArg(argCount++, distDeviceWidth  + diffCubicLinearWidth);
    setGhostCellBCKernel.setArg(argCount++, distDeviceDepth  + diffCubicLinearWidth);
    setGhostCellBCKernel.setArg(argCount++, distSubt);
  }catch (const cl::Error& error)
  {
    std::cout << "  -> Prolongation class, Problem in setting the argument of "
                 "kernel setGhostCellBC" << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }

  // set different ranges for the setGhostCellBCKernel, since we need to reach
  // boundary extended domain (domain + 4 ghost cell) to set values of ghost cells
  size_t maxGlobalWorkSize[3];
  maxGlobalWorkSize[0] = (size_t)roundUp(distDeviceWidth  + diffCubicLinearWidth, WGX);
  maxGlobalWorkSize[1] = (size_t)roundUp(distDeviceHeight + diffCubicLinearWidth, WGY);
  maxGlobalWorkSize[2] = (size_t)roundUp(distDeviceDepth  + diffCubicLinearWidth, WGZ);

  cl::NDRange globalRange(maxGlobalWorkSize[0], maxGlobalWorkSize[1], 
                          maxGlobalWorkSize[2]);
  try
  {
    queue.enqueueNDRangeKernel(setGhostCellBCKernel, cl::NullRange, globalRange,
                               distLocalRange, NULL, NULL);
  }catch (const cl::Error& error)
  {
    std::cout << "  -> Prolongation class, Problem in enqueue kernel setGhostCellBC" 
              << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }

  try
  {
    queue.finish();
  }catch (const cl::Error& error)
  {
    std::cout << "  -> Prolongation class, Problem in finishing kernel setGhostCellBC"
              << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }
  // local size is WGX + padding, padding in cubic interpolation is 6 and
  // in linear interpolation is 2, so (+4) is to convert padding
  int localMemSize = (distLocalSize[0] + diffCubicLinearWidth) * 
                     (distLocalSize[1] + diffCubicLinearWidth) * 
                     (distLocalSize[2] + diffCubicLinearWidth) * sizeof(T); 

  // Impelement cubic interpolation on buffer with ghost cell
  argCount = 0;
  try
  {
    cubicKernel.setArg(argCount++, cubicIntermediateBuffer);
    cubicKernel.setArg(argCount++, distDeviceHeight);
    cubicKernel.setArg(argCount++, distDeviceWidth);
    cubicKernel.setArg(argCount++, distDeviceDepth);
    cubicKernel.setArg(argCount++, cubicStencilBuffer);
    cubicKernel.setArg(argCount++, cubicStencilWidth);
    cubicKernel.setArg(argCount++, outputBuffer);
    cubicKernel.setArg(argCount++, distSubt);
    cubicKernel.setArg(argCount++, localMemSize, NULL);
    cubicKernel.setArg(argCount++, distLocalSize[0] + diffCubicLinearWidth);
    cubicKernel.setArg(argCount++, distLocalSize[1] + diffCubicLinearWidth);
    cubicKernel.setArg(argCount++, distLocalSize[2] + diffCubicLinearWidth);
  } catch (const cl::Error& error)
  {
    std::cout << "  -> Prolongation class, Problem in setting the argument of kernel "
                 "cubicInterpolation" << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }

  try
  {
    queue.enqueueNDRangeKernel(cubicKernel, cl::NullRange, distGlobalRange, 
                               distLocalRange, NULL, &event);
  }catch (const cl::Error& error)
  {
    std::cout << "  -> Prolongation class, Problem in enqueue kernel cubicInterpolation" 
              << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }

  try
  {
    queue.finish();
  }catch (const cl::Error& error)
  {
    std::cout << "  -> Prolongation class, Problem in finishing kernel cubicInterpolation"
              << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }

  auto t2 = std::chrono::high_resolution_clock::now();
  timeOfOperation += std::chrono::duration_cast<std::chrono::microseconds>
                     (t2-t1).count() ;
}
