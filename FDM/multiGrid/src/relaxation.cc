
/*-----------------------------------------------------------------------*\
           ___________________________________________________
          /                                                  /|
         /                                                  / |
        |--------------------------------------------------|  |
        |    ______ __     __  ______    _____  __   __    |  |
        |   |   ___|  \   /  |/   ___|  |  _  \|  | |  |   |  |
        |   |  |_  |   \_/   |   /      | |_)  |  | |  |   |  |
        |   |    | |         |  |   ____|   __/|  | |  |   |  |
        |   |   _| |   ___   |  |  |_  _|  |   |  |_|  |   |  |
        |   |  |   |  |   |  |   \__/ / |  |   |       |   |  |
        |   |__|   |__|   |__|\______/  |__|    \_____/    |  |
        |                                                  |  |
        |      Full      Multigrid      On     GPGPU       | /
        |__________________________________________________|/

--------------------------------------------------------------------------

Author

    Mehdi Baba Mehdi
    mehdi.baba_mehdi@uni-wuppertal.de

Date  
    Fri 10.04.2020
\*-----------------------------------------------------------------------*/



#include "../includes/relaxation.hpp"

template<class T>
relaxation<T>::relaxation(cl::Program& _program, cl::CommandQueue& _queue,
                          const cl::Context& _context, 
                          std::string relaxationMethod_, T omega_)
:
                     relaxationMethod{relaxationMethod_},
                     omega{omega_}
{
  createKernel(_program);
  setStencil(_context, _queue);
}


template<class T>
relaxation<T>::~relaxation()
{
   delete[] stencil;
}

template<class T>
inline const T relaxation<T>::getOmega() const
{
   return omega;
}


template<class T>
inline const long double relaxation<T>::getTime() const
{
   return timeOfOperation;
}


template<class T>
void relaxation<T>::setStencil(const cl::Context& context, cl::CommandQueue& queue)
{
  stencil = new T[relaxationStencilSize];
  stencil[0] = 0.0f;
  stencil[1] = 0.0f;
  stencil[2] = 0.0f;
  stencil[3] = 0.0f;
  stencil[4] = 1.0f / 6.0f * omega;
  stencil[5] = 0.0f;
  stencil[6] = 0.0f;
  stencil[7] = 0.0f;
  stencil[8] = 0.0f;

  stencil[9] = 0.0f;
  stencil[10] = 1.0f / 6.0f * omega;
  stencil[11] = 0.0f;
  stencil[12] = 1.0f / 6.0f * omega;
  stencil[13] = (1.0f - omega);
  stencil[14] = 1.0f / 6.0f * omega;
  stencil[15] = 0.0f;
  stencil[16] = 1.0f / 6.0f * omega;
  stencil[17] = 0.0f;

  stencil[18] = 0.0f;
  stencil[19] = 0.0f;
  stencil[20] = 0.0f;
  stencil[21] = 0.0f;
  stencil[22] = 1.0f / 6.0f * omega;
  stencil[23] = 0.0f;
  stencil[24] = 0.0f;
  stencil[25] = 0.0f;
  stencil[26] = 0.0f;

  try
  {
    relaxationStencilBuffer = cl::Buffer(context, CL_MEM_READ_ONLY,  
                                         relaxationStencilSize * sizeof(T));
    queue.enqueueWriteBuffer(relaxationStencilBuffer,  CL_TRUE, 0, 
                             relaxationStencilSize * sizeof(T), stencil);

  }catch (const cl::Error& error)
  {
    std::cout << "  -> Relaxaation class, Problem in buffer creation/writing "
                   "data to device " << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }
}


template<class T>
void relaxation<T>::createKernel(cl::Program& program)
{
  std::cout << std::setfill(' ') << std::setw(40);
  std::cout << std::left << "==> Relaxation class, Creating kernels";

  try
  {
    kernelDirectSolver = cl::Kernel(program, "directSolver");

    if (relaxationMethod == "jacobi")
    {
       kernelJacobi  = cl::Kernel(program, "jacobi");
    } else if (relaxationMethod == "GS")
    {  
       kernelGSRed    = cl::Kernel(program, "GS_red");
       kernelGSBlack  = cl::Kernel(program, "GS_black");
    }
    std::cout << std::right << "  -> Done! " << std::endl;
  }catch (const cl::Error& error)
  {
    std::cout << "  -> Relaxation class, Problem in kernel  " << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }
}


/*
 * x_n = b - Ax_{n-1}
 * inoutBuffer -> x
 * RHSBuffer   -> b
 * relaxationStencilBuffer -> A
 */
template<class T>
void relaxation<T>::relaxing(cl::CommandQueue& queue,
                         cl::Buffer& inoutBuffer, cl::Buffer& RHSBuffer,
                         cl::Buffer& intermediateBuffer,
                         const cl::NDRange& globalRange, const cl::NDRange& localRange,
                         const int numberOfRelaxationSweep,
                         const int& deviceHeight, const int& deviceWidth,
                         const int& deviceDepth, const std::vector<int> localSize, 
                         cl::size_t<3> bufferOrigin, cl::size_t<3> hostOrigin, 
                         cl::size_t<3> region, 
                         const T& spatialStepSize, const int& subt, 
                         cl::Event& event)
{
  auto t1 = std::chrono::high_resolution_clock::now();

  if (deviceWidth - subt == 3)
  {
     directSolver(queue, inoutBuffer, RHSBuffer,
                         intermediateBuffer,
                         globalRange, localRange,
                         deviceHeight, deviceWidth,
                         deviceDepth, localSize, 
                         bufferOrigin, hostOrigin, 
                         region, 
                         spatialStepSize, subt, 
                         event); 
  } else if (deviceWidth - subt > 3)
  {
     if (relaxationMethod == "jacobi")
     {
        jacobi(queue, inoutBuffer, RHSBuffer,
                      intermediateBuffer,
                      globalRange, localRange,
                      numberOfRelaxationSweep,
                      deviceHeight, deviceWidth,
                      deviceDepth, localSize, 
                      bufferOrigin, hostOrigin, 
                      region, 
                      spatialStepSize, subt, 
                      event); 

     } else if (relaxationMethod == "GS")
     {
        Guass_Seidel(queue, inoutBuffer, RHSBuffer,
                            intermediateBuffer,
                            globalRange, localRange,
                            numberOfRelaxationSweep,
                            deviceHeight, deviceWidth,
                            deviceDepth, localSize,
                            bufferOrigin, hostOrigin, 
                            region, 
                            spatialStepSize, subt, 
                            event); 
     }
  }
  auto t2 = std::chrono::high_resolution_clock::now();
  timeOfOperation += std::chrono::duration_cast<std::chrono::microseconds>
                     (t2-t1).count();
}



template<class T>
void relaxation<T>::jacobi(cl::CommandQueue& queue,
                         cl::Buffer& inoutBuffer, cl::Buffer& RHSBuffer,
                         cl::Buffer& intermediateBuffer,
                         const cl::NDRange& globalRange, const cl::NDRange& localRange,
                         const int numberOfRelaxationSweep,
                         const int& deviceHeight, const int& deviceWidth,
                         const int& deviceDepth,const std::vector<int> localSize, 
                         cl::size_t<3> bufferOrigin, cl::size_t<3> hostOrigin, 
                         cl::size_t<3> region, 
                         const T& spatialStepSize, const int& subt, 
                         cl::Event& event)
{
  cl::Event copyEvent;
  cl::Event iterationEvent;

  int localMemSize = localSize[0] * localSize[1] * localSize[2] * sizeof(T);

  try
  {
    queue.enqueueCopyBufferRect(inoutBuffer, intermediateBuffer, bufferOrigin, 
                                hostOrigin, region, deviceWidth * sizeof(T), 0, 
                                deviceWidth * sizeof(T), 0, NULL, &copyEvent);
  } catch (const cl::Error& error)
  {
    std::cout << "  -> Relaxation class, Problem in copying buffer x to y" << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }

  try
  {
    queue.finish();
  }catch (const cl::Error& error)
  {
    std::cout << "  -> Relaxation class, Problem in finishing copy" << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }
  copyEvent.wait();

  int argCount = 0;
  try
  {
    kernelJacobi.setArg(argCount++, inoutBuffer);
    kernelJacobi.setArg(argCount++, deviceHeight);
    kernelJacobi.setArg(argCount++, deviceWidth);
    kernelJacobi.setArg(argCount++, deviceDepth);
    kernelJacobi.setArg(argCount++, relaxationStencilBuffer);
    kernelJacobi.setArg(argCount++, stencilDataWidth);
    kernelJacobi.setArg(argCount++, RHSBuffer);
    kernelJacobi.setArg(argCount++, intermediateBuffer);
    kernelJacobi.setArg(argCount++, spatialStepSize);
    kernelJacobi.setArg(argCount++, subt);
    kernelJacobi.setArg(argCount++, localMemSize, NULL);
    kernelJacobi.setArg(argCount++, localSize[0]);
    kernelJacobi.setArg(argCount++, localSize[1]);
    kernelJacobi.setArg(argCount++, localSize[2]);
  } catch (const cl::Error& error)
  {
    std::cout << "  -> Relaxation class, Problem in setting the argument of kernel" << 
                std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }

  for (int i = 0; i < numberOfRelaxationSweep; ++i)
  {
    try
    {
      queue.enqueueNDRangeKernel(kernelJacobi, cl::NullRange, globalRange, localRange, 
                                 NULL, &iterationEvent);
    }catch (const cl::Error& error)
    {
      std::cout << "  -> Relaxation class, Problem in enqueue kernel" << 
                   std::endl;
      std::cout << "  -> " << getErrorString(error) << std::endl;
      exit(0);
    }

    try
    {
      queue.finish();
    }catch (const cl::Error& error)
    {
      std::cout << "  -> Relaxation class, Problem in finishing kernel" << std::endl;
      std::cout << "  -> " << getErrorString(error) << std::endl;
      exit(0);
    }
    
    iterationEvent.wait();
  
    if (i == numberOfRelaxationSweep - 1)
    {
       try
       {
          queue.enqueueCopyBufferRect(intermediateBuffer, inoutBuffer, bufferOrigin, 
                                      hostOrigin, region, deviceWidth * sizeof(T), 0, 
                                      deviceWidth * sizeof(T), 0, NULL, &event);
       } catch (const cl::Error& error)
       {
           std::cout << "  -> Problem in copying buffer x to y" << std::endl;
           std::cout << "  -> " << getErrorString(error) << std::endl;
           exit(0);
       }
    } else
    {
       try
       {
          queue.enqueueCopyBufferRect(intermediateBuffer, inoutBuffer, bufferOrigin, 
                                      hostOrigin, region, deviceWidth * sizeof(T), 0, 
                                      deviceWidth * sizeof(T), 0, NULL, &copyEvent);
       } catch (const cl::Error& error)
       {
           std::cout << "  -> Problem in copying buffer x to y" << std::endl;
           std::cout << "  -> " << getErrorString(error) << std::endl;
           exit(0);
       }
       copyEvent.wait();
    }

    try
    {
      queue.finish();
    }catch (const cl::Error& error)
    {
      std::cout << "  -> Relaxation class, Problem in finishing copy" << std::endl;
      std::cout << "  -> " << getErrorString(error) << std::endl;
      exit(0);
    }
  }
}


template<class T>
void relaxation<T>::Guass_Seidel(cl::CommandQueue& queue,
                         cl::Buffer& inoutBuffer, cl::Buffer& RHSBuffer,
                         cl::Buffer& intermediateBuffer,
                         const cl::NDRange& globalRange, const cl::NDRange& localRange,
                         const int numberOfRelaxationSweep,
                         const int& deviceHeight, const int& deviceWidth,
                         const int& deviceDepth, const std::vector<int> localSize,
                         cl::size_t<3> bufferOrigin, cl::size_t<3> hostOrigin, 
                         cl::size_t<3> region, 
                         const T& spatialStepSize, const int& subt, 
                         cl::Event& event)
{
  cl::Event copyEvent;
  cl::Event iterationEvent;

  int localMemSize = localSize[0] * localSize[1] * localSize[2] * sizeof(T);

  try
  {
    queue.enqueueCopyBufferRect(inoutBuffer, intermediateBuffer, bufferOrigin, 
                                hostOrigin, region, deviceWidth * sizeof(T), 0, 
                                deviceWidth * sizeof(T), 0, NULL, &copyEvent);
  } catch (const cl::Error& error)
  {
    std::cout << "  -> Relaxation class, Problem in copying buffer x to y" << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }

  try
  {
    queue.finish();
  }catch (const cl::Error& error)
  {
    std::cout << "  -> Relaxation class, Problem in finishing copy" << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }
  copyEvent.wait();

  int argCount = 0;
  try
  {
    kernelGSRed.setArg(argCount++, inoutBuffer);
    kernelGSRed.setArg(argCount++, deviceHeight);
    kernelGSRed.setArg(argCount++, deviceWidth);
    kernelGSRed.setArg(argCount++, deviceDepth);
    kernelGSRed.setArg(argCount++, relaxationStencilBuffer);
    kernelGSRed.setArg(argCount++, stencilDataWidth);
    kernelGSRed.setArg(argCount++, RHSBuffer);
    kernelGSRed.setArg(argCount++, intermediateBuffer);
    kernelGSRed.setArg(argCount++, spatialStepSize);
    kernelGSRed.setArg(argCount++, subt);
    kernelGSRed.setArg(argCount++, localMemSize, NULL);
    kernelGSRed.setArg(argCount++, localSize[0]);
    kernelGSRed.setArg(argCount++, localSize[1]);
    kernelGSRed.setArg(argCount++, localSize[2]);
  } catch (const cl::Error& error)
  {
    std::cout << "  -> Relaxation class, Problem in setting the argument of kernel" << 
                std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }

  argCount = 0;
  try
  {
    kernelGSBlack.setArg(argCount++, inoutBuffer);
    kernelGSBlack.setArg(argCount++, deviceHeight);
    kernelGSBlack.setArg(argCount++, deviceWidth);
    kernelGSBlack.setArg(argCount++, deviceDepth);
    kernelGSBlack.setArg(argCount++, relaxationStencilBuffer);
    kernelGSBlack.setArg(argCount++, stencilDataWidth);
    kernelGSBlack.setArg(argCount++, RHSBuffer);
    kernelGSBlack.setArg(argCount++, intermediateBuffer);
    kernelGSBlack.setArg(argCount++, spatialStepSize);
    kernelGSBlack.setArg(argCount++, subt);
    kernelGSBlack.setArg(argCount++, localMemSize, NULL);
    kernelGSBlack.setArg(argCount++, localSize[0]);
    kernelGSBlack.setArg(argCount++, localSize[1]);
    kernelGSBlack.setArg(argCount++, localSize[2]);
  } catch (const cl::Error& error)
  {
    std::cout << "  -> Relaxation class, Problem in setting the argument of kernel" << 
                std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }
  
  for (int i = 0; i < numberOfRelaxationSweep; ++i)
  {
    try
    {
       queue.enqueueNDRangeKernel(kernelGSRed, cl::NullRange, globalRange, localRange, 
                                  NULL, &iterationEvent);
    }catch (const cl::Error& error)
    {
       std::cout << "  -> Relaxation class, Problem in enqueue kernel" << 
                    std::endl;
       std::cout << "  -> " << getErrorString(error) << std::endl;
       exit(0);
    }

    try
    {
       queue.finish();
    }catch (const cl::Error& error)
    {
       std::cout << "  -> Relaxation class, Problem in finishing kernel" << std::endl;
       std::cout << "  -> " << getErrorString(error) << std::endl;
       exit(0);
    }
    
    iterationEvent.wait();
    try
    {
       queue.enqueueCopyBufferRect(intermediateBuffer, inoutBuffer, bufferOrigin, 
                                   hostOrigin, region, deviceWidth * sizeof(T), 0, 
                                   deviceWidth * sizeof(T), 0, NULL, NULL);
    } catch (const cl::Error& error)
    {
       std::cout << "  -> Problem in copying buffer x to y" << std::endl;
       std::cout << "  -> " << getErrorString(error) << std::endl;
       exit(0);
    }
    
    try
    {
       queue.finish();
    }catch (const cl::Error& error)
    {
       std::cout << "  -> Relaxation class, Problem in finishing kernel" << std::endl;
       std::cout << "  -> " << getErrorString(error) << std::endl;
       exit(0);
    }
    
    try
    {
       queue.enqueueNDRangeKernel(kernelGSBlack, cl::NullRange, globalRange, localRange, 
                                  NULL, &iterationEvent);
    }catch (const cl::Error& error)
    {
       std::cout << "  -> Relaxation class, Problem in enqueue kernel" << 
                   std::endl;
       std::cout << "  -> " << getErrorString(error) << std::endl;
       exit(0);
    }

    try
    {
       queue.finish();
    }catch (const cl::Error& error)
    {
       std::cout << "  -> Relaxation class, Problem in finishing kernel" << std::endl;
       std::cout << "  -> " << getErrorString(error) << std::endl;
       exit(0);
    }
    
    iterationEvent.wait();


    if (i == numberOfRelaxationSweep - 1)
    {
       try
       {
          queue.enqueueCopyBufferRect(intermediateBuffer, inoutBuffer, bufferOrigin, 
                                      hostOrigin, region, deviceWidth * sizeof(T), 0, 
                                      deviceWidth * sizeof(T), 0, NULL, &event);
       } catch (const cl::Error& error)
       {
          std::cout << "  -> Problem in copying buffer x to y" << std::endl;
          std::cout << "  -> " << getErrorString(error) << std::endl;
          exit(0);
       }
    } else
    {
       try
       {
          queue.enqueueCopyBufferRect(intermediateBuffer, inoutBuffer, bufferOrigin, 
                                      hostOrigin, region, deviceWidth * sizeof(T), 0, 
                                      deviceWidth * sizeof(T), 0, NULL, &copyEvent);
       } catch (const cl::Error& error)
       {
           std::cout << "  -> Problem in copying buffer x to y" << std::endl;
           std::cout << "  -> " << getErrorString(error) << std::endl;
           exit(0);
       }
       copyEvent.wait();
    }
    
    try
    {
      queue.finish();
    }catch (const cl::Error& error)
    {
      std::cout << "  -> Relaxation class, Problem in finishing copy" << std::endl;
      std::cout << "  -> " << getErrorString(error) << std::endl;
      exit(0);
    }
  }
}



template<class T>
void relaxation<T>::directSolver(cl::CommandQueue& queue,
                         cl::Buffer& inoutBuffer, cl::Buffer& RHSBuffer,
                         cl::Buffer& intermediateBuffer,
                         const cl::NDRange& globalRange, const cl::NDRange& localRange,
                         const int& deviceHeight, const int& deviceWidth,
                         const int& deviceDepth,const std::vector<int> localSize, 
                         cl::size_t<3> bufferOrigin, cl::size_t<3> hostOrigin, 
                         cl::size_t<3> region, 
                         const T& spatialStepSize, const int& subt, 
                         cl::Event& event)
{
  cl::Event copyEvent;
  cl::Event iterationEvent;

  int localMemSize = localSize[0] * localSize[1] * localSize[2] * sizeof(T);

  try
  {
    queue.enqueueCopyBufferRect(inoutBuffer, intermediateBuffer, bufferOrigin, 
                                hostOrigin, region, deviceWidth * sizeof(T), 0, 
                                deviceWidth * sizeof(T), 0, NULL, &copyEvent);
  } catch (const cl::Error& error)
  {
    std::cout << "  -> Relaxation class, Problem in copying buffer x to y" << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }

  try
  {
    queue.finish();
  }catch (const cl::Error& error)
  {
    std::cout << "  -> Relaxation class, Problem in finishing copy" << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }
  copyEvent.wait();

  int argCount = 0;
  try
  {
    kernelDirectSolver.setArg(argCount++, inoutBuffer);
    kernelDirectSolver.setArg(argCount++, deviceHeight);
    kernelDirectSolver.setArg(argCount++, deviceWidth);
    kernelDirectSolver.setArg(argCount++, deviceDepth);
    kernelDirectSolver.setArg(argCount++, relaxationStencilBuffer);
    kernelDirectSolver.setArg(argCount++, stencilDataWidth);
    kernelDirectSolver.setArg(argCount++, RHSBuffer);
    kernelDirectSolver.setArg(argCount++, intermediateBuffer);
    kernelDirectSolver.setArg(argCount++, spatialStepSize);
    kernelDirectSolver.setArg(argCount++, subt);
    kernelDirectSolver.setArg(argCount++, localMemSize, NULL);
    kernelDirectSolver.setArg(argCount++, localSize[0]);
    kernelDirectSolver.setArg(argCount++, localSize[1]);
    kernelDirectSolver.setArg(argCount++, localSize[2]);
  } catch (const cl::Error& error)
  {
    std::cout << "  -> Relaxation class, Problem in setting the argument of kernel" << 
                std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }

  try
  {
    queue.enqueueNDRangeKernel(kernelDirectSolver, cl::NullRange, globalRange, localRange,
                                 NULL, &iterationEvent);
  }catch (const cl::Error& error)
  {
    std::cout << "  -> Relaxation class, Problem in enqueue kernel" << 
                   std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }

  try
  {
    queue.finish();
  }catch (const cl::Error& error)
  {
    std::cout << "  -> Relaxation class, Problem in finishing kernel" << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }
    
  iterationEvent.wait();
  
  try
  {
    queue.enqueueCopyBufferRect(intermediateBuffer, inoutBuffer, bufferOrigin, 
                                      hostOrigin, region, deviceWidth * sizeof(T), 0, 
                                      deviceWidth * sizeof(T), 0, NULL, &event);
  } catch (const cl::Error& error)
  {
    std::cout << "  -> Problem in copying buffer x to y" << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }
  
  try
  {
    queue.finish();
  }catch (const cl::Error& error)
  {
    std::cout << "  -> Relaxation class, Problem in finishing copy" << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }
}




