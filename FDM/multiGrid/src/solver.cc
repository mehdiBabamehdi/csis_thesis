
/*-----------------------------------------------------------------------*\
           ___________________________________________________
          /                                                  /|
         /                                                  / |
        |--------------------------------------------------|  |
        |    ______ __     __  ______    _____  __   __    |  |
        |   |   ___|  \   /  |/   ___|  |  _  \|  | |  |   |  |
        |   |  |_  |   \_/   |   /      | |_)  |  | |  |   |  |
        |   |    | |         |  |   ____|   __/|  | |  |   |  |
        |   |   _| |   ___   |  |  |_  _|  |   |  |_|  |   |  |
        |   |  |   |  |   |  |   \__/ / |  |   |       |   |  |
        |   |__|   |__|   |__|\______/  |__|    \_____/    |  |
        |                                                  |  |
        |      Full      Multigrid      On     GPGPU       | /
        |__________________________________________________|/

--------------------------------------------------------------------------

Author

    Mehdi Baba Mehdi
    mehdi.baba_mehdi@uni-wuppertal.de

Date  
    Wed 01.04.2020
\*-----------------------------------------------------------------------*/

#include "../includes/solver.hpp"

template <class T>
solver<T>::solver(const T* _RHS,
               const int _origHeight, const int _origWidth, const int _origDepth)
               :
               RHS{_RHS},
               origHeight{_origHeight+1},
               origWidth{_origWidth+1},
               origDepth{_origDepth+1}
{
  printLabel();
  x = new T[origWidth * origHeight * origDepth]();
  RHS = new T[origWidth * origHeight * origDepth]();
  origSpatialStepSize = (double)spatialLength / (double)(origHeight - 1);
  setInitialValues(initialValue);   
  setRightHandSide();
  bcObject = new boundaryCondition<T>(*this);  
}


template <class T>
solver<T>::solver(std::ifstream _RHSFileName,
               const int _origHeight, const int _origWidth, const int _origDepth)
               :
               origHeight{_origHeight+1},
               origWidth{_origWidth+1},
               origDepth{_origDepth+1}
{
  printLabel();
  x = new T[origWidth * origHeight * origDepth]();
  RHS = new T[origWidth * origHeight * origDepth]();
  
  if(!RHSFileName.is_open())
  {
     std::cerr << "==> Solver class, Input File is NULL!" << std::endl;
     exit(0);
  }

  readDataFromFile();
  origSpatialStepSize = (double)spatialLength / (double)(origHeight - 1);
  setInitialValues(initialValue);   
  bcObject = new boundaryCondition<T>(*this);  
}


template <class T>
solver<T>::solver(const int _origHeight, const int _origWidth, const int _origDepth)
               :
               origHeight{_origHeight+1},
               origWidth{_origWidth+1},
               origDepth{_origDepth+1}
{
  printLabel();
  x = new T[origWidth * origHeight * origDepth]();
  RHS = new T[origWidth * origHeight * origDepth]();
  
  origSpatialStepSize = (double)spatialLength / (double)(origHeight - 1);
  setInitialValues(initialValue);   
  bcObject = new boundaryCondition<T>(*this);  
  setRightHandSide();
}


template <class T>
solver<T>::solver(const solver<T>& _solver)              
{           
  if (_solver.origHeight == 0 || _solver.origWidth == 0 ||
      _solver.origDepth  == 0 || _solver.origSpatialStepSize == 0)
  {
     std::cout << "==> Solver class, uninitialized variable in copy constructor!" 
               << std::endl;
  }
  origHeight = _solver.getOrigHeight();
  origWidth = _solver.getOrigWidth();
  origDepth = _solver.getOrigDepth();

  if ( _solver.origSpatialStepSize == 0.0)
  {
     origSpatialStepSize = (double)spatialLength / (double)(origHeight - 1);
  } else
  {
     origSpatialStepSize = _solver.origSpatialStepSize;
  }

  if(_solver.x == NULL)
  {
     x = new T[origWidth * origHeight * origDepth]();
     setInitialValues(initialValue);   
     bcObject = new boundaryCondition<T>(*this);  
  } else 
  {
     x = new T[origWidth * origHeight * origDepth]();
     x = _solver.x;
  }
  
  if (_solver.RHS == NULL)
  {
     RHS = new T[origWidth * origHeight * origDepth]();
     setRightHandSide();
  } else
  {
     RHS = new T[origWidth * origHeight * origDepth]();
     RHS = _solver.RHS;
  }
}


template <class T>
solver<T>::solver()
{
}


template <class T>
 solver<T>::~solver()
{
}


template <class T>
void solver<T>::solve()
{
}


template <class T>
inline int solver<T>::getOrigWidth() const
{
   return origWidth;
}


template <class T>
inline int solver<T>::getOrigHeight() const
{
   return origHeight;
}


template <class T>
inline int solver<T>::getOrigDepth() const
{
   return origDepth;
}


template <class T>
inline T solver<T>::getOrigSpatialStepSize() const
{
   return origSpatialStepSize;
}


template <class T>
void solver<T>::readDataFromFile()
{
  if(!RHSFileName.is_open())
  {
     std::cerr << "==> Solver class, RHS File is NULL!" << std::endl;
     exit(0);
  }

  for(int i=0; i < origWidth * origHeight * origDepth; ++i)
  {
    RHSFileName >> *(RHS + i);
  }
}


template <class T>
std::ofstream& operator<<(std::ofstream& ofs, const solver<T>& result)
{
   std::cout << "==> Solver class, writing data into given file" << std::endl;
   int arrayNumber;
   for (int k = 0; k < result.origDepth; ++k)
   {
      for (int i = 0; i < result.origHeight; ++i)
      {
          for (int j = 0; j < result.origWidth; ++j)
          {
              arrayNumber = k * result.origHeight * result.origWidth +
                            i * result.origWidth + j;
              ofs << *(result.x + arrayNumber) << "  ";
          }
          ofs << "\n";
      }
      ofs << "\n\n";
   }
   return ofs;
}


template <class T>
std::iostream& operator<<(std::iostream& os, const solver<T>& result)
{
   int arrayNumber;
   for (int k = 0; k < result.origDepth; ++k)
   {
      for (int i = 0; i < result.origHeight; ++i)
      {
          for (int j = 0; j < result.origWidth; ++j)
          {
              arrayNumber = k * result.origHeight * result.origWidth +
                            i * result.origWidth + j;
                            
              os << *(result.x + arrayNumber) << "   ";
          }
          os << "\n";
      }
      os << "\n\n";
   }
   return os;
}


template <class T>
void  solver<T>::setInitialValues(T initValue)
{
 for (int i = 0; i < origWidth * origHeight * origDepth; ++i)
  {
    x[i] = initValue;
  }
}


template <class T>
void solver<T>::setRightHandSide()
{
  T xPos;
  T yPos;
  T zPos;

  for (int k = 1; k < origDepth-1; ++k)
  {  
     for (int i = 1; i < origWidth-1; ++i)
     {
        for (int j = 1; j < origHeight-1; ++j)
        {
           xPos = j * origSpatialStepSize;
           yPos = i * origSpatialStepSize;
           zPos = k * origSpatialStepSize;

           *(RHS + k * origWidth * origHeight + i * origWidth + j) 
                = sourceTerm(xPos,yPos,zPos);
        }
     }
  }
}


template <class T>
T solver<T>::u(T x, T y, T z)
{
    
      return  (x - pow(x,2)) * (y - pow(y,2)) * (z - pow(z,2));
   // return  (pow(x,2) - pow(x,4)) * (pow(y,4)- pow(y,2)) * (pow(z,2) - pow(z,4));
   // return  ((1.0 + x) * sin(x + y + z));
}


template <class T>
void solver<T>::calcX_exact(T* x_exact)
{
  T xPos, yPos, zPos;
  for (int k = 0; k < origDepth ; ++k)
  {
     for (int i = 0; i < origHeight; ++i)
     {
        for (int j = 0; j < origWidth; ++j)
        {
           xPos = j * getOrigSpatialStepSize();
           yPos = i * getOrigSpatialStepSize();
           zPos = k * getOrigSpatialStepSize();
           *(x_exact + k * getOrigWidth() * getOrigHeight() + i * getOrigWidth() + j) = 
             this->u(xPos, yPos, zPos); 
        }
     }
  }
}


template <class T>
void solver<T>::calcError(T* x_exact, T* x_app, T* error)
{
  for (int i = 0; i < origWidth * origHeight * origDepth; ++i)
  {
     error[i] = std::abs(x_exact[i] - x_app[i]);
  }
} 


template <class T>
double solver<T>::norm_2(const T* vec)
{
  T vec_2 = 0.0;
  for (int i = 0; i < origWidth * origHeight * origDepth; ++i)
  {
    vec_2 += pow(vec[i],2);
  }
  return sqrt(vec_2);
}


template <class T>
double solver<T>::norm_L2(const T* vec, const int& domainSize, const T& spatialStepSize)
{
  T vec_2 = 0.0;
  for (int i = 0; i < domainSize; ++i)
  {
    vec_2 += pow(vec[i],2);
  }
  return sqrt(pow(spatialStepSize, 3) * vec_2);
}



template<class T>
T solver<T>::norm_inf(const T* vec)
{
  T vec_inf = 0.0;
  for (int i = 0; i < origWidth * origHeight * origDepth; ++i)
  {
    if (vec[i] > vec_inf) vec_inf = vec[i];
  }
  return vec_inf;
}


template <class T>
inline void solver<T>::printLabel()
{
  auto time = std::chrono::system_clock::now();
  std::time_t currentTime = std::chrono::system_clock::to_time_t(time);
  std::string label = "\n\n"
"\t           ___________________________________________________\n"
"\t          /                                                  /|\n"
"\t         /__________________________________________________/ |\n"
"\t        |                                                  |  |\n"
"\t        |    ______ __     __  ______    _____  __   __    |  |\n"
"\t        |   |   ___|  \\   /  |/   ___|  |  _  \\|  | |  |   |  |\n"
"\t        |   |  |_  |   \\_/   |   /      | |_)  |  | |  |   |  |\n"
"\t        |   |    | |         |  |   ____|   __/|  | |  |   |  |\n"
"\t        |   |   _| |   ___   |  |  |_  _|  |   |  |_|  |   |  |\n"
"\t        |   |  |   |  |   |  |   \\__/ / |  |   |       |   |  |\n"
"\t        |   |__|   |__|   |__|\\______/  |__|    \\_____/    |  |\n"
"\t        |                                                  | /\n"
"\t        |__________________________________________________|/\n\n"
"\t\t    Author\n"
"\t\t        Mehdi Baba Mehdi\n"
"\t\t        mehdi.baba_mehdi@uni-wuppertal.de\n\n"
"\t\t    Date";

  std::cout << label << std::endl;
  std::cout << "\t\t       " << std::ctime(&currentTime) << std::endl;
std::cout << "\n\n";

} 
