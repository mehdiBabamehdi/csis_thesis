
## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [How to use](#how-to-use)

## General info
The code implements multigrid algorithm (FMG and V-Cycle) to solve Laplace equation descritised by FDM and dG-FEM on GPGPU. The host part developed by C++ programming language and device kernel by OPENCL library.

## Technologies
Project is created with:

g++: version 10.1.0

OPENCL: version 2.0

python : version 3.8

pybind11: version 2.5.0 

boost: version 1.74.0

## How to use
To run the code, modify makefile and add path to header(`CL_INC`) and shared object (`CL_LIB`) of OPENCL library in your local machine. 
